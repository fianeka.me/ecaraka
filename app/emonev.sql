-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 02, 2020 at 10:21 AM
-- Server version: 5.7.26-0ubuntu0.16.04.1-log
-- PHP Version: 7.0.33-0ubuntu0.16.04.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emonev`
--

-- --------------------------------------------------------

--
-- Table structure for table `dataibubayi`
--

CREATE TABLE `dataibubayi` (
  `idibu` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `jumlahpersalinan` varchar(11) NOT NULL,
  `jumlahkematianibu` varchar(11) NOT NULL,
  `jumlahkematianbayi` varchar(11) NOT NULL,
  `penyebabmatiibu` varchar(256) NOT NULL,
  `penyebabmatibayi` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dataibubayi`
--

INSERT INTO `dataibubayi` (`idibu`, `nomorbulan`, `bulan`, `tahun`, `jumlahpersalinan`, `jumlahkematianibu`, `jumlahkematianbayi`, `penyebabmatiibu`, `penyebabmatibayi`) VALUES
(1, 1, 'Januari', '2020', '178', '1', '16', 'IUFD', 'BBLR,Asfiksia,Tetanus,Neonatorum,RDS');

-- --------------------------------------------------------

--
-- Table structure for table `datajiwa`
--

CREATE TABLE `datajiwa` (
  `idjiwa` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `laki` varchar(64) NOT NULL,
  `perempuan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `datajiwa`
--

INSERT INTO `datajiwa` (`idjiwa`, `nomorbulan`, `bulan`, `tahun`, `laki`, `perempuan`) VALUES
(1, 1, 'Januari', '2020', '105', '86'),
(2, 2, 'Februari', '2020', '103', '74');

-- --------------------------------------------------------

--
-- Table structure for table `datars`
--

CREATE TABLE `datars` (
  `idrs` int(11) NOT NULL,
  `namars` varchar(64) NOT NULL,
  `tahunberdiri` varchar(64) NOT NULL,
  `direktur` varchar(64) NOT NULL,
  `jenis` varchar(64) NOT NULL,
  `kelas` varchar(64) NOT NULL,
  `status` varchar(64) NOT NULL,
  `kepemilikan` varchar(64) NOT NULL,
  `namapenyelenggara` varchar(64) NOT NULL,
  `bpjs` varchar(64) NOT NULL,
  `alamat` varchar(64) NOT NULL,
  `kab` varchar(64) NOT NULL,
  `luastanah` varchar(64) NOT NULL,
  `luasbangunan` varchar(64) NOT NULL,
  `telpon` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `website` varchar(64) NOT NULL,
  `noizin` varchar(64) NOT NULL,
  `suratizin` varchar(64) NOT NULL,
  `berlaku` varchar(64) NOT NULL,
  `akreditasi` varchar(64) NOT NULL,
  `penetapan` varchar(64) NOT NULL,
  `aktifakreditasi` varchar(64) NOT NULL,
  `layananunggulan` varchar(64) NOT NULL,
  `simrs` varchar(64) NOT NULL,
  `bankdarah` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `datars`
--

INSERT INTO `datars` (`idrs`, `namars`, `tahunberdiri`, `direktur`, `jenis`, `kelas`, `status`, `kepemilikan`, `namapenyelenggara`, `bpjs`, `alamat`, `kab`, `luastanah`, `luasbangunan`, `telpon`, `email`, `website`, `noizin`, `suratizin`, `berlaku`, `akreditasi`, `penetapan`, `aktifakreditasi`, `layananunggulan`, `simrs`, `bankdarah`) VALUES
(1, 'RS Umum Daerah Syarifah Ambami Rato Ebu', '1959', 'Dr. Nunuk Kristiani', 'Rumah Sakit Umum', 'B', 'BLUD', 'Pemkab', 'Pemkab Bangkalan', 'Ya', 'Jalan Pemuda Kaffa No. 09 Bangkalan', 'Bangkalan', '25.247', '18.439', '3091111', 'eres_bangkalan@yahoo.co.id', 'www.rsabangkalan.com', 'P2T/6/03.22/01/VI/2016', '06-06-2016', '06-06-2021', 'Tingkat Utama', '23-09-2019', '01-01-2019', 'CT Scan, Atroschpy, HD', 'Berfungsi - Front Office, Back Office', 'Ada');

-- --------------------------------------------------------

--
-- Table structure for table `dpa`
--

CREATE TABLE `dpa` (
  `iddpa` int(11) NOT NULL,
  `idindikator` int(11) NOT NULL,
  `idrekening` int(11) NOT NULL,
  `sumberdana` varchar(256) NOT NULL,
  `jumlah` bigint(16) NOT NULL,
  `tw` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `tglinput` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dpa`
--

INSERT INTO `dpa` (`iddpa`, `idindikator`, `idrekening`, `sumberdana`, `jumlah`, `tw`, `tahun`, `tglinput`) VALUES
(16, 2, 20, 'DAUM', 5250000, 'TW3', '2020', '2020-09-26'),
(17, 3, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(18, 3, 11, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(20, 3, 29, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(25, 5, 15, 'DBHCHT', 1854495226, 'TW3', '2020', '2020-09-26'),
(28, 5, 18, 'DBHCHT', 2449827340, 'TW3', '2020', '2020-09-26'),
(30, 6, 14, 'DAUM', 8711500, 'TW3', '2020', '2020-09-26'),
(34, 7, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(35, 7, 20, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(36, 7, 16, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(37, 7, 23, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(38, 8, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(39, 8, 21, 'DAUM', 30522000, 'TW3', '2020', '2020-09-26'),
(40, 9, 11, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(41, 9, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(42, 10, 38, 'DAUM', 750000, 'TW3', '2020', '2020-09-26'),
(43, 10, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(44, 10, 14, 'DAUM', 634000, 'TW3', '2020', '2020-09-26'),
(45, 10, 16, 'DAUM', 1026000, 'TW3', '2020', '2020-09-26'),
(46, 10, 23, 'DAUM', 225000, 'TW3', '2020', '2020-09-26'),
(47, 11, 12, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(48, 11, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(49, 11, 25, 'DAUM', 0, 'TW3', '2020', '2020-09-26'),
(50, 11, 18, 'DAK', 5718602276, 'TW3', '2020', '2020-09-26'),
(54, 12, 26, 'DDL', 46310932370, 'TW3', '2020', '2020-09-26'),
(56, 13, 32, 'DAUM', 16491000, 'TW3', '2020', '2020-09-27'),
(57, 13, 29, 'DAUM', 45815000, 'TW3', '2020', '2020-09-27'),
(58, 14, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-27'),
(59, 14, 14, 'DAUM', 441500, 'TW3', '2020', '2020-09-27'),
(60, 14, 20, 'DAUM', 0, 'TW3', '2020', '2020-09-27'),
(61, 14, 16, 'DAUM', 0, 'TW3', '2020', '2020-09-27'),
(62, 14, 23, 'DAUM', 1800000, 'TW3', '2020', '2020-09-27'),
(64, 15, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-28'),
(69, 16, 11, 'DAUM', 0, 'TW3', '2020', '2020-09-28'),
(70, 16, 9, 'DAUM', 0, 'TW3', '2020', '2020-09-28'),
(71, 16, 30, 'DAUM', 0, 'TW3', '2020', '2020-09-28'),
(72, 16, 31, 'DAUM', 0, 'TW3', '2020', '2020-09-28'),
(75, 17, 40, 'DAUM', 52800000, 'TW3', '2020', '2020-09-28'),
(76, 17, 41, 'DAUM', 18000000, 'TW3', '2020', '2020-09-28'),
(79, 17, 43, 'DAUM', 1821575000, 'TW3', '2020', '2020-09-28'),
(80, 17, 36, 'DAUM', 3600000, 'TW3', '2020', '2020-09-28'),
(81, 18, 14, 'DAUM', 588000, 'TW3', '2020', '2020-09-28'),
(82, 18, 23, 'DAUM', 2700000, 'TW3', '2020', '2020-09-28'),
(83, 12, 27, 'DDL', 29914963210, 'TW3', '2020', '2020-09-28'),
(84, 12, 28, 'DDL', 1321081967, 'TW3', '2020', '2020-09-28'),
(85, 18, 5, 'DAUM', 24538350, 'TW4', '2019', '2020-09-28'),
(86, 14, 5, 'DAUM', 5015100, 'TW4', '2019', '2020-09-28'),
(88, 16, 5, 'DAUM', 15200000, 'TW4', '2019', '2020-09-28');

-- --------------------------------------------------------

--
-- Table structure for table `fasilitastt`
--

CREATE TABLE `fasilitastt` (
  `idfasilitas` int(11) NOT NULL,
  `jenispelayanan` varchar(64) NOT NULL,
  `vvip` int(3) NOT NULL,
  `vip` int(3) NOT NULL,
  `kelas1` int(3) NOT NULL,
  `kelas2` int(3) NOT NULL,
  `kelas3` int(3) NOT NULL,
  `khusus` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fasilitastt`
--

INSERT INTO `fasilitastt` (`idfasilitas`, `jenispelayanan`, `vvip`, `vip`, `kelas1`, `kelas2`, `kelas3`, `khusus`) VALUES
(1, 'Penyakit Dalam', 0, 0, 16, 10, 38, 10),
(4, 'Kesehatan Anak', 0, 0, 9, 4, 16, 4),
(5, 'Obstetri', 0, 0, 2, 4, 21, 0),
(6, 'Ginekologi', 0, 0, 1, 1, 10, 0),
(7, 'Bedah', 0, 0, 8, 5, 42, 0),
(8, 'Isolasi', 0, 0, 0, 0, 0, 23),
(9, 'Umum', 4, 4, 18, 0, 0, 0),
(10, 'Perinatologi/Bayi', 0, 0, 0, 0, 0, 28);

-- --------------------------------------------------------

--
-- Table structure for table `indikatorkerja`
--

CREATE TABLE `indikatorkerja` (
  `idindikatorkerja` int(11) NOT NULL,
  `idraker` int(11) NOT NULL,
  `indikator` varchar(255) NOT NULL,
  `capaianawal` varchar(64) NOT NULL,
  `t1t` varchar(64) NOT NULL,
  `t1r` bigint(12) NOT NULL,
  `t2t` varchar(64) NOT NULL,
  `t2r` bigint(12) NOT NULL,
  `t3t` varchar(64) NOT NULL,
  `t3r` bigint(12) NOT NULL,
  `t4t` varchar(64) NOT NULL,
  `t4r` bigint(12) NOT NULL,
  `t5t` varchar(64) NOT NULL,
  `t5r` bigint(12) NOT NULL,
  `targetakhir` varchar(64) NOT NULL,
  `realisasiakhir` bigint(12) NOT NULL,
  `idunitkerja` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `indikatorkerja`
--

INSERT INTO `indikatorkerja` (`idindikatorkerja`, `idraker`, `indikator`, `capaianawal`, `t1t`, `t1r`, `t2t`, `t2r`, `t3t`, `t3r`, `t4t`, `t4r`, `t5t`, `t5r`, `targetakhir`, `realisasiakhir`, `idunitkerja`) VALUES
(1, 1, 'Jumlah petugas yang memperoleh vaksin hepatitis B sebagai tindakan pencegahan', '100 Orang', '100 Orang', 127500000, '100 Orang', 127500000, '100 Orang', 127500000, '100 Orang', 127500000, '100 Orang', 127500000, '500 Orang', 637500000, 2),
(2, 2, 'Jumlah alat dan media promosi kesehatan', '1 Paket', '1 Paket', 222500000, '1 Paket', 222500000, '1 Paket', 222500000, '1 Paket', 222500000, '1 Paket', 222500000, '1 Paket', 1125000000, 3),
(3, 3, 'Jumlah petugas dan dokter yang terpapar', '27 Orang', '27 Orang', 150000000, '27 Orang', 150000000, '27 Orang', 150000000, '27 Orang', 150000000, '27 Orang', 150000000, '27 Orang', 750000000, 5),
(5, 7, 'Sarana alkes untuk pelayanan kesehatan rumah sakit', '1 paket', '1 paket', 4500000000, '1 paket', 4900000000, '1 paket', 4900000000, '1 paket', 5980000000, '1 paket', 5980000000, '1 paket', 26260000000, 8),
(6, 8, 'Jumlah Tahapan Akreditasi Rumah Sakit', '2 kegiatan', '1 kegiatan', 201579100, '1 kegiatan', 550000000, '1 kegiatan', 550000000, '1 kegiatan', 750000000, '1 kegiatan', 550000000, '6 kegiatan', 2601579100, 5),
(7, 9, 'Indeks kepuasan masyarakat', '1 dokumen', '1 dokumen', 5200000, '1 dokumen', 5200000, '1 dokumen', 5200000, '1 dokumen', 5200000, '1 dokumen', 5200000, '6 dokumen', 26000000, 10),
(8, 10, 'Dokumen laporan dan kajian WASDAL lingkungan RSA', '1 dokumen', '1 dokumen', 59470900, '1 dokumen', 59470900, '1 dokumen', 59470900, '1 dokumen', 59470900, '1 dokumen', 59470900, '1 dokumen', 297354500, 10),
(9, 11, 'jumlah pasien dan keluarga pasien yang memperoleh sosialisasi dan penyuluhan tentang pencegahan dan pemberantasan penyakit HIV (Home visite) dan hemodialisa', '20 orang', '20 orang', 23000000, '20 orang', 23000000, '20 orang', 23000000, '20 orang', 23000000, '20 orang', 23000000, '100 orang', 115000000, 2),
(10, 12, 'Jumlah dokumen laporan kinerja keperawatan', '1 dokumen', '1 dokumen', 3000000, '1 dokumen', 3000000, '1 dokumen', 3000000, '1 dokumen', 3000000, '1 dokumen', 3000000, '6 dokumen', 15000000, 2),
(11, 14, 'Jumlah alat alat kedokteran kesehatan yang tersedia', '1 paket', '1 paket', 12757496000, '1 paket', 12757496000, '1 paket', 12757496000, '1 paket', 12757496000, '1 paket', 12757496000, '1 paket', 63787480000, 7),
(12, 15, 'Jumlah peningkatan pelayanan dan pendukung pelayanan kesehatan BLUD', '104%', '100%', 112830000000, '100%', 112969586466, '100%', 114616705995, '100%', 117232033707, '100%', 122229931828, '100%', 579878257996, 16),
(13, 16, 'Jumlah aparatur yang mengikuti pelatihan/bimtek/sosialisasi/seminar', '100%', '0', 0, '100%', 20000000, '100%', 265000000, '100%', 265000000, '100%', 250000000, '100%', 980000000, 15),
(14, 17, 'Jumlah laporan triwulanan dan semesteran monev PD yang tersusun (APBD,Renja/RKPD)', '100%', '100%', 5200000, '100%', 5200000, '100%', 5200000, '100%', 5200000, '100%', 5200000, '100%', 26000000, 16),
(15, 18, 'Jumlah dokumen laporan keuangan akhir tahun', '100%', '1 Dokumen', 5200000, '1 Dokumen', 5200000, '1Dokumen', 5200000, '1 Dokumen', 5200000, '1Dokumen', 5200000, '6 Dokumen', 26000000, 1),
(16, 19, 'Jumlah kegiatan peningkatan hari jadi dan hari besar yang difasilitasi', '100%', '1 Kegiatan', 15200000, '1 Kegiatan', 25000000, '1 Kegiatan', 25000000, '1 Kegiatan', 25000000, '1 Kegiatan', 25000000, '5 Kegiatan', 115200000, 15),
(17, 20, 'Jumlah jenis jasa perkantoran yang disediakan ', '100%', '100%', 3504350000, '100%', 3553850000, '100%', 3493850000, '100%', 3493850000, '100%', 3493850000, '100%', 17539750000, 1),
(18, 21, 'Jumlah data dan informasi PD', '100%', '100%', 25200000, '100%', 10000000, '100%', 10200000, '100%', 10200000, '100%', 25200000, '100%', 80800000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `indikatorlayanan`
--

CREATE TABLE `indikatorlayanan` (
  `idindikatorlayanan` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `bor` varchar(64) NOT NULL,
  `los` varchar(64) NOT NULL,
  `bto` varchar(64) NOT NULL,
  `toi` varchar(64) NOT NULL,
  `ndr` varchar(64) NOT NULL,
  `gdr` varchar(64) NOT NULL,
  `ratarata` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `indikatorlayanan`
--

INSERT INTO `indikatorlayanan` (`idindikatorlayanan`, `tahun`, `bor`, `los`, `bto`, `toi`, `ndr`, `gdr`, `ratarata`) VALUES
(1, 2019, '63,17', '2,96', '79,16', '1,70', '21,11', '54,00', '445'),
(3, 2020, '29,98', '3,48', '2,75', '7,90', '41,72', '82,17', '26,52');

-- --------------------------------------------------------

--
-- Table structure for table `kodeprogram`
--

CREATE TABLE `kodeprogram` (
  `idkode` int(11) NOT NULL,
  `kodeprogram` varchar(64) NOT NULL,
  `namaprogram` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kodeprogram`
--

INSERT INTO `kodeprogram` (`idkode`, `kodeprogram`, `namaprogram`) VALUES
(1, '1.02.1.02.02.39', 'Program Pelayanan Kesehatan Masyarakat'),
(2, '1.02.1.02.02.39.02', 'Kegiatan Penurunan Faktor Resiko Penyakit Menular Termasuk Imunisasi'),
(3, '1.02.1.02.02.39.03', 'Kegiatan Peningkatan Promosi Kesehatan'),
(4, '1.02.1.02.02.39.04', 'Kegiatan Peningkatan Kesehatan Keluarga'),
(5, '1.02.1.02.02.41', 'Program Pembinaan Lingkungan Sosial Bidang Kesehatan'),
(6, '1.02.1.02.02.41.01', 'Kegiatan Penyediaan/Peningkatan/Pemeliharaan Sarana dan Prasarana Fasilitas Kesehatan yang Bekerjasama dengan Badan Penyelenggara Jaminan Sosial Kesehatan'),
(7, '1.02.1.02.02.47', 'Program Pelayanan Medik'),
(8, '1.02.1.02.02.47.03', 'Kegiatan Standarisasi Rumah Sakit'),
(9, '1.02.1.02.02.48', 'Program Pelayanan Penunjang'),
(10, '1.02.1.02.02.48.01', 'Kegiatan Pelayanan Penunjang Medik'),
(11, '1.02.1.02.02.49', 'Program Pelayanan Keperawatan'),
(12, '1.02.1.02.02.49.02', 'Kegiatan Perencanaan dan Pengembangan Pelayanan Keperawatan'),
(13, '1.02.1.02.02.50', 'Program Pengadaan Peningkatan Sarana Prasarana RSUD'),
(14, '1.02.1.02.02.50.01', 'Kegiatan Penyediaan Alat Kesehatan di Rumah Sakit'),
(15, '1.02.1.02.02.51', 'Program Peningkatan Mutu Pelayanan Kesehatan BLUD'),
(16, '1.02.1.02.02.51.01', 'Kegiatan Pelayanan dan Pendukung Pelayanan'),
(18, '1.02.1.02.02.48.02', 'Kegiatan Pelayanan Penunjang Non Medik'),
(19, '1.02.1.02.02.49.01', 'Kegiatan Pengawasan dan Pengendalian Pelayanan Keperawatan'),
(20, '0.00.1.02.02.16', 'Program Pelayanan Kesekretariatan'),
(21, '0.00.1.02.02.16.07', 'Kegiatan Peningkatan Kapasitas Aparatur'),
(22, '0.00.1.02.02.16.09', 'Kegiatan Monitoring, Evaluasi dan Pelaporan Kinerja Perangkat Daerah'),
(23, '0.00.1.02.02.16.10', 'Kegiatan Penyusunan Laporan Keuangan'),
(24, '0.00.1.02.02.16.11', 'Kegiatan Fasilitasi Peringatan Hari Jadi Kabupaten/Provinsi/Nasional dan Hari Besar Lainnya'),
(25, '0.00.1.02.02.16.01', 'Kegiatan Penyediaan Barang dan Jasa Perkantoran'),
(26, '0.00.1.02.02.16.08', 'Kegiatan Penyusunan Perencanaan dan Informasi Perangkat Daerah');

-- --------------------------------------------------------

--
-- Table structure for table `koderekening`
--

CREATE TABLE `koderekening` (
  `idkoderek` int(11) NOT NULL,
  `koderekening` varchar(64) NOT NULL,
  `uraian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `koderekening`
--

INSERT INTO `koderekening` (`idkoderek`, `koderekening`, `uraian`) VALUES
(5, '5.2.1', 'Belanja Pegawai'),
(6, '5.2.1.01', 'Honorium PNS'),
(7, '5.2.1.01.02', 'Honorarium Tim Pengadaan Barang Dan Jasa'),
(9, '5.2.1.01.10', 'Honor Pejabat Pelaksan Teknis Kegiatan'),
(10, '5.2.1.02', 'Honorarium Non Pns'),
(11, '5.2.1.02.02', 'Honor / Uang Transport Kegiatan'),
(12, '5.2.1.01.08', 'Uang Kinerja / Uang Representasi Petugas Khusus'),
(14, '5.2.2.01.01', 'Belanja Alat Tulis Kantor'),
(15, '5.2.2.01.10', 'Belanja Alat Kesehatan Pakai Habis'),
(16, '5.2.2.06.02', 'Belanja Penggandaan'),
(17, '5.2.2.11.01', 'Belanja Makanan dan Minuman Harian Pegawai'),
(18, '5.2.3.19.15', 'Belanja Modal Alat Kesehatan'),
(19, '5.2.2.49.01', 'Belanja Jasa Tenaga Ahli'),
(20, '5.2.2.06.01', 'Belanja Cetak'),
(21, '5.2.2.43.01', 'Belanja Jasa Konsultasi Penelitian '),
(22, '5.2.1.01.03', 'Honor/Uang Transport Kegiatan '),
(23, '5.2.2.11.02', 'Belanja Makanan dan Minuman Rapat'),
(24, '5.2.1.01.08', 'Uang Kinerja / Uang Representasi Petugas Khusus'),
(25, '5.2.3.11.11', 'Belanja Modal Skat / Pembatas Ruangan'),
(26, '5.2.1.06.01', 'Belanja Pegawai BLUD Rumah Sakit'),
(27, '5.2.2.47.01', 'Belanja Barang dan Jasa BLUD Rumah Sakit'),
(28, '5.2.3.34.01', 'Belanja Modal BLUD Rumah Sakit'),
(29, '5.2.2.17.04', 'Belanja Keikutsertaan'),
(30, '5.2.2.03.16', 'Belanja Dekorasi'),
(31, '5.2.2.07.03', 'Belanja Sewa Stand'),
(32, '5.2.2.17.02', 'Belanja Sosialisasi'),
(33, '5.2.1.01.04', 'Honor Bendahara, PPK, Pembantu Bendahara'),
(34, '5.2.1.01.05', 'Honor Perencana'),
(35, '5.2.1.02.01', 'Honorarium Pegawai Honorer / tidak tetap'),
(36, '5.2.1.02.04', 'Honor Operator SKPD'),
(37, '5.2.2.46.01', 'Belanja Jasa Kegiatan Kesehatan/Rumah Sakit/LSM'),
(38, '5.2.1.01.07', 'uang sidang / uang rapat'),
(40, '5.2.1.01.04', 'Honor Bendahara, PPK, Pembantu Bendahara'),
(41, '5.2.1.01.05', 'Honor Perencana /Operator SKPD'),
(42, '5.2.1.01.10', 'Honor PPTK'),
(43, '5.2.1.02.01', 'Honorarium Pegawai Honorer / tidak tetap'),
(44, '5.2.2.17.04', 'Honor Operator / Penata usahaan (SAPA)');

-- --------------------------------------------------------

--
-- Table structure for table `pelayanan`
--

CREATE TABLE `pelayanan` (
  `idpelayanan` int(11) NOT NULL,
  `namalayanan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pelayanan`
--

INSERT INTO `pelayanan` (`idpelayanan`, `namalayanan`) VALUES
(1, 'Pelayanan Medik Dasar'),
(2, 'Pelayanan Medik Gigi'),
(4, 'Pelayanan KIA/KB'),
(5, 'Pelayanan Gawat Darurat Umum 24 jam & 7 hari seminggu'),
(6, 'Penyakit dalam'),
(7, 'Kesehatan anak'),
(8, 'Bedah'),
(9, 'Obstetri dan ginekologi'),
(10, 'Anestesi'),
(11, 'Radiologi'),
(12, 'Patologi Klinik'),
(13, 'Patologi Anatomi'),
(14, 'Rehabilitasi Medik'),
(15, 'Mata'),
(16, 'Paru'),
(17, 'Kulit dan Kelamin'),
(18, 'Kedokteran Jiwa / Psikiatri/ Psikogeriatri/ NAPZA'),
(19, 'Orthopedi'),
(20, 'Telinga Hidung Tenggorok Kepala Leher'),
(21, 'Saraf'),
(22, 'Jantung dan Pembuluh Darah'),
(23, 'Kedokteran Forensik'),
(24, 'Urologi'),
(25, 'Dialisis/ CAPD'),
(26, 'Bedah Katarak'),
(27, 'Pelayanan kesehatan tumbuh kembang anak dan remaja'),
(28, 'Pelayanan kesehatan jiwa dewasa'),
(29, 'Pelayanan kesehatan jiwa lansia'),
(30, 'Pelayanan psikologi dan psikometri'),
(31, 'Pelayanan kesehatan jiwa masyarakat'),
(32, 'Pelayanan konseling dan psikoterapi'),
(33, 'Bedah Mulut'),
(34, 'Konservasi / endodonsi'),
(35, 'Orthodonti'),
(36, 'Periodonti'),
(37, 'Penyakit Mulut'),
(38, 'Bedah Saraf'),
(39, 'Perinatologi'),
(40, 'Epilepsi'),
(41, 'CT Scan'),
(42, 'Elektromedik diagnostik (EKG/EEG/EEG Brain Mapping)'),
(43, 'Pelayanan farmasi'),
(44, 'Bank darah'),
(45, 'Sterilisasi/CSSD'),
(46, 'Rekam medik'),
(47, 'Pemeliharaan Sarana, Prasarana dan fasilitas'),
(48, 'Pengelolaan limbah / kesehatan lingkungan'),
(49, 'Sistem informasi dan komunikasi/SIRS/IT'),
(50, 'Pemulasaran jenazah'),
(51, 'Akupuntur'),
(52, 'Geriatri'),
(53, 'Bedah Anak'),
(54, 'Pelayanan Intensif PICU'),
(55, 'Emergensi');

-- --------------------------------------------------------

--
-- Table structure for table `pengunjungirja`
--

CREATE TABLE `pengunjungirja` (
  `idpengunjung` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `namalayanan` varchar(64) NOT NULL,
  `laki` int(11) NOT NULL,
  `perempuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengunjungirja`
--

INSERT INTO `pengunjungirja` (`idpengunjung`, `nomorbulan`, `bulan`, `tahun`, `namalayanan`, `laki`, `perempuan`) VALUES
(4, 8, 'Agustus', '2020', 'Bedah', 155, 123),
(5, 8, 'Agustus', '2020', 'Kesehatan Anak', 107, 78),
(6, 8, 'Agustus', '2020', 'Bedah Syaraf', 21, 16);

-- --------------------------------------------------------

--
-- Table structure for table `pengunjungirna`
--

CREATE TABLE `pengunjungirna` (
  `idpengunjung` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `namalayanan` varchar(64) NOT NULL,
  `laki` int(11) NOT NULL,
  `perempuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengunjungirna`
--

INSERT INTO `pengunjungirna` (`idpengunjung`, `nomorbulan`, `bulan`, `tahun`, `namalayanan`, `laki`, `perempuan`) VALUES
(1, 1, 'Januari', '2020', 'Penyakit Dalam', 549, 729),
(4, 8, 'Agustus', '2020', 'Bedah', 150, 87);

-- --------------------------------------------------------

--
-- Table structure for table `pengunjungrs`
--

CREATE TABLE `pengunjungrs` (
  `idpengunjung` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `barul` int(11) NOT NULL,
  `barup` int(11) NOT NULL,
  `lamal` int(11) NOT NULL,
  `lamap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengunjungrs`
--

INSERT INTO `pengunjungrs` (`idpengunjung`, `nomorbulan`, `bulan`, `tahun`, `barul`, `barup`, `lamal`, `lamap`) VALUES
(1, 1, 'Januari', '2020', 3066, 2282, 4357, 2905),
(5, 2, 'Februari', '2020', 2868, 2149, 4343, 2896),
(6, 3, 'Maret', '2020', 2610, 2047, 3869, 2580),
(7, 4, 'April', '2020', 1252, 1069, 2153, 1436),
(8, 5, 'Mei', '2020', 1140, 1024, 1752, 1168),
(9, 6, 'Juni', '2020', 1071, 910, 2244, 1497),
(10, 7, 'Juli', '2020', 1098, 915, 2212, 1475),
(11, 8, 'Agustus', '2020', 1168, 953, 1845, 1230);

-- --------------------------------------------------------

--
-- Table structure for table `pokja`
--

CREATE TABLE `pokja` (
  `idprogram` int(11) NOT NULL,
  `idkode` int(11) NOT NULL,
  `indikator` varchar(255) NOT NULL,
  `capaianawal` varchar(64) NOT NULL,
  `t1t` varchar(64) NOT NULL,
  `t1r` bigint(12) NOT NULL,
  `t2t` varchar(64) NOT NULL,
  `t2r` bigint(12) NOT NULL,
  `t3t` varchar(64) NOT NULL,
  `t3r` bigint(12) NOT NULL,
  `t4t` varchar(64) NOT NULL,
  `t4r` bigint(12) NOT NULL,
  `t5t` varchar(64) NOT NULL,
  `t5r` bigint(12) NOT NULL,
  `targetakhir` varchar(64) NOT NULL,
  `realisasiakhir` bigint(12) NOT NULL,
  `idunitkerja` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pokja`
--

INSERT INTO `pokja` (`idprogram`, `idkode`, `indikator`, `capaianawal`, `t1t`, `t1r`, `t2t`, `t2r`, `t3t`, `t3r`, `t4t`, `t4r`, `t5t`, `t5r`, `targetakhir`, `realisasiakhir`, `idunitkerja`) VALUES
(1, 1, 'Presentase Capaian Layanan Kesehatan Kepada Masyarakat', '100%', '100%', 500000, '100%', 500000, '100%', 500000, '100%', 500000, '100%', 500000, '100%', 2500000000, 1),
(3, 5, 'Persentase Penyelenggaraan Penyediaan Fasilitas Kesehatan', '100%', '100%', 4500000000, '100%', 6500000000, '100%', 4900000000, '100%', 5980000000, '100%', 5980000000, '100%', 26260000000, 2),
(4, 7, 'Persentase terlaksananya peningkatan pelayanan', '100%', '100%', 201579100, '100%', 550000000, '100%', 550000000, '100%', 750000000, '100%', 550000000, '100%', 2601579100, 8),
(5, 9, 'Persentase dokumen laporan peningkatan mutu pelayanan penunjang rumah sakit yang tersusun', '100%', '100%', 64670900, '100%', 64670900, '100%', 64670900, '100%', 64670900, '100%', 64670900, '100%', 323354500, 11),
(6, 11, 'Persentase peningkatan mutu pelayanan keperawatan', '100%', '100%', 26000000, '100%', 26000000, '100%', 26000000, '100%', 26000000, '100%', 26000000, '100%', 130000000, 2),
(8, 13, 'Persentase ketersediaan sarana dan prasarana rumah sakit yang terpenuhi', '100%', '100%', 12757496000, '100%', 12757496000, '100%', 12757496000, '100%', 12757496000, '100%', 12757496000, '100%', 63787480000, 8),
(9, 15, 'Persentase kemampuan keuangan RSUD dengan prinsip BLUD', '100%', '100%', 112830000000, '100%', 112969586466, '100%', 114616705995, '100%', 117232033707, '100%', 122229931828, '100%', 579878257996, 17),
(10, 20, 'Persentase terpenuhinya pelayanan', '100%', '100%', 3555150000, '100%', 3799250000, '100%', 3804250000, '100%', 3804450000, '100%', 3804450000, '100%', 18767550000, 13);

-- --------------------------------------------------------

--
-- Table structure for table `raker`
--

CREATE TABLE `raker` (
  `idraker` int(11) NOT NULL,
  `idkode` int(11) NOT NULL,
  `t1` bigint(12) NOT NULL,
  `t2` bigint(12) NOT NULL,
  `t3` bigint(12) NOT NULL,
  `t4` bigint(12) NOT NULL,
  `t5` bigint(12) NOT NULL,
  `akhir` bigint(12) NOT NULL,
  `idpokja` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `raker`
--

INSERT INTO `raker` (`idraker`, `idkode`, `t1`, `t2`, `t3`, `t4`, `t5`, `akhir`, `idpokja`) VALUES
(1, 2, 127500000, 0, 127500000, 127500000, 127500000, 637500000, 1),
(2, 3, 222500000, 10000000, 222500000, 222500000, 222500000, 1112500000, 1),
(3, 4, 150000000, 0, 150000000, 150000000, 150000000, 750000000, 1),
(7, 6, 4500000000, 6500000000, 4900000000, 5980000000, 5980000000, 26260000000, 3),
(8, 8, 201579100, 59111500, 550000000, 750000000, 550000000, 2601579100, 4),
(9, 10, 5200000, 250000, 5200000, 5200000, 5200000, 26000000, 5),
(10, 18, 59470900, 59471000, 59470900, 59470900, 59470900, 297354500, 5),
(11, 19, 23000000, 19500000, 23000000, 23000000, 23000000, 115000000, 6),
(12, 12, 3000000, 11500000, 3000000, 3000000, 3000000, 15000000, 6),
(13, 14, 12757496000, 12757496000, 12757496000, 12757496000, 12757496000, 63787480000, 7),
(14, 14, 12757496000, 12199927700, 12757496000, 12757496000, 12757496000, 63787480000, 8),
(15, 16, 112830000000, 128748000000, 114616705995, 117232033707, 122229931828, 579878257996, 9),
(16, 21, 0, 62706000, 265000000, 265000000, 250000000, 980000000, 10),
(17, 22, 5200000, 2491500, 5200000, 5200000, 5200000, 26000000, 10),
(18, 23, 5200000, 250000, 5200000, 5200000, 5200000, 26000000, 10),
(19, 24, 15200000, 0, 25000000, 25000000, 25000000, 115200000, 10),
(20, 25, 3504350000, 2863800000, 3493850000, 3493850000, 3493850000, 17539750000, 10),
(21, 26, 25200000, 10000000, 10200000, 10200000, 25200000, 80800000, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tenirja`
--

CREATE TABLE `tenirja` (
  `idten` int(11) NOT NULL,
  `kodeicd` varchar(64) NOT NULL,
  `deskripsi` varchar(64) NOT NULL,
  `hidupl` int(11) NOT NULL,
  `hidupp` int(11) NOT NULL,
  `matil` int(11) NOT NULL,
  `matip` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tenirja`
--

INSERT INTO `tenirja` (`idten`, `kodeicd`, `deskripsi`, `hidupl`, `hidupp`, `matil`, `matip`, `nomorbulan`, `bulan`, `tahun`) VALUES
(1, 'K04.1', 'Peny. Pulpa', 68, 75, 75, 75, 1, 'Januari', '2020');

-- --------------------------------------------------------

--
-- Table structure for table `tenirna`
--

CREATE TABLE `tenirna` (
  `idten` int(11) NOT NULL,
  `kodeicd` varchar(64) NOT NULL,
  `deskripsi` varchar(64) NOT NULL,
  `hidupl` int(11) NOT NULL,
  `hidupp` int(11) NOT NULL,
  `matil` int(11) NOT NULL,
  `matip` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tenirna`
--

INSERT INTO `tenirna` (`idten`, `kodeicd`, `deskripsi`, `hidupl`, `hidupp`, `matil`, `matip`, `nomorbulan`, `bulan`, `tahun`) VALUES
(1, 'R50.9', 'Febris', 68, 75, 75, 75, 1, 'Januari', '2020');

-- --------------------------------------------------------

--
-- Table structure for table `triwulanan`
--

CREATE TABLE `triwulanan` (
  `idtriwulanan` int(11) NOT NULL,
  `idindikatorkerja` int(11) NOT NULL,
  `alokasi` int(11) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `jenistw` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `triwulanan`
--

INSERT INTO `triwulanan` (`idtriwulanan`, `idindikatorkerja`, `alokasi`, `tahun`, `jenistw`) VALUES
(8, 13, 400000, '2020', 'TW1'),
(9, 1, 0, '2020', 'TW1'),
(10, 1, 0, '2020', 'TW2'),
(11, 1, 0, '2020', 'TW3'),
(12, 1, 127500000, '2020', 'TW4');

-- --------------------------------------------------------

--
-- Table structure for table `ttrs`
--

CREATE TABLE `ttrs` (
  `idtt` int(11) NOT NULL,
  `kelas` varchar(64) NOT NULL,
  `jumlah` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ttrs`
--

INSERT INTO `ttrs` (`idtt`, `kelas`, `jumlah`) VALUES
(1, 'VVIP/Suoer VIP', '4'),
(5, 'VIP', '4'),
(6, 'I', '54'),
(7, 'II', '24'),
(8, 'III', '127'),
(9, 'Non Kelas', '14'),
(10, 'Isolasi', '23'),
(11, 'ICU', '10'),
(12, 'Perinatologi', '28');

-- --------------------------------------------------------

--
-- Table structure for table `unitkerja`
--

CREATE TABLE `unitkerja` (
  `idunitkerja` int(11) NOT NULL,
  `namaunit` varchar(256) NOT NULL,
  `keterangan` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `unitkerja`
--

INSERT INTO `unitkerja` (`idunitkerja`, `namaunit`, `keterangan`) VALUES
(1, 'Yankep', 'kabid keperawatan'),
(2, 'Yankep', 'kasie renbang keperawatan'),
(3, 'Yanjang', 'kasubbag penunjang non medik'),
(5, 'Yanmed', 'kabid yanmed'),
(7, 'Yanmed', 'kasie renbang yanmed'),
(8, 'Yanmed', 'kasie wasdal yanmed'),
(9, 'Yanjang', 'kabid yanjang'),
(10, 'Yanjang', 'kasie penunjang non medik'),
(11, 'Yanjang', 'kasie penunjang medik'),
(12, 'yankep', 'kasie wasdal keperawatan'),
(13, 'Keuangan', 'kabag keuangan'),
(14, 'Keuangan', 'kasubbag akuntansi'),
(15, 'Tata Usaha', 'kasubbag kepegawaian'),
(16, 'Sungram', 'kabag sungram'),
(17, 'Sungram', 'kasubbag evapor'),
(18, 'Sungram', 'kasubbag sungram'),
(19, 'Tata Usaha', 'kasubbag umum');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `nama` varchar(256) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `idunitkerja` int(11) NOT NULL,
  `jabatan` varchar(64) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'o ruangan 1 admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `nama`, `username`, `password`, `idunitkerja`, `jabatan`, `status`) VALUES
(1, 'Administrator ', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'Administrator', 1),
(3, 'HUSNUL KHOTIMAH, M.Sc.,Apt', 'Admin', 'c21f969b5f03d33d43e04f8f136e7682', 16, 'Kasubbag RSUD', 0),
(4, 'IRA ROSARIA TAUSARIA, S.Kep.Ns.,M.Si', 'Yankep02', 'c21f969b5f03d33d43e04f8f136e7682', 2, 'Kasubid RSUD', 0),
(6, 'MOH. TAUFIQ, SKM.,M.Kes', 'Admin', 'c21f969b5f03d33d43e04f8f136e7682', 16, 'Kabag RSUD', 0),
(7, 'ISNAINI HASAN ALI W, AKL', 'RM01', 'c21f969b5f03d33d43e04f8f136e7682', 1, 'Kasubbag RSUD', 0),
(9, 'DWI HANDAYANI, SH', 'YANMEN', 'c21f969b5f03d33d43e04f8f136e7682', 5, 'KASUBAG RSUD', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dataibubayi`
--
ALTER TABLE `dataibubayi`
  ADD PRIMARY KEY (`idibu`);

--
-- Indexes for table `datajiwa`
--
ALTER TABLE `datajiwa`
  ADD PRIMARY KEY (`idjiwa`);

--
-- Indexes for table `datars`
--
ALTER TABLE `datars`
  ADD PRIMARY KEY (`idrs`);

--
-- Indexes for table `dpa`
--
ALTER TABLE `dpa`
  ADD PRIMARY KEY (`iddpa`);

--
-- Indexes for table `fasilitastt`
--
ALTER TABLE `fasilitastt`
  ADD PRIMARY KEY (`idfasilitas`);

--
-- Indexes for table `indikatorkerja`
--
ALTER TABLE `indikatorkerja`
  ADD PRIMARY KEY (`idindikatorkerja`);

--
-- Indexes for table `indikatorlayanan`
--
ALTER TABLE `indikatorlayanan`
  ADD PRIMARY KEY (`idindikatorlayanan`);

--
-- Indexes for table `kodeprogram`
--
ALTER TABLE `kodeprogram`
  ADD PRIMARY KEY (`idkode`);

--
-- Indexes for table `koderekening`
--
ALTER TABLE `koderekening`
  ADD PRIMARY KEY (`idkoderek`);

--
-- Indexes for table `pelayanan`
--
ALTER TABLE `pelayanan`
  ADD PRIMARY KEY (`idpelayanan`);

--
-- Indexes for table `pengunjungirja`
--
ALTER TABLE `pengunjungirja`
  ADD PRIMARY KEY (`idpengunjung`);

--
-- Indexes for table `pengunjungirna`
--
ALTER TABLE `pengunjungirna`
  ADD PRIMARY KEY (`idpengunjung`);

--
-- Indexes for table `pengunjungrs`
--
ALTER TABLE `pengunjungrs`
  ADD PRIMARY KEY (`idpengunjung`);

--
-- Indexes for table `pokja`
--
ALTER TABLE `pokja`
  ADD PRIMARY KEY (`idprogram`);

--
-- Indexes for table `raker`
--
ALTER TABLE `raker`
  ADD PRIMARY KEY (`idraker`);

--
-- Indexes for table `tenirja`
--
ALTER TABLE `tenirja`
  ADD PRIMARY KEY (`idten`);

--
-- Indexes for table `tenirna`
--
ALTER TABLE `tenirna`
  ADD PRIMARY KEY (`idten`);

--
-- Indexes for table `triwulanan`
--
ALTER TABLE `triwulanan`
  ADD PRIMARY KEY (`idtriwulanan`);

--
-- Indexes for table `ttrs`
--
ALTER TABLE `ttrs`
  ADD PRIMARY KEY (`idtt`);

--
-- Indexes for table `unitkerja`
--
ALTER TABLE `unitkerja`
  ADD PRIMARY KEY (`idunitkerja`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dataibubayi`
--
ALTER TABLE `dataibubayi`
  MODIFY `idibu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `datajiwa`
--
ALTER TABLE `datajiwa`
  MODIFY `idjiwa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `datars`
--
ALTER TABLE `datars`
  MODIFY `idrs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dpa`
--
ALTER TABLE `dpa`
  MODIFY `iddpa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `fasilitastt`
--
ALTER TABLE `fasilitastt`
  MODIFY `idfasilitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `indikatorkerja`
--
ALTER TABLE `indikatorkerja`
  MODIFY `idindikatorkerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `indikatorlayanan`
--
ALTER TABLE `indikatorlayanan`
  MODIFY `idindikatorlayanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kodeprogram`
--
ALTER TABLE `kodeprogram`
  MODIFY `idkode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `koderekening`
--
ALTER TABLE `koderekening`
  MODIFY `idkoderek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `pelayanan`
--
ALTER TABLE `pelayanan`
  MODIFY `idpelayanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `pengunjungirja`
--
ALTER TABLE `pengunjungirja`
  MODIFY `idpengunjung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pengunjungirna`
--
ALTER TABLE `pengunjungirna`
  MODIFY `idpengunjung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pengunjungrs`
--
ALTER TABLE `pengunjungrs`
  MODIFY `idpengunjung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `pokja`
--
ALTER TABLE `pokja`
  MODIFY `idprogram` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `raker`
--
ALTER TABLE `raker`
  MODIFY `idraker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tenirja`
--
ALTER TABLE `tenirja`
  MODIFY `idten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tenirna`
--
ALTER TABLE `tenirna`
  MODIFY `idten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `triwulanan`
--
ALTER TABLE `triwulanan`
  MODIFY `idtriwulanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ttrs`
--
ALTER TABLE `ttrs`
  MODIFY `idtt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `unitkerja`
--
ALTER TABLE `unitkerja`
  MODIFY `idunitkerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
