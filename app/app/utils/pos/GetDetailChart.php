<?php
session_start();
include("../../config.php");
include("../../library/mylib.php");
$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("connection failed".mysqli_errno());
$idt = $_GET['idt'];

$sql ="SELECT * FROM `transaksi` WHERE idtransaksi =".$idt;
$resulttrans =mysqli_query($con,$sql);
$rowtrans = mysqli_fetch_assoc($resulttrans);

$date = date('d-m-Y',strtotime($rowtrans['tglorder']));
$time = date('H:i:s',strtotime($rowtrans['tglorder']));

$sql ="SELECT * FROM transaksidetail, produk WHERE transaksidetail.idproduk = produk.idproduk and transaksidetail.idtransaksi =".$idt;
$result =mysqli_query($con,$sql);
?>

  <table class="table table-bordered">
    <tr>
      <th colspan="5">No. Invoices : <?php echo $rowtrans['invoice'] ?></th>
    </tr>
    <tr>
      <td colspan="3">No. Meja : <?php echo $rowtrans['nomeja'] ?></td>
      <td colspan="2">Tanggal : <?php echo $date ?></td>
    </tr>
    <tr>
      <td colspan="3">Atas Nama : <?php echo $rowtrans['atasnama'] ?></td>
      <td colspan="2">Jam : <?php echo $time ?></td>
    </tr>
    <tr>
      <td colspan="5">Total Belanja : <b> <?php echo rupiah((int)$rowtrans['totalbelanja']); ?> </b> </td>
    </tr>
    <tr>
      <td>No</td>
      <td>Nama</td>
      <td>Qty</td>
      <td>Harga</td>
      <td>Total</td>
    </tr>
    <?php
    $jml = 0;
    $no = 1;
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
    ?>
    <tr style="border-bottom: 1px solid grey">
      <td><?php echo $no; ?></td>
      <td><?php echo $row["namaproduk"]; ?></td>
      <td><?php echo $row["qty"]; ?></td>
      <td><?php echo $row["hargajual"]; ?></td>
      <td><?php echo rupiah((int)$row["hargajual"] * $row["qty"]); ?></td>
    </tr>
    <?php
    $jml = $jml + ($row["hargajual"] * $row["qty"]);
    $no++;
    }
    ?>
    <tr>
      <td colspan="4">Sub Total</td>
      <td><?php echo rupiah((int)$jml) ?></td>
    </tr>
    <tr>
      <td colspan="4">Discount (<?php echo $rowtrans['discountpercent'] ?> %) </td>
      <td><?php echo rupiah((int)$rowtrans['discount']) ?></td>
    </tr>
    <tr>
      <td colspan="4">Ppn (10%)</td>
      <td><?php echo rupiah((int)$rowtrans['tax']) ?></td>
    </tr>
    <tr>
      <td colspan="4">Total</td>
      <td><?php echo rupiah((int)$rowtrans['total']) ?></td>
    </tr>
  </table>
