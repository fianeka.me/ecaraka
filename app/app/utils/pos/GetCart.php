<?php
session_start();
include("../../config.php");
$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("connection failed".mysqli_errno());
$idt = $_GET['id'];

$sql ="SELECT * FROM `transaksitemp`, produk WHERE transaksitemp.idproduct = produk.idproduk and transaksitemp.idtransaksi =".$idt;
$result=mysqli_query($con,$sql);
?>


<?php if (mysqli_num_rows($result) > 0): ?>
  <table class="table table-borderless">
    <thead>
      <tr style="border-bottom: 1px solid grey">
        <th scope="col">Menu</th>
        <th scope="col">Harga</th>
        <th scope="col">Qty</th>
        <th scope="col">Total</th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
      <?php
      $jml = 0;
      // output data of each row
      while($row = mysqli_fetch_assoc($result)) {
      ?>
      <tr style="border-bottom: 1px solid grey">
        <td><?php echo $row["namaproduk"]; ?></td>
        <td><?php echo $row["hargajual"]; ?></td>
        <!-- <td> <a id="data-tes" type="button" name="min"><i class="fa fa-minus"></i></a>  <a class="m-2" href="#"> <?php echo $row["qty"]; ?> </a>  <a type="button" id="changedata" name="plus"><i class="fa fa-plus"></i></a></td> -->
        <td><a class="m-2" href="#"> <?php echo $row["qty"]; ?> </a></td>
        <td><?php echo $row["hargajual"] * $row["qty"]; ?></td>
        <td><a class="deldata" href="#" id="<?php echo $row["idordertemp"]."-".$row["idtransaksi"]; ?>"><i class="fa fa-trash"></i></a></td>
      </tr>
      <?php
      $jml = $jml + ($row["hargajual"] * $row["qty"]);
      }
      ?>
    </tbody>
  </table>
  <input type="hidden" id="jml" name="jml" value="<?php echo $jml; ?>">
<?php else: ?>
  Belum Ada Pesanan Di Transaksi Ini
<?php endif; ?>

<script type="text/javascript">
  var bla = $('#jml').val();
  $('#sbtotal').val(bla);
  $('#jumlahbarang').val(<?php echo $jml; ?>);

  $('.deldata').click(function(){
    // console.log(this.id);
    // var datanya = {idtemp : this.id};
    var data = this.id.split("-");
    var datanya = {idtemp : data[0]};
    // console.log(datanya);
    $.ajax({
    	type: 'GET',
      url: "utils/pos/DelCart.php",
    	data: datanya,
    	success: function(response) {
    		$('.cartview').load("utils/pos/GetCart.php?id="+data[1]);
        $('#sbtotal').val(bla);
    	}
    });
  });

</script>
