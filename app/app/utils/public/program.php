<?php
session_start();
// if (isset($_SESSION['toprint'])) {
  $toprint = $_SESSION['toprint'];
  include '../../library/mylib.php';

// }
?>
<pre>
  <!-- <?php print_r($toprint); ?> -->
</pre>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Laporan Program</title>
  <!-- <link rel="stylesheet" href="table.css"> -->
  <style>

    body {
        background: #fff;
        font-size: 18px;
    }

    table {
     border-collapse: collapse;
    }

    table, th, td {
     border: 1px solid black;
     padding: 8px;
    }

    .content {
/* 72 dpi (web) = 595 X 842 pixels */
        height: 500px;
        width: 2200px;
        margin: auto;
        background: white;
        padding: 10px;
    }
    hr.style15 {
      border-top: 6px double #8c8b8b;
    }
    #prety {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        /* width: 100%; */
    }

    #prety td, #prety th {
        /* border: 1px solid #ddd; */
        padding: 7px;
    }
  </style>
</head>
<body>
  <?php if (isset($_SESSION['toprint'])): ?>
    <div class="content">
      <!-- <table width="100%" border="0">
        <tr>
          <td valign="bottom" rowspan="5"><img src="../../resources/images/logor.jpg" height="100" width="150" alt=""></td>
          <td valign="bottom" colspan="3" style="padding-left:10px;">
            <h2 style="margin:2px;font-size:25px;">
              <b><u>RSUD SYAMRABU</u></b>
            </h2>
          </td>
          <td valign="bottom" rowspan="5"><img src="../../resources/images/iso.png" height="100" width="150" alt=""></td>
        </tr>
        <tr valign="top" style="padding-left:15px;">
          <td style="padding-left:15px;font-size:12px;"><b></b></td>
          <td style="font-size:12px;">:</td>
          <td style="font-size:12px;"></td>
        </tr>
        <tr valign="top" style="padding-left:15px;">
          <td style="padding-left:15px;font-size:12px;"><b></b></td>
          <td style="font-size:12px;">:</td>
          <td style="font-size:12px;">9776</td>
        </tr>
        <tr valign="top" style="padding-left:15px;">
          <td style="padding-left:15px;font-size:12px;"><b></b></td>
          <td style="font-size:12px;">:</td>
          <td style="font-size:12px;"></td>
        </tr>
      </table> -->
      <hr class="style15">
      <?php
        $date = date('Y-m-d');
      ?>
      <center><u><h2>DATA LAPORAN PROGRAM</h2></u></center>
        <table border="1">
        <thead>
          <tr>
            <th rowspan="3">Kode Rekening</th>
            <th rowspan="3">Program Dan Kegiatan</th>
            <th rowspan="3">Indikator Kinerja</th>
            <?php
              for ($x = 2019; $x <= $toprint["tahunberjalan"]; $x++) {
            ?>
            <?php if ($toprint["tahunberjalan"] == $x): ?>
              <th colspan="11"><?php echo $x ?></th>
            <?php else: ?>
              <th colspan="3"><?php echo $x ?></th>
            <?php endif; ?>

            <?php
              }
            ?>
            <th rowspan="3">Unit Kerja</th>
          </tr>
          <tr>
            <?php
              for ($x = 2019; $x <= $toprint["tahunberjalan"]; $x++) {
            ?>
            <?php if ($toprint["tahunberjalan"] == $x): ?>
              <td colspan="2">TW1</td>
              <td colspan="2">TW2</td>
              <td colspan="2">TW3</td>
              <td colspan="2">TW4</td>
            <?php endif; ?>
              <td rowspan="2">Belanja</td>
              <td rowspan="2">Sisa</td>
              <td rowspan="2">(%)</td>
            <?php
              }
            ?>
          </tr>
          <tr>
            <?php
              for ($x = 2019; $x <= $toprint["tahunberjalan"]; $x++) {
            ?>
            <?php if ($toprint["tahunberjalan"] == $x): ?>
              <td>Rp</td>
              <td>%</td>
              <td>Rp</td>
              <td>%</td>
              <td>Rp</td>
              <td>%</td>
              <td>Rp</td>
              <td>%</td>
            <?php endif; ?>

            <?php
              }
            ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($toprint["data"] as $usr): ?>
            <tr>
              <td>
                <?php echo $usr["program"]->kodeprogram; ?>
              </td>
              <td>
                <?php echo $usr["program"]->namaprogram; ?>
              </td>
              <td>
                <?php echo $usr["program"]->indikator; ?>
              </td>
              <?php
                for ($x = 2019; $x <= $toprint["tahunberjalan"]; $x++) {
              ?>
              <?php if ($x==2019): ?>
                <td colspan="3">
                  <?php echo rupiah($usr["program"]->t1r); ?>
                </td>
              <?php endif; ?>
              <?php if ($x==2020): ?>
                <td colspan="11">
                  <?php echo rupiah($usr["program"]->t2r); ?>
                </td>
              <?php endif; ?>
              <?php if ($x==2021): ?>
                <td colspan="11">
                  <?php echo rupiah($usr["program"]->t3r); ?>
                </td>
              <?php endif; ?>
              <?php if ($x==2022): ?>
                <td colspan="11">
                  <?php echo rupiah($usr["program"]->t4r); ?>
                </td>
              <?php endif; ?>
              <?php if ($x==2023): ?>
                <td colspan="11">
                  <?php echo rupiah($usr["program"]->t5r); ?>
                </td>
              <?php endif; ?>
              <?php
                }
              ?>
              <td>
                <?php echo $usr["program"]->namaunit; ?>
              </td>
            </tr>
            <?php foreach ($usr["raker"] as $usr1): ?>
              <tr>
                <td>
                  <?php echo $usr1["raker"]->kodeprogram; ?>
                </td>
                <td colspan="2">
                  <?php echo $usr1["raker"]->namaprogram; ?>
                </td>
                <?php
                  for ($x = 2019; $x <= $toprint["tahunberjalan"]; $x++) {
                ?>
                <?php if ($x==2019): ?>
                  <td colspan="3">
                    <?php echo rupiah($usr1["raker"]->t1); ?>
                  </td>
                <?php endif; ?>
                <?php if ($x==2020): ?>
                  <td colspan="11">
                    <?php echo rupiah($usr1["raker"]->t2); ?>
                  </td>
                <?php endif; ?>
                <?php if ($x==2021): ?>
                  <td colspan="11">
                    <?php echo rupiah($usr1["raker"]->t3); ?>
                  </td>
                <?php endif; ?>
                <?php if ($x==2022): ?>
                  <td colspan="11">
                    <?php echo rupiah($usr1["raker"]->t4); ?>
                  </td>
                <?php endif; ?>
                <?php if ($x==2023): ?>
                  <td colspan="11">
                    <?php echo rupiah($usr1["raker"]->t5); ?>
                  </td>
                <?php endif; ?>
                <?php
                  }
                ?>
                <td></td>
              </tr>
              <?php $datatahunan = array('2019' => $usr1["raker"]->t1, '2020' => $usr1["raker"]->t2, '2021' => $usr1["raker"]->t3, '2022' => $usr1["raker"]->t4, '2025' => $usr1["raker"]->t5 ); ?>
              <?php foreach ($usr1["indikator"] as $usr2): ?>
                <tr>
                  <td></td>
                  <td colspan="2">
                    <?php echo $usr2["indikator"]->indikator; ?>
                  </td>
                  <?php
                    for ($x = 2019; $x <= $toprint["tahunberjalan"]; $x++) {
                  ?>
                  <?php if ($toprint["tahunberjalan"] == $x): ?>
                    <td><?php echo rupiah($usr2["realisasi"][$x]->tw1) ?></td>
                    <td><?php echo round(($usr2["realisasi"][$x]->tw1/$datatahunan[$x])*100,2)."%"; ?></td>
                    <td><?php echo rupiah($usr2["realisasi"][$x]->tw2) ?></td>
                    <td><?php echo round(($usr2["realisasi"][$x]->tw2/$datatahunan[$x])*100,2)."%"; ?></td>
                    <td><?php echo rupiah($usr2["realisasi"][$x]->tw3) ?></td>
                    <td><?php echo round(($usr2["realisasi"][$x]->tw3/$datatahunan[$x])*100,2)."%"; ?></td>
                    <td><?php echo rupiah($usr2["realisasi"][$x]->tw4) ?></td>
                    <td><?php echo round(($usr2["realisasi"][$x]->tw4/$datatahunan[$x])*100,2)."%"; ?></td>
                  <?php endif; ?>
                  <?php $totalbelanja = $usr2["realisasi"][$x]->tw1+$usr2["realisasi"][$x]->tw2+$usr2["realisasi"][$x]->tw3+$usr2["realisasi"][$x]->tw4; ?>
                  <td><?php echo rupiah($totalbelanja) ?></td>
                  <td><?php echo rupiah($datatahunan[$x]-$totalbelanja) ?></td>
                  <td><?php echo round(($totalbelanja/$datatahunan[$x])*100,2)."%" ?></td>

                  <?php
                    }
                  ?>
                  <td>
                    <?php echo $usr2["indikator"]->namaunit; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endforeach; ?>
          <?php endforeach; ?>
        </tbody>
        </table>
        <br><br><br>
    </div>

  <?php endif; ?>

  <script type="text/javascript">
    window.onload = function() { window.print(); }
  </script>
</body>
</html>
