<?php
// Turn off all error reporting
error_reporting(0);
session_start();
include("../config.php");
include("../library/mylib.php");
require __DIR__ . '/Mike42/autoloader.php';
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("connection failed".mysqli_errno());
$tgl = $_GET['idt'];

$sql ="SELECT transaksidetail.idproduk, SUM(transaksidetail.qty) AS jml, produk.hargajual, produk.namaproduk FROM transaksidetail, produk WHERE produk.idproduk = transaksidetail.idproduk AND tgltrans like '".$tgl."%'GROUP BY idproduk";
$result =mysqli_query($con,$sql);

// membuat connector printer ke shared printer bernama "printer_a" (yang telah disetting sebelumnya)
$connector = new Escpos\PrintConnectors\WindowsPrintConnector("Printer2");
// membuat objek $printer agar dapat di lakukan fungsinya
$printer = new Escpos\Printer($connector);


/* Information for the receipt */
$items = array();
$jml = 0;
// output data of each row
while($row = mysqli_fetch_assoc($result)) {
  array_push($items,new item($row["jml"]." x ".$row["namaproduk"], $row["hargajual"] * $row["jml"]));
}


/* Name of shop */
$printer -> selectPrintMode(Escpos\Printer::MODE_DOUBLE_WIDTH);
$printer -> setJustification(Escpos\Printer::JUSTIFY_CENTER);
$printer -> text("LAPORAN PRODUK.\n");
$printer -> selectPrintMode();
$printer -> text($tgl."\n");
$printer -> selectPrintMode();
$printer -> feed();

/* Items */
$printer -> setJustification(Escpos\Printer::JUSTIFY_LEFT);
$printer -> setEmphasis(true);
$printer -> text(new item('', 'Rp.'));
$printer -> setEmphasis(false);
foreach ($items as $item) {
    $printer -> text($item);
}
$printer -> setEmphasis(true);
$printer -> text($subtotal);
$printer -> setEmphasis(false);
$printer -> feed();

$printer -> setEmphasis(true);
$printer -> feed();

/* Cut the receipt and open the cash drawer */
$printer -> cut();
$printer -> pulse();

$printer -> close();
header('Location: '."http://localhost/surapos/app/");


 /* A wrapper to do organise item names & prices into columns */
class item
{
    private $name;
    private $price;
    private $dollarSign;

    public function __construct($name = '', $price = '', $dollarSign = false)
    {
        $this -> name = $name;
        $this -> price = $price;
        $this -> dollarSign = $dollarSign;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 38;
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> name, $leftCols) ;

        $sign = ($this -> dollarSign ? 'Rp. ' : '');
        $right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}
?>
