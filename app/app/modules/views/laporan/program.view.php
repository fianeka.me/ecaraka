<!-- DataTales Example -->
  <div class="row">
    <div class="col-lg-3">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Proses Pelayanan</h6>
        </div>
        <div class="card-body">
          <form id="myForm" role="form" action="<?php echo PATH; ?>?page=laporan-program" method="post">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault01">Pilih Tahun</label>
                  <select class="form-control" name="tahun" id="tahun">
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-block btn-primary">Proses Laporan Pelayanan</button>
            </div>
        </form>
        </div>
    </div>
    <?php if (isset($data["kode"])): ?>
      <div class="col-lg-9">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Laporan Program</h6>
          </div>
          <div class="card-body">
            <a href="<?php echo PATH; ?>?page=laporan-program&&action=reportprint" target="_blank" class="btn btn-success btn-icon-split" type="button">
              <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
              </span>
              <span class="text">Cetak</span>
            </a>
            <hr>
            <div class="accordion" id="accordionExample">
              <?php foreach ($data["kode"]["data"] as $usr): ?>
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#<?php echo "string".$usr["program"]->idprogram; ?>" aria-expanded="true" aria-controls="collapseOne">
                        <?php echo $usr["program"]->kodeprogram." ".$usr["program"]->namaprogram;?>
                      </button>
                    </h2>
                  </div>
                  <div id="<?php echo "string".$usr["program"]->idprogram;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Kode Program</th>
                            <th>:</th>
                            <th><?php echo $usr["program"]->kodeprogram; ?></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Nama Program Dan Kegiatan<br></td>
                            <td>:</td>
                            <td><?php echo $usr["program"]->namaprogram; ?></td>
                          </tr>
                          <tr>
                            <td>Indikator Kinerja Program</td>
                            <td>:</td>
                            <td><?php echo $usr["program"]->indikator; ?></td>
                          </tr>
                          <tr>
                            <td>Data Capaian Pada Tahun Awal</td>
                            <td>:</td>
                            <td><?php echo $usr["program"]->capaianawal; ?></td>
                          </tr>
                          <tr>
                            <td>Penanggung Jawab Unit Kerja</td>
                            <td>:</td>
                            <td><?php echo $usr["program"]->namaunit; ?></td>
                          </tr>
                          <?php
                            for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                          ?>
                          <?php if ($x==2019): ?>
                            <tr>
                              <td><?php echo "Pagu ".$x;?></td>
                              <td>:</td>
                              <td><?php echo rupiah($usr["program"]->t1r); ?></td>
                            </tr>
                          <?php endif; ?>
                          <?php if ($x==2020): ?>
                            <tr>
                              <td><?php echo "Pagu ".$x;?></td>
                              <td>:</td>
                              <td><?php echo rupiah($usr["program"]->t2r); ?></td>
                            </tr>
                          <?php endif; ?>
                          <?php if ($x==2021): ?>
                            <tr>
                              <td><?php echo "Pagu ".$x;?></td>
                              <td>:</td>
                              <td><?php echo rupiah($usr["program"]->t3r); ?></td>
                            </tr>
                          <?php endif; ?>
                          <?php if ($x==2022): ?>
                            <tr>
                              <td><?php echo "Pagu ".$x;?></td>
                              <td>:</td>
                              <td><?php echo rupiah($usr["program"]->t4r); ?></td>
                            </tr>
                          <?php endif; ?>
                          <?php if ($x==2023): ?>
                            <tr>
                              <td><?php echo "Pagu ".$x;?></td>
                              <td>:</td>
                              <td><?php echo rupiah($usr["program"]->t5r); ?></td>
                            </tr>
                          <?php endif; ?>
                          <?php
                            }
                          ?>
                        </tbody>
                        </table>
                      </div>
                      <hr>

                      <!-- <table> -->
                        <?php foreach ($usr["raker"] as $usr1): ?>
                          <div class="card">
                            <div class="card-header">
                              <a href="#">  <?php echo $usr1["raker"]->kodeprogram." ".$usr1["raker"]->namaprogram; ?></a>
                            </div>
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>Kode Program</th>
                                    <th>:</th>
                                    <th><?php echo $usr1["raker"]->kodeprogram; ?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Nama Program Dan Kegiatan<br></td>
                                    <td>:</td>
                                    <td><?php echo $usr1["raker"]->namaprogram; ?></td>
                                  </tr>
                                  <?php
                                    for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                                  ?>
                                  <?php if ($x==2019): ?>
                                    <tr>
                                      <td><?php echo "Pagu ".$x;?></td>
                                      <td>:</td>
                                      <td><?php echo rupiah($usr1["raker"]->t1); ?></td>
                                    </tr>
                                  <?php endif; ?>
                                  <?php if ($x==2020): ?>
                                    <tr>
                                      <td><?php echo "Pagu ".$x;?></td>
                                      <td>:</td>
                                      <td><?php echo rupiah($usr1["raker"]->t2); ?></td>
                                    </tr>
                                  <?php endif; ?>
                                  <?php if ($x==2021): ?>
                                    <tr>
                                      <td><?php echo "Pagu ".$x;?></td>
                                      <td>:</td>
                                      <td><?php echo rupiah($usr1["raker"]->t3); ?></td>
                                    </tr>
                                  <?php endif; ?>
                                  <?php if ($x==2022): ?>
                                    <tr>
                                      <td><?php echo "Pagu ".$x;?></td>
                                      <td>:</td>
                                      <td><?php echo rupiah($usr1["raker"]->t4); ?></td>
                                    </tr>
                                  <?php endif; ?>
                                  <?php if ($x==2023): ?>
                                    <tr>
                                      <td><?php echo "Pagu ".$x;?></td>
                                      <td>:</td>
                                      <td><?php echo rupiah($usr1["raker"]->t5); ?></td>
                                    </tr>
                                  <?php endif; ?>
                                  <?php
                                    }
                                  ?>
                                </tbody>
                                </table>
                              </div>
                              <hr>

                              <?php $datatahunan = array('2019' => $usr1["raker"]->t1, '2020' => $usr1["raker"]->t2, '2021' => $usr1["raker"]->t3, '2022' => $usr1["raker"]->t4, '2023' => $usr1["raker"]->t5 ); ?>
                              <?php foreach ($usr1["indikator"] as $usr2): ?>
                                <div class="alert alert-danger" role="alert">
                                  <b>
                                    <?php echo $usr2["indikator"]->indikator; ?>
                                  </b>
                                </div>
                                <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                      <th>Indikator Kegiatan</th>
                                      <th>:</th>
                                      <th><?php echo $usr2["indikator"]->indikator; ?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>Unit Penangung Jawab<br></td>
                                      <td>:</td>
                                      <td><?php echo $usr2["indikator"]->namaunit; ?></td>
                                    </tr>
                                  </tbody>
                                </table>

                                <div class="row">
                                  <div class="col-2">
                                    <div class="list-group" id="list-tab" role="tablist">
                                      <?php
                                        for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                                      ?>
                                          <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-<?php echo $usr2["indikator"]->idindikatorkerja.$x; ?>" role="tab" aria-controls="profile"><?php echo $x; ?></a>
                                      <?php
                                        }
                                      ?>
                                    </div>

                                  </div>
                                  <div class="col-10">
                                    <div class="tab-content" id="nav-tabContent">
                                      <?php
                                        for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                                      ?>
                                      <div class="tab-pane fade" id="list-<?php echo $usr2["indikator"]->idindikatorkerja.$x; ?>" role="tabpanel" aria-labelledby="list-profile-list">
                                        <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th>Data <?php echo $x ?></th>
                                              <th>Total Belanja</th>
                                              <th>Realisasi (%)</th>
                                            </tr>
                                          </thead>
                                          <?php $totalbelanja = $usr2["realisasi"][$x]->tw1+$usr2["realisasi"][$x]->tw2+$usr2["realisasi"][$x]->tw3+$usr2["realisasi"][$x]->tw4; ?>
                                          <tbody>
                                            <tr>
                                              <td>TW 1</td>
                                              <td><?php echo rupiah($usr2["realisasi"][$x]->tw1) ?></td>
                                              <td><?php echo round(($usr2["realisasi"][$x]->tw1/$datatahunan[$x])*100,2)."%"; ?></td>
                                            </tr>
                                            <tr>
                                              <td>TW 2</td>
                                              <td><?php echo rupiah($usr2["realisasi"][$x]->tw2) ?></td>
                                              <td><?php echo round(($usr2["realisasi"][$x]->tw2/$datatahunan[$x])*100,2)."%"; ?></td>
                                            </tr>
                                            <tr>
                                              <td>TW 3</td>
                                              <td><?php echo rupiah($usr2["realisasi"][$x]->tw3) ?></td>
                                              <td><?php echo round(($usr2["realisasi"][$x]->tw3/$datatahunan[$x])*100,2)."%"; ?></td>
                                            </tr>
                                            <tr>
                                              <td>TW 4</td>
                                              <td><?php echo rupiah($usr2["realisasi"][$x]->tw4) ?></td>
                                              <td><?php echo round(($usr2["realisasi"][$x]->tw4/$datatahunan[$x])*100,2)."%"; ?></td>
                                            </tr>
                                            <tr>
                                              <th>Total Pagu</th>
                                              <th>Total Belanja</th>
                                              <th>Total Realisasi</th>
                                            </tr>
                                            <tr>
                                              <td><?php echo rupiah($datatahunan[$x]) ?></td>
                                              <td><?php echo rupiah($totalbelanja) ?></td>
                                              <td><?php echo round(($totalbelanja/$datatahunan[$x])*100,2)."%" ?></td>
                                            </tr>
                                            <tr>
                                              <th>Sisa Uang</th>
                                              <th colspan="2"><?php echo rupiah($datatahunan[$x]-$totalbelanja) ?></th>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                      <?php
                                        }
                                      ?>
                                    </div>
                                  </div>
                                </div>

                              <?php endforeach; ?>
                            </div>
                          </div>
                          <br><br>

                        <?php endforeach; ?>
                      <!-- </table> -->


                    </div>

                  </div>
                </div>
                <br>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

  </div>
