<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Fasilitas Tempat Tidur Rawat Inap</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Data Fasilitas TT IRNA</span>
      </button>
      <hr>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Jenis Pelayanan</th>
            <th>VVIP</th>
            <th>VIP</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>Khusus</th>
            <th>Jumlah TT</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Jenis Pelayanan</th>
            <th>VVIP</th>
            <th>VIP</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>Khusus</th>
            <th>Jumlah TT</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($data["kode"] as $usr): ?>
            <?php $datas = $usr->idfasilitas."/".$usr->jenispelayanan."/".$usr->vvip
            ."/".$usr->vip."/".$usr->kelas1."/".$usr->kelas2
            ."/".$usr->kelas3."/".$usr->khusus; ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $usr->jenispelayanan; ?>
              </td>
              <td>
                <?php echo $usr->vvip; ?>
              </td>
              <td>
                <?php echo $usr->vip; ?>
              </td>
              <td>
                <?php echo $usr->kelas1; ?>
              </td>
              <td>
                <?php echo $usr->kelas2; ?>
              </td>
              <td>
                <?php echo $usr->kelas3; ?>
              </td>
              <td>
                <?php echo $usr->khusus; ?>
              </td>
              <td>
                <?php echo $usr->vvip+$usr->vip+$usr->kelas1+$usr->kelas2+$usr->kelas3+$usr->khusus; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=layanan-fasilitastt&&action=delete&&id=<?php echo $usr->idfasilitas; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
              <?php $no = $no + 1; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Kode Program</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=layanan-fasilitastt&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-12">
              <input type="hidden" name="id" id="id" class="form-control">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Jenis Pelayanan</label>
                <input type="text" name="jenispelayanan" id="jenispelayanan" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> VVIP </label>
                <input type="text" name="vvip" id="vvip" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> VIP </label>
                <input type="text" name="vip" id="vip" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> Kelas 1 </label>
                <input type="text" name="kelas1" id="kelas1" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> Kelas 2 </label>
                <input type="text" name="kelas2" id="kelas2" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> Kelas 3 </label>
                <input type="text" name="kelas3" id="kelas3" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> Khusus </label>
                <input type="text" name="khusus" id="khusus" class="form-control" required>
              </div>
            </div>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('jenispelayanan').value= data[1];
              document.getElementById('vvip').value= data[2];
              document.getElementById('vip').value= data[3];
              document.getElementById('kelas1').value= data[4];
              document.getElementById('kelas2').value= data[5];
              document.getElementById('kelas3').value= data[6];
              document.getElementById('khusus').value= data[7];
            }

         });
    });
  </script>
