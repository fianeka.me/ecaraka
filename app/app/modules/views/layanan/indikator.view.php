<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Indikator Pelayanan Rumah Sakit</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Data Indikator Layanan</span>
      </button>
      <hr>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Tahun</th>
            <th>BOR</th>
            <th>LOS</th>
            <th>BTO</th>
            <th>TOI</th>
            <th>NDR</th>
            <th>GDR</th>
            <th>Rata Rata Kunjungan</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Tahun</th>
            <th>BOR</th>
            <th>LOS</th>
            <th>BTO</th>
            <th>TOI</th>
            <th>NDR</th>
            <th>GDR</th>
            <th>Rata Rata Kunjungan</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($data["kode"] as $usr): ?>
            <?php $datas = $usr->idindikatorlayanan."/".$usr->tahun
            ."/".$usr->bor."/".$usr->los."/".$usr->bto
            ."/".$usr->toi."/".$usr->ndr."/".$usr->gdr
            ."/".$usr->ratarata; ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $usr->tahun; ?>
              </td>
              <td>
                <?php echo $usr->bor; ?>
              </td>
              <td>
                <?php echo $usr->los; ?>
              </td>
              <td>
                <?php echo $usr->bto; ?>
              </td>
              <td>
                <?php echo $usr->toi; ?>
              </td>
              <td>
                <?php echo $usr->ndr; ?>
              </td>
              <td>
                <?php echo $usr->gdr; ?>
              </td>
              <td>
                <?php echo $usr->ratarata; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=layanan-indikatorlayanan&&action=delete&&id=<?php echo $usr->idindikatorlayanan; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
              <?php $no = $no + 1; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Indikator Layanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=layanan-indikatorlayanan&&action=addchange" method="post">
          <div class="form-row">
              <input type="hidden" name="id" id="id" class="form-control">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault01">Tahun</label>
                  <select class="form-control" name="tahun" id="tahun">
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                  </select>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">BOR</label>
                <input type="text" name="bor" id="bor" class="form-control"  required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">LOS</label>
                <input type="text" name="los" id="los" class="form-control"  required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">BTO</label>
                <input type="text" name="bto" id="bto" class="form-control"  required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">TOI</label>
                <input type="text" name="toi" id="toi" class="form-control"  required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">NDR</label>
                <input type="text" name="ndr" id="ndr" class="form-control"  required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">GDR</label>
                <input type="text" name="gdr" id="gdr" class="form-control"  required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Rata Rata</label>
                <input type="text" name="ratarata" id="ratarata" class="form-control"  required>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('tahun').value= data[1];
              document.getElementById('bor').value= data[2];
              document.getElementById('los').value= data[3];
              document.getElementById('bto').value= data[4];
              document.getElementById('toi').value= data[5];
              document.getElementById('ndr').value= data[6];
              document.getElementById('gdr').value= data[7];
              document.getElementById('ratarata').value= data[8];
            }

         });
    });
  </script>
