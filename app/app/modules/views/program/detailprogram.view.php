<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Informasi Program Kerja</h6>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
          <thead>
            <tr>
              <th>Kode Program</th>
              <th>:</th>
              <th><?php echo $data["program"]->kodeprogram; ?></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Nama Program Dan Kegiatan<br></td>
              <td>:</td>
              <td><?php echo $data["program"]->namaprogram; ?></td>
            </tr>
            <tr>
              <td>Indikator Kinerja Program</td>
              <td>:</td>
              <td><?php echo $data["program"]->indikator; ?></td>
            </tr>
            <tr>
              <td>Data Capaian Pada Tahun Awal</td>
              <td>:</td>
              <td><?php echo $data["program"]->capaianawal; ?></td>
            </tr>
            <tr>
              <td>Penanggung Jawab Unit Kerja</td>
              <td>:</td>
              <td><?php echo $data["program"]->namaunit; ?></td>
            </tr>
          </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th colspan="1">2019</th>
                <th colspan="1">2020</th>
                <th colspan="1">2021</th>
                <th colspan="1">2022</th>
                <th colspan="1">2023</th>
                <th colspan="1">Realisasi Akhir</th>
              </tr>
            </thead>
            <tbody>
              <!-- <tr>
                <td>Rp</td>
                <td>Rp</td>
                <td>Rp</td>
                <td>Rp</td>
                <td>Rp</td>
                <td>Rp</td>
              </tr> -->
              <tr>
                <!-- <td><?php echo $data["program"]->t1t; ?></td> -->
                <td><?php echo rupiah($data["program"]->t1r); ?></td>
                <!-- <td><?php echo $data["program"]->t2t; ?></td> -->
                <td><?php echo rupiah($data["program"]->t2r); ?></td>
                <!-- <td><?php echo $data["program"]->t3t; ?></td> -->
                <td><?php echo rupiah($data["program"]->t3r); ?></td>
                <!-- <td><?php echo $data["program"]->t4t; ?></td> -->
                <td><?php echo rupiah($data["program"]->t4r); ?></td>
                <!-- <td><?php echo $data["program"]->t5t; ?></td> -->
                <td><?php echo rupiah($data["program"]->t5r); ?></td>
                <!-- <td><?php echo $data["program"]->targetakhir; ?></td> -->
                <td><?php echo rupiah($data["program"]->realisasiakhir); ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Rencana Kerja</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Rencana Kerja Baru</span>
      </button>
      <hr>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Kode</th>
            <th>Nama Program</th>
            <th>Realisasi Akhir</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Kode</th>
            <th>Nama Program</th>
            <th>Realisasi Akhir</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php foreach ($data["raker"] as $usr): ?>
            <?php $datas = $usr->idraker."/".$usr->idkode.
            "/".$usr->t1.
            "/".$usr->t2.
            "/".$usr->t3.
            "/".$usr->t4.
            "/".$usr->t5.
            "/".$usr->akhir; ?>
            <tr>
              <td>
                <?php echo $usr->kodeprogram; ?>
              </td>
              <td>
                <?php echo $usr->namaprogram; ?>
              </td>
              <td>
                <?php echo rupiah($usr->akhir); ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>"  class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-flag"></i>
                    </span>
                    <span class="text"></span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=program-raker&&action=delete&&id=<?php echo $usr->idraker; ?>" onclick="return confirm('Data Akan Di Hapus ?');"  class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text"></span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=program-raker&&idr=<?php echo $usr->idraker; ?>&&idp=<?php echo $usr->idpokja; ?>" class="btn btn-info btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-info"></i>
                    </span>
                    <span class="text">Detail</span>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Rencana Program Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=program-raker&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-12">
              <input type="hidden" name="id" id="id" class="form-control">
              <input type="hidden" name="idpokja" id="idpokja" value="<?php echo $data["program"]->idprogram; ?>"class="form-control">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Nama Program : </label>
                <select name="kodeprogram" class="form-control js-example-basic-single" id="kodeprogram">
                  <?php
                      foreach ($data["kodeprogram"] as $bahan) {
                          ?>
                          <option value="<?php echo $bahan->idkode; ?>"><?php echo $bahan->kodeprogram." - ".$bahan->namaprogram;?></option>
                      <?php
                      }
                  ?>
                </select>
              </div>
              <hr>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="form-control-label"s for="validationDefault01">Tahun 1</label>
                <input type="text" name="r1" id="r1" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Tahun 2</label>
                <input type="text" name="r2" id="r2" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Tahun 3</label>
                <input type="text" name="r3" id="r3" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Tahun 4</label>
                <input type="text" name="r4" id="r4" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Tahun 5</label>
                <input type="text" name="r5" id="r5" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Realisasi Akhir</label>
                <input type="text" name="rakhir" id="rakhir" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-12">
              <hr>
            </div>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              $("#kodeprogram").val(data[1]).trigger('change');
              document.getElementById('r1').value= data[2];
              document.getElementById('r2').value= data[3];
              document.getElementById('r3').value= data[4];
              document.getElementById('r4').value= data[5];
              document.getElementById('r5').value= data[6];
              document.getElementById('rakhir').value= data[7];
          }
         });
    });
  </script>

  <script type="text/javascript">
  (function($) {
    'use strict';
    if ($(".js-example-basic-single").length) {
      $(".js-example-basic-single").select2();
    }
  })(jQuery);
  </script>
