<!-- DataTales Example -->

<div class="row">

  <?php if (!empty($_GET["idr"]) && !empty($_GET["idr"])): ?>
    <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Data Informasi Indikator Kerja</h6>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <tr>
                    <th>Kode Rekening Program</th>
                    <th>:</th>
                    <th><?php echo $data["program"]->kodeprogram; ?></th>
                  </tr>
                  <tr>
                    <td>Nama Program<br></td>
                    <td>:</td>
                    <td><?php echo $data["program"]->namaprogram; ?></td>
                  </tr>
                  <tr>
                    <td>Penanggung Jawab Program<br></td>
                    <td>:</td>
                    <td><?php echo $data["program"]->namaunit; ?></td>
                  </tr>
                  <tr>
                    <th>Kode Rekening Kegiatan</th>
                    <th>:</th>
                    <th><?php echo $data["raker"]->kodeprogram; ?></th>
                  </tr>
                  <tr>
                    <td>Nama Kegiatan<br></td>
                    <td>:</td>
                    <td><?php echo $data["raker"]->namaprogram; ?></td>
                  </tr>
                  <tr>
                    <th colspan="3" textcenter> Indikator Dan Tolak Ukur</th>
                  </tr>
                  <tr>
                    <th>Capaian Program</th>
                    <th>:</th>
                    <th><?php echo $data["program"]->indikator; ?></th>
                  </tr>
                  <tr>
                    <td>Masukan<br></td>
                    <td>:</td>
                    <td>APBD</td>
                  </tr>
                  <tr>
                    <td>Keluaran<br></td>
                    <td>:</td>
                    <td><?php echo $data["indikator"]->indikator; ?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>


  <div class="col-md-3">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Informasi Indiikator</h6>
      </div>
      <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item"><?php echo $data["indikator"]->indikator; ?></li>
          <li class="list-group-item">2019 : <?php echo rupiah($data["indikator"]->t1r); ?></li>
          <li class="list-group-item">2020 : <?php echo rupiah($data["indikator"]->t2r); ?></li>
          <li class="list-group-item">2021 : <?php echo rupiah($data["indikator"]->t3r); ?></li>
          <li class="list-group-item">2022 : <?php echo rupiah($data["indikator"]->t4r); ?></li>
          <li class="list-group-item">2023 : <?php echo rupiah($data["indikator"]->t5r); ?></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Pembelanjaan</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">Tambah Pembelanjaan</span>
          </button>
          <hr>
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Kode</th>
                <th>Uraian</th>
                <th>Jumlah</th>
                <th>Tahun</th>
                <th>Tindakan</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Kode</th>
                <th>Uraian</th>
                <th>Jumlah</th>
                <th>Tahun</th>
                <th>Tindakan</th>
              </tr>
            </tfoot>
            <tbody>
              <?php foreach ($data["dpa"] as $usr): ?>
                <?php $datas = $usr->iddpa."/".$usr->idindikator."/".$usr->idrekening."/".$usr->sumberdana."/".$usr->jumlah."/".$usr->tw."/".$usr->tahun; ?>
                <tr>
                  <td>
                    <?php echo $usr->koderekening; ?>
                  </td>
                  <td>
                    <?php echo $usr->uraian; ?>
                  </td>
                  <td>
                    <?php echo rupiah($usr->jumlah); ?>
                  </td>
                  <td>
                    <?php echo $usr->tw." - ".$usr->tahun; ?>
                  </td>
                  <td>
                    <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                          <i class="fas fa-wrench"></i>
                        </span>
                        <span class="text"></span>
                    </a>
                    <a href="<?php echo SITE_URL; ?>?page=program-dpa&&action=delete&&id=<?php echo $usr->iddpa; ?>" onclick="return confirm('Data Akan Di Hapus ?');"  class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                          <i class="fas fa-trash"></i>
                        </span>
                        <span class="text"></span>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade bd-example-modal-xl" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-m" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Belanja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=program-dpa&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault02">Kode Rekening</label>
                <select name="koderek" id="koderek" class="form-control" id="exampleFormControlSelect1">
                  <?php foreach ($data["koderek"] as $datanya): ?>
                    <option value="<?php echo $datanya->idkoderek; ?>"><?php echo $datanya->koderekening." - ".$datanya->uraian; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Tahun</label>
                <select class="form-control" name="thn" id="thn">
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                </select>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Jenis Tw</label>
                <select class="form-control" name="jenistw" id="jenistw">
                  <option value="TW1">TW1</option>
                  <option value="TW2">TW2</option>
                  <option value="TW3">TW3</option>
                  <option value="TW4">TW4</option>
                </select>
              </div>
            </div>

            <div class="col-md-12">
              <input type="hidden" name="id" id="id" class="form-control">
              <input type="hidden" name="idi" id="idi" value="<?php echo $data["indikator"]->idindikatorkerja; ?>" class="form-control">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Jumlah</label>
                <input type="number" name="jumlah" id="jumlah" class="form-control" placeholder="" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Sumber Dana</label>
                <select class="form-control" name="dana" id="dana">
                  <option value="DAUM">DAUM</option>
                  <option value="DAK">DAK</option>
                  <option value="DBHCHT">DBHCHT</option>
                  <option value="DDL">DDL</option>
                </select>
              </div>
            </div>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('idi').value= data[1];
              document.getElementById('jumlah').value= data[4];
              document.getElementById('dana').value= data[3];
              document.getElementById('thn').value= data[6];
              document.getElementById('jenistw').value= data[5];
              // $("#dana").val(data[3]).trigger('change');

              document.getElementById('koderek').value= data[2];
            }

         });
    });
  </script>
