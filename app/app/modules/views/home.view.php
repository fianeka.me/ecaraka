
<div class="jumbotron">
  <h1 class="display-4">e-Reporting Caraka</h1>
  <p class="lead">Aplikasi untuk mempermudah pencatatan bisa ditaruh disini deskripsi</p>
  <hr class="my-4">
  <p>Untuk mulai menggunakannya dapat menggunakna menu di sebelah kiri, ataupun pintasan dibawah ini.</p>
</div>

<?php if ($_SESSION["type"]==1): ?>
<hr>
<div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Data User</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola User</div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <a href="<?php echo PATH; ?>?page=main-user">
              <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Data Unit Kerja</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola Unit Kerja</div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <a href="<?php echo PATH; ?>?page=master-unitkerja">
              <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Data Kode Program</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola Kode Program</div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <a href="<?php echo PATH; ?>?page=master-kodeprogram">
              <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Data Kode Rekening </div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola Kode Rekening </div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <a href="<?php echo PATH; ?>?page=master-koderekening">
              <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<?php endif; ?>
<hr>
<?php if ($_SESSION["login"]->idunitkerja==20 || $_SESSION["type"]==1): ?>
  <div class="row">
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 1.1</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola Data Rumah Sakit</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a  href="<?php echo PATH; ?>?page=layanan-datautama">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 1.1</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola Data Tempat Tidur</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-datatt">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 1.1</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola Data Pelayanan</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-pelayanan">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 1.2</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola Data Indikator Pelayanan</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-indikatorlayanan">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 1.3</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kelola Fasilitas Tempat Tidur</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-fasilitastt">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 5</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kunjungan Rumah Sakit</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-pengunjungrs">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 5</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kunjungan Rawat Inap</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-pengunjungirna">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 5</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kunjungan Rawat Jalan</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-pengunjungirja">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 5</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kunjungan Gangguan Jiwa</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-datajiwa">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 5</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Kematian Ibu Dan Bayi</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-dataibu">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 5</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">10 Besar Penyakit Irna</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a href="<?php echo PATH; ?>?page=layanan-tenirna">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Data RL 5</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">10 Besar Penyakit Irja</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <a  href="<?php echo PATH; ?>?page=layanan-tenirja">
                <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
<?php else: ?>

  <?php if (isset($data["kode"]["data"])): ?>
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Laporan Program</h6>
      </div>
      <div class="card-body">
        <div class="alert alert-info" role="alert">
          Anda Dapat Melihat Detail Laporan Dari Program & Kegiatan Yang Menjadi Tanggung Jawab Pada Bagian Ini
        </div>
        <hr>
        <div class="accordion" id="accordionExample">
          <?php foreach ($data["kode"]["data"] as $usr): ?>
            <div class="card">
              <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#<?php echo "string".$usr["program"]->idprogram; ?>" aria-expanded="true" aria-controls="collapseOne">
                    <?php echo $usr["program"]->kodeprogram." ".$usr["program"]->namaprogram;?>
                  </button>
                </h2>
              </div>
              <div id="<?php echo "string".$usr["program"]->idprogram;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Kode Program</th>
                        <th>:</th>
                        <th><?php echo $usr["program"]->kodeprogram; ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Nama Program Dan Kegiatan<br></td>
                        <td>:</td>
                        <td><?php echo $usr["program"]->namaprogram; ?></td>
                      </tr>
                      <tr>
                        <td>Indikator Kinerja Program</td>
                        <td>:</td>
                        <td><?php echo $usr["program"]->indikator; ?></td>
                      </tr>
                      <tr>
                        <td>Data Capaian Pada Tahun Awal</td>
                        <td>:</td>
                        <td><?php echo $usr["program"]->capaianawal; ?></td>
                      </tr>
                      <tr>
                        <td>Penanggung Jawab Unit Kerja</td>
                        <td>:</td>
                        <td><?php echo $usr["program"]->namaunit; ?></td>
                      </tr>
                      <?php
                        for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                      ?>
                      <?php if ($x==2019): ?>
                        <tr>
                          <td><?php echo "Pagu ".$x;?></td>
                          <td>:</td>
                          <td><?php echo rupiah($usr["program"]->t1r); ?></td>
                        </tr>
                      <?php endif; ?>
                      <?php if ($x==2020): ?>
                        <tr>
                          <td><?php echo "Pagu ".$x;?></td>
                          <td>:</td>
                          <td><?php echo rupiah($usr["program"]->t2r); ?></td>
                        </tr>
                      <?php endif; ?>
                      <?php if ($x==2021): ?>
                        <tr>
                          <td><?php echo "Pagu ".$x;?></td>
                          <td>:</td>
                          <td><?php echo rupiah($usr["program"]->t3r); ?></td>
                        </tr>
                      <?php endif; ?>
                      <?php if ($x==2022): ?>
                        <tr>
                          <td><?php echo "Pagu ".$x;?></td>
                          <td>:</td>
                          <td><?php echo rupiah($usr["program"]->t4r); ?></td>
                        </tr>
                      <?php endif; ?>
                      <?php if ($x==2023): ?>
                        <tr>
                          <td><?php echo "Pagu ".$x;?></td>
                          <td>:</td>
                          <td><?php echo rupiah($usr["program"]->t5r); ?></td>
                        </tr>
                      <?php endif; ?>
                      <?php
                        }
                      ?>
                    </tbody>
                    </table>
                  </div>
                  <hr>

                  <!-- <table> -->
                    <?php foreach ($usr["raker"] as $usr1): ?>
                      <div class="card">
                        <div class="card-header">
                          <a href="#">  <?php echo $usr1["raker"]->kodeprogram." ".$usr1["raker"]->namaprogram; ?></a>
                        </div>
                        <div class="card-body">
                          <div class="table-responsive">
                            <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Kode Program</th>
                                <th>:</th>
                                <th><?php echo $usr1["raker"]->kodeprogram; ?></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Nama Program Dan Kegiatan<br></td>
                                <td>:</td>
                                <td><?php echo $usr1["raker"]->namaprogram; ?></td>
                              </tr>
                              <?php
                                for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                              ?>
                              <?php if ($x==2019): ?>
                                <tr>
                                  <td><?php echo "Pagu ".$x;?></td>
                                  <td>:</td>
                                  <td><?php echo rupiah($usr1["raker"]->t1); ?></td>
                                </tr>
                              <?php endif; ?>
                              <?php if ($x==2020): ?>
                                <tr>
                                  <td><?php echo "Pagu ".$x;?></td>
                                  <td>:</td>
                                  <td><?php echo rupiah($usr1["raker"]->t2); ?></td>
                                </tr>
                              <?php endif; ?>
                              <?php if ($x==2021): ?>
                                <tr>
                                  <td><?php echo "Pagu ".$x;?></td>
                                  <td>:</td>
                                  <td><?php echo rupiah($usr1["raker"]->t3); ?></td>
                                </tr>
                              <?php endif; ?>
                              <?php if ($x==2022): ?>
                                <tr>
                                  <td><?php echo "Pagu ".$x;?></td>
                                  <td>:</td>
                                  <td><?php echo rupiah($usr1["raker"]->t4); ?></td>
                                </tr>
                              <?php endif; ?>
                              <?php if ($x==2023): ?>
                                <tr>
                                  <td><?php echo "Pagu ".$x;?></td>
                                  <td>:</td>
                                  <td><?php echo rupiah($usr1["raker"]->t5); ?></td>
                                </tr>
                              <?php endif; ?>
                              <?php
                                }
                              ?>
                            </tbody>
                            </table>
                          </div>
                          <hr>

                          <?php $datatahunan = array('2019' => $usr1["raker"]->t1, '2020' => $usr1["raker"]->t2, '2021' => $usr1["raker"]->t3, '2022' => $usr1["raker"]->t4, '2023' => $usr1["raker"]->t5 ); ?>
                          <?php foreach ($usr1["indikator"] as $usr2): ?>
                            <div class="alert alert-danger" role="alert">
                              <b>
                                <?php echo $usr2["indikator"]->indikator; ?>
                              </b>
                            </div>
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>Indikator Kegiatan</th>
                                  <th>:</th>
                                  <th><?php echo $usr2["indikator"]->indikator; ?></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Unit Penangung Jawab<br></td>
                                  <td>:</td>
                                  <td><?php echo $usr2["indikator"]->namaunit; ?></td>
                                </tr>
                              </tbody>
                            </table>

                            <div class="row">
                              <div class="col-2">
                                <div class="list-group" id="list-tab" role="tablist">
                                  <?php
                                    for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                                  ?>
                                      <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-<?php echo $usr2["indikator"]->idindikatorkerja.$x; ?>" role="tab" aria-controls="profile"><?php echo $x; ?></a>
                                  <?php
                                    }
                                  ?>
                                </div>

                              </div>
                              <div class="col-10">
                                <div class="tab-content" id="nav-tabContent">
                                  <?php
                                    for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                                  ?>
                                  <div class="tab-pane fade" id="list-<?php echo $usr2["indikator"]->idindikatorkerja.$x; ?>" role="tabpanel" aria-labelledby="list-profile-list">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Data <?php echo $x ?></th>
                                          <th>Total Belanja</th>
                                          <th>Realisasi (%)</th>
                                        </tr>
                                      </thead>
                                      <?php $totalbelanja = $usr2["realisasi"][$x]->tw1+$usr2["realisasi"][$x]->tw2+$usr2["realisasi"][$x]->tw3+$usr2["realisasi"][$x]->tw4; ?>
                                      <tbody>
                                        <tr>
                                          <td>TW 1</td>
                                          <td><?php echo rupiah($usr2["realisasi"][$x]->tw1) ?></td>
                                          <td><?php echo round(($usr2["realisasi"][$x]->tw1/$datatahunan[$x])*100,2)."%"; ?></td>
                                        </tr>
                                        <tr>
                                          <td>TW 2</td>
                                          <td><?php echo rupiah($usr2["realisasi"][$x]->tw2) ?></td>
                                          <td><?php echo round(($usr2["realisasi"][$x]->tw2/$datatahunan[$x])*100,2)."%"; ?></td>
                                        </tr>
                                        <tr>
                                          <td>TW 3</td>
                                          <td><?php echo rupiah($usr2["realisasi"][$x]->tw3) ?></td>
                                          <td><?php echo round(($usr2["realisasi"][$x]->tw3/$datatahunan[$x])*100,2)."%"; ?></td>
                                        </tr>
                                        <tr>
                                          <td>TW 4</td>
                                          <td><?php echo rupiah($usr2["realisasi"][$x]->tw4) ?></td>
                                          <td><?php echo round(($usr2["realisasi"][$x]->tw4/$datatahunan[$x])*100,2)."%"; ?></td>
                                        </tr>
                                        <tr>
                                          <th>Total Pagu</th>
                                          <th>Total Belanja</th>
                                          <th>Total Realisasi</th>
                                        </tr>
                                        <tr>
                                          <td><?php echo rupiah($datatahunan[$x]) ?></td>
                                          <td><?php echo rupiah($totalbelanja) ?></td>
                                          <td><?php echo round(($totalbelanja/$datatahunan[$x])*100,2)."%" ?></td>
                                        </tr>
                                        <tr>
                                          <th>Sisa Uang</th>
                                          <th colspan="2"><?php echo rupiah($datatahunan[$x]-$totalbelanja) ?></th>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <?php
                                    }
                                  ?>
                                </div>
                              </div>
                            </div>

                          <?php endforeach; ?>
                        </div>
                      </div>
                      <br><br>
                    <?php endforeach; ?>
                  <!-- </table> -->
                </div>
              </div>
            </div>
            <br>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($data["indi"])): ?>
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Progam & Kegiatan Anda</h6>
      </div>
      <div class="card-body">

        <div class="alert alert-info" role="alert">
          Anda Dapat Melihat Kegiatan Dalam Tanggung Jawab Anda Pada Bagian Ini
        </div>
        <hr>
        <div class="row">
          <?php foreach ($data["indi"] as $usr): ?>
              <div class="col-xl-6 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="font-weight-bold text-default text-uppercase mb-1"><?php echo $usr["indikator"]->indikator; ?></div>
                        <br>
                        <div class="h6 mb-0 font-weight-bold text-gray-800"><?php echo $usr["indikator"]->namaunit; ?></div>
                      </div>
                      <div class="col-auto">
                        <a  href="<?php echo SITE_URL; ?>?page=program-indikator&&idi=<?php echo $usr["indikator"]->idindikatorkerja; ?>" >
                          <i class="fas fa-arrow-right fa-2x text-gray-300"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>




<?php endif; ?>
