<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Unit Kerja</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Unit Kerja Baru</span>
      </button>
      <hr>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID Unit Kerja</th>
            <th>Nama Unit Kerja</th>
            <th>Keterangan Unit Kerja</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>ID Unit Kerja</th>
            <th>Nama Unit Kerja</th>
            <th>Keterangan Unit Kerja</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php foreach ($data["unitkerja"] as $usr): ?>
            <?php $datas = $usr->idunitkerja."/".$usr->namaunit."/".$usr->keterangan; ?>
            <tr>
              <td>
                <?php echo "UNIT-".$usr->idunitkerja; ?>
              </td>
              <td>
                <?php echo $usr->namaunit; ?>
              </td>
              <td>
                <?php echo $usr->keterangan; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=master-unitkerja&&action=delete&&id=<?php echo $usr->idunitkerja; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Unit Kerja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=master-unitkerja&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-12">
              <input type="hidden" name="id" id="id" class="form-control">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Nama Unit</label>
                <input type="text" name="namaunit" id="namaunit" class="form-control" placeholder="Nama" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Keterangan Unit</label>
                <input type="text" name="ketunit" id="ketunit" class="form-control" placeholder="Nama" required>
              </div>
            </div>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('namaunit').value= data[1];
              document.getElementById('ketunit').value= data[2];
            }

         });
    });
  </script>
