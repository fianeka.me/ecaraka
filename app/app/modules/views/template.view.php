<?php
$page = (isset($_GET['page']) && $_GET['page']) ? $_GET['page'] : 'main-home';
$temp = explode('-',$page);
$dir = $temp[0]=="main" ? "" : $temp[0];
$page = $temp[1];
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>e-Reporting Caraka</title>

  <!-- Custom fonts for this template-->
  <link href="../resource/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->

  <!-- Custom styles for this template-->
  <link href="../resource/css/sb-admin-2.css" rel="stylesheet">
  <link href="../resource/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <link href="../resource/vendor/select2/dist/css/select2.css" rel="stylesheet">

  <!-- Bootstrap core JavaScript-->
  <script src="../resource/vendor/jquery/jquery.min.js"></script>
  <script src="../resource/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../resource/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../resource/vendor/select2/dist/js/select2.full.js"></script>

  <!-- Custom scripts for all pages-->

  <!-- Page level plugins -->
  <script src="../resource/vendor/chart.js/Chart.min.js"></script>
  <script src="../resource/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../resource/vendor/datatables/dataTables.bootstrap4.min.js"></script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-database"></i>
        </div>
        <div class="sidebar-brand-text mx-2 ">e-Reporting</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <?php if ($_SESSION["type"]==1): ?>
        <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Master Data Program
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Data Master Utama</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?php echo PATH; ?>?page=main-user">Data User</a>
            <a class="collapse-item" href="<?php echo PATH; ?>?page=master-unitkerja">Data Unit Kerja</a>
            <a class="collapse-item" href="<?php echo PATH; ?>?page=master-kodeprogram">Data Kode Program</a>
            <a class="collapse-item" href="<?php echo PATH; ?>?page=master-koderekening">Data Kode Rekening</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Rencana Program</span>
        </a>
        <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?php echo PATH; ?>?page=program-program">Program & Kegiatan</a>
          </div>
        </div>
      </li>

      <?php endif; ?>


      <?php if ($_SESSION["login"]->idunitkerja==20 || $_SESSION["type"]==1): ?>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="sidebar-heading">
          Master Data Pelayanan
        </div>
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages1" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>RL 1.1</span>
          </a>
          <div id="collapsePages1" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-datautama">Data Rumah Sakit</a>
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-datatt">Data Tempat Tidur</a>
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-pelayanan">Data Pelayanan</a>
            </div>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages12" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>RL 1.2</span>
          </a>
          <div id="collapsePages12" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-indikatorlayanan">Indikator Pelayanan</a>
            </div>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages13" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>RL 1.3</span>
          </a>
          <div id="collapsePages13" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-fasilitastt">Fasilitas TT Rawat Inap</a>
            </div>
          </div>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePagesbulanan" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Data Bulanan RL 5</span>
          </a>
          <div id="collapsePagesbulanan" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-pengunjungrs">Kunjungan RS</a>
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-pengunjungirja">Kunjungan IRJA</a>
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-pengunjungirna">Kunjungan IRNA</a>
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-datajiwa">Kunjungan Gangguan Jiwa</a>
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-dataibu">Kematian Ibu & Bayi</a>
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-tenirna">10 Besar Penyakit IRNA</a>
              <a class="collapse-item" href="<?php echo PATH; ?>?page=layanan-tenirja">10 Besar Penyakit IRJA</a>
            </div>
          </div>
        </li>

        <?php if ($_SESSION["login"]->idunitkerja==20): ?>
          <li class="nav-item active">
            <a class="nav-link" href="<?php echo PATH; ?>?page=laporan-pelayanan">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Laporan Pelayanan</span></a>
          </li>
        <?php endif; ?>
      <?php endif; ?>


      <!-- Divider -->


      <hr class="sidebar-divider">
      <?php if ($_SESSION["type"]==1): ?>

      <!-- Heading -->
      <div class="sidebar-heading">
        Laporan
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Laporan</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?php echo PATH; ?>?page=laporan-program">Laporan Program</a>
            <a class="collapse-item" href="<?php echo PATH; ?>?page=laporan-pelayanan">Laporan Pelayanan</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

    <?php endif; ?>


      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo "Hallo,".$_SESSION["login"]->nama ?></span>
                <img class="img-profile rounded-circle" src="images/admin.jpg">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo PATH; ?>?page=main-login&&action=logout">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <?php
              $view = new View($viewName);
              $view->bind('data', $data);
              $view->forceRender();
          ?>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; e-Monev App 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="../resource/js/demo/datatables-demo.js"></script>
  <script src="../resource/js/sb-admin-2.min.js"></script>


</body>

</html>
