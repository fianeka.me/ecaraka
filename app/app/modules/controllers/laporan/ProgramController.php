<?php

use \modules\controllers\MainController;

class ProgramController extends MainController {

      public function index() {

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          $tahun  = isset($_POST["tahun"]) ? $_POST["tahun"] : "";

          $tahunan = ["2019","2020","2021","2022","2023"];

          $this->model('pokja');
          $data = $this->pokja->getJoin(array('kodeprogram', 'unitkerja'),
              array(
                'pokja.idkode' => 'kodeprogram.idkode',
                'pokja.idunitkerja' => 'unitkerja.idunitkerja'
              ),
              'JOIN'
          );

          $datas = array();
          foreach ($data as $singledata) {
            // echo $singledata->idprogram;
            $this->model('raker');
            $data1 = $this->raker->getJoin(array('kodeprogram'),
                array(
                  'raker.idkode' => 'kodeprogram.idkode'
                ),
                'JOIN',
                array(
                  'raker.idpokja' => $singledata->idprogram
                )
            );

            $temparray = array();
            foreach ($data1 as $datanya) {
              $this->model('indikator');
              $data2 = $this->indikator->getJoin(array('unitkerja'),
                  array(
                    'indikatorkerja.idunitkerja' => 'unitkerja.idunitkerja'
                  ),
                  'JOIN',
                  array(
                    'indikatorkerja.idraker' => $datanya->idraker
                  )
              );

              $datarakertw = array();
              foreach($data2 as $item) {
                $this->model('dpa');
                $datapertahun = array();
                foreach ($tahunan as $thn) {
                  if ($thn <= $tahun ) {
                    $coutnya = "SELECT SUM( CASE WHEN tw = 'TW1' THEN jumlah END ) AS tw1,
                    SUM( CASE WHEN tw = 'TW2' THEN jumlah END ) AS tw2,
                    SUM( CASE WHEN tw = 'TW3' THEN jumlah END ) AS tw3,
                    SUM( CASE WHEN tw = 'TW4' THEN jumlah END ) AS tw4 FROM dpa WHERE idindikator = ".$item->idindikatorkerja." and tahun = ".$thn;
                    $datacount = $this->dpa->customSql($coutnya);
                    $datapertahun[$thn] = $datacount[0];
                  }
                  // array_push($datapertahun, ar$datacount[0]);
                }
                array_push($datarakertw, array("indikator" => $item, "realisasi" => $datapertahun));

              }

              $bor = array('raker' => $datanya, 'indikator' => $datarakertw);
              array_push($temparray, $bor);
            }

            array_push($datas, array("program" => $singledata, "raker" => $temparray));
          }
          $datax = array();
          $datax["tahunberjalan"] = $tahun;
          $datax["data"] = $datas;

          $_SESSION['toprint'] = $datax;
          $this->template('laporan/program', array("kode"=>$datax, "thn"=>$tahun));

        }else{

          $this->template('laporan/program', array());
        }

      }

      public function reportprint() {
          ?>
            <meta http-equiv="refresh" content="1;url=<?php SITE_URL; ?>utils/public/program.php">
          <?php
      }


}
?>
