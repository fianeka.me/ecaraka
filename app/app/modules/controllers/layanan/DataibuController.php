<?php

use \modules\controllers\MainController;

class DataibuController extends MainController {

      public function index() {
        $this->model('dataibu');
        $data1 = $this->dataibu->get();
        $this->template('layanan/dataibu', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('dataibu');
          $databulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
          ];

          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $bulan  = isset($_POST["bulan"]) ? $_POST["bulan"] : "";
              $tahun  = isset($_POST["tahun"]) ? $_POST["tahun"] : "";
              $jumlahpersalinan  = isset($_POST["jumlahpersalinan"]) ? $_POST["jumlahpersalinan"] : "";
              $jumlahkematianibu  = isset($_POST["jumlahkematianibu"]) ? $_POST["jumlahkematianibu"] : "";
              $jumlahkematianbayi  = isset($_POST["jumlahkematianbayi"]) ? $_POST["jumlahkematianbayi"] : "";
              $penyebabmatiibu  = isset($_POST["penyebabmatiibu"]) ? $_POST["penyebabmatiibu"] : "";
              $penyebabmatibayi  = isset($_POST["penyebabmatibayi"]) ? $_POST["penyebabmatibayi"] : "";

              $nomorbulan = array_search($bulan, $databulan)+1;


              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->dataibu->insert(
                        array(
                          'nomorbulan' => $nomorbulan,
                          'bulan' => $bulan,
                          'tahun' => $tahun,
                          'jumlahpersalinan' => $jumlahpersalinan,
                          'jumlahkematianibu' => $jumlahkematianibu,
                          'jumlahkematianbayi' => $jumlahkematianbayi,
                          'penyebabmatiibu' => $penyebabmatiibu,
                          'penyebabmatibayi' => $penyebabmatibayi
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'nomorbulan' => $nomorbulan,
                  'bulan' => $bulan,
                  'tahun' => $tahun,
                  'jumlahpersalinan' => $jumlahpersalinan,
                  'jumlahkematianibu' => $jumlahkematianibu,
                  'jumlahkematianbayi' => $jumlahkematianbayi,
                  'penyebabmatiibu' => $penyebabmatiibu,
                  'penyebabmatibayi' => $penyebabmatibayi
                );
                if(count($error) == 0) {
                    $update = $this->dataibu->update($updateArrayData, array('idibu' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('dataibu');
          $delete = $this->dataibu->delete(array('idibu' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
