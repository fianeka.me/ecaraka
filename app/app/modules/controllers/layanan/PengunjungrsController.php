<?php

use \modules\controllers\MainController;

class PengunjungrsController extends MainController {

      public function index() {
        $this->model('pengunjungrs');
        $data1 = $this->pengunjungrs->get();
        $this->template('layanan/pengunjung', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('pengunjungrs');

          $databulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
          ];

          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $bulan  = isset($_POST["bulan"]) ? $_POST["bulan"] : "";
              $tahun  = isset($_POST["tahun"]) ? $_POST["tahun"] : "";
              $barul  = isset($_POST["barul"]) ? $_POST["barul"] : "";
              $barup  = isset($_POST["barup"]) ? $_POST["barup"] : "";
              $lamal  = isset($_POST["lamal"]) ? $_POST["lamal"] : "";
              $lamap  = isset($_POST["lamap"]) ? $_POST["lamap"] : "";

              $nomorbulan = array_search($bulan, $databulan)+1;


              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->pengunjungrs->insert(
                        array(
                          'nomorbulan' => $nomorbulan,
                          'bulan' => $bulan,
                          'tahun' => $tahun,
                          'barul' => $barul,
                          'barup' => $barup,
                          'lamal' => $lamal,
                          'lamap' => $lamap
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'nomorbulan' => $nomorbulan,
                  'bulan' => $bulan,
                  'tahun' => $tahun,
                  'barul' => $barul,
                  'barup' => $barup,
                  'lamal' => $lamal,
                  'lamap' => $lamap
                );
                if(count($error) == 0) {
                    $update = $this->pengunjungrs->update($updateArrayData, array('idpengunjung' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('pengunjungrs');
          $delete = $this->pengunjungrs->delete(array('idpengunjung' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
