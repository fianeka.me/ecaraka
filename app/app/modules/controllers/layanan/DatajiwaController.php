<?php

use \modules\controllers\MainController;

class DatajiwaController extends MainController {

      public function index() {
        $this->model('datajiwa');
        $data1 = $this->datajiwa->get();
        $this->template('layanan/datajiwa', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('datajiwa');
          $databulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
          ];

          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $bulan  = isset($_POST["bulan"]) ? $_POST["bulan"] : "";
              $tahun  = isset($_POST["tahun"]) ? $_POST["tahun"] : "";
              $laki  = isset($_POST["laki"]) ? $_POST["laki"] : "";
              $perempuan  = isset($_POST["perempuan"]) ? $_POST["perempuan"] : "";

              $nomorbulan = array_search($bulan, $databulan)+1;


              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->datajiwa->insert(
                        array(
                          'nomorbulan' => $nomorbulan,
                          'bulan' => $bulan,
                          'tahun' => $tahun,
                          'laki' => $laki,
                          'perempuan' => $perempuan
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'nomorbulan' => $nomorbulan,
                  'bulan' => $bulan,
                  'tahun' => $tahun,
                  'laki' => $laki,
                  'perempuan' => $perempuan
                );
                if(count($error) == 0) {
                    $update = $this->datajiwa->update($updateArrayData, array('idjiwa' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('datajiwa');
          $delete = $this->datajiwa->delete(array('idjiwa' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
