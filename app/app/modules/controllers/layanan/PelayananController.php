<?php

use \modules\controllers\MainController;

class PelayananController extends MainController {

      public function index() {
        $this->model('pelayanan');
        $data1 = $this->pelayanan->get();
        $this->template('layanan/pelayanan', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('pelayanan');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $kelas  = isset($_POST["kelas"]) ? $_POST["kelas"] : "";

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->pelayanan->insert(
                        array(
                          'namalayanan' => $kelas
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'namalayanan' => $kelas
                );
                if(count($error) == 0) {
                    $update = $this->pelayanan->update($updateArrayData, array('idpelayanan' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('pelayanan');
          $delete = $this->pelayanan->delete(array('idpelayanan' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
