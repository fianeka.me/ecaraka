<?php

use \modules\controllers\MainController;

class KodeprogramController extends MainController {

      public function index() {
        $this->model('kodeprogram');
        $data1 = $this->kodeprogram->get();
        $this->template('master/kodeprogram', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('kodeprogram');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $kodeprogram  = isset($_POST["kodeprogram"]) ? $_POST["kodeprogram"] : "";
              $namaprogram = isset($_POST["namaprogram"]) ? $_POST["namaprogram"] : "";

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->kodeprogram->insert(
                        array(
                          'kodeprogram' => $kodeprogram,
                          'namaprogram' => $namaprogram
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'kodeprogram' => $kodeprogram,
                  'namaprogram' => $namaprogram
                );
                if(count($error) == 0) {
                    $update = $this->kodeprogram->update($updateArrayData, array('idkode' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('kodeprogram');
          $delete = $this->kodeprogram->delete(array('idkode' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
