<?php

use \modules\controllers\MainController;

class UnitkerjaController extends MainController {

    public function index() {
      $this->model('unitkerja');
      $data1 = $this->unitkerja->get();
      $this->template('master/unitkerja', array("unitkerja"=>$data1));
    }

    public function addchange() {
        $this->model('unitkerja');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $namaunit  = isset($_POST["namaunit"]) ? $_POST["namaunit"] : "";
            $ketunit = isset($_POST["ketunit"]) ? $_POST["ketunit"] : "";

            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->unitkerja->insert(
                      array(
                        'namaunit' => $namaunit,
                        'keterangan' => $ketunit
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'namaunit' => $namaunit,
                'keterangan' => $ketunit
              );
              if(count($error) == 0) {
                  $update = $this->unitkerja->update($updateArrayData, array('idunitkerja' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
        // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
    }

    public function delete()
    {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('unitkerja');
        $delete = $this->unitkerja->delete(array('idunitkerja' => $id));
        if ($delete) {
            $this->back();
        }
    }
}
?>
