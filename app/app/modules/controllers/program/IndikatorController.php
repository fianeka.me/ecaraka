<?php

use \modules\controllers\MainController;

class IndikatorController extends MainController {

      public function index() {
        $idr = isset($_GET["idr"]) ? $_GET["idr"] : 0;
        $idp = isset($_GET["idp"]) ? $_GET["idp"] : 0;
        $idi = isset($_GET["idi"]) ? $_GET["idi"] : 0;

        $this->model('pokja');
        $data = $this->pokja->getJoin(array('kodeprogram', 'unitkerja'),
            array(
              'pokja.idkode' => 'kodeprogram.idkode',
              'pokja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN',
            array(
              'pokja.idprogram' => $idp
            )
        );


        $this->model('raker');
        $data1 = $this->raker->getJoin(array('kodeprogram'),
            array(
              'raker.idkode' => 'kodeprogram.idkode'
            ),
            'JOIN',
            array(
              'raker.idraker' => $idr
            )
        );

        $this->model('indikator');
        $data2 = $this->indikator->getJoin(array('unitkerja'),
            array(
              'indikatorkerja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN',
            array(
              'indikatorkerja.idindikatorkerja' => $idi
            )
        );

        $this->model('koderekening');
        $koderek = $this->koderekening->get();

        $this->model('dpa');
        $dpa = $this->dpa->getJoin(array('koderekening'),
            array(
              'dpa.idrekening' => 'koderekening.idkoderek'
            ),
            'JOIN',
            array(
              'dpa.idindikator' => $idi
            )
        );

        $this->template('program/indikator', array("koderek"=>$koderek, "dpa"=>$dpa, "program"=>$data[0], "raker"=>$data1[0], "indikator"=>$data2[0]));
      }

      public function addchange() {
        $this->model('indikator');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $idr = isset($_POST["idr"]) ? $_POST["idr"] : "";
            $indikator = isset($_POST["indikator"]) ? $_POST["indikator"] : "";
            $capaianawal = isset($_POST["capaianawal"]) ? $_POST["capaianawal"] : "";
            $unit = isset($_POST["unit"]) ? $_POST["unit"] : "";
            $t1 = isset($_POST["t1"]) ? $_POST["t1"] : "";
            $r1 = isset($_POST["r1"]) ? $_POST["r1"] : "";
            $t2 = isset($_POST["t2"]) ? $_POST["t2"] : "";
            $r2 = isset($_POST["r2"]) ? $_POST["r2"] : "";
            $t3 = isset($_POST["t3"]) ? $_POST["t3"] : "";
            $r3 = isset($_POST["r3"]) ? $_POST["r3"] : "";
            $t4 = isset($_POST["t4"]) ? $_POST["t4"] : "";
            $r4 = isset($_POST["r4"]) ? $_POST["r4"] : "";
            $t5 = isset($_POST["t5"]) ? $_POST["t5"] : "";
            $r5 = isset($_POST["r5"]) ? $_POST["r5"] : "";
            $takhir = isset($_POST["takhir"]) ? $_POST["takhir"] : "";
            $rakhir = isset($_POST["rakhir"]) ? $_POST["rakhir"] : "";

            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->indikator->insert(
                      array(
                        'idraker' => $idr,
                        'indikator' => $indikator,
                        'capaianawal' => $capaianawal,
                        'idunitkerja' => $unit,
                        't1t' => $t1,
                        't1r' => $r1,
                        't2t' => $t2,
                        't2r' => $r2,
                        't3t' => $t3,
                        't3r' => $r3,
                        't4t' => $t4,
                        't4r' => $r4,
                        't5t' => $t5,
                        't5r' => $r5,
                        'targetakhir' => $takhir,
                        'realisasiakhir' => $rakhir
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'idraker' => $idr,
                'indikator' => $indikator,
                'capaianawal' => $capaianawal,
                'idunitkerja' => $unit,
                't1t' => $t1,
                't1r' => $r1,
                't2t' => $t2,
                't2r' => $r2,
                't3t' => $t3,
                't3r' => $r3,
                't4t' => $t4,
                't4r' => $r4,
                't5t' => $t5,
                't5r' => $r5,
                'targetakhir' => $takhir,
                'realisasiakhir' => $rakhir
              );
              if(count($error) == 0) {
                  $update = $this->indikator->update($updateArrayData, array('idindikatorkerja' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('indikator');
          $delete = $this->indikator->delete(array('idindikatorkerja' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
