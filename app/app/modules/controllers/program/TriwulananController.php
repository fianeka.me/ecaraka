<?php

use \modules\controllers\MainController;

class TriwulananController extends MainController {

      public function index() {
        $idi = isset($_GET["idi"]) ? $_GET["idi"] : 0;

        $this->model('indikator');
        $data2 = $this->indikator->getJoin(array('unitkerja'),
            array(
              'indikatorkerja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN',
            array(
              'indikatorkerja.idindikatorkerja' => $idi
            )
        );

        $this->model('triwulan');
        $twan = $this->triwulan->getWhere(array(
            'idindikatorkerja' => $idi
        ));

        $this->template('program/triwulanan', array("indikator"=>$data2[0], "tw"=>$twan));
      }

      public function addchange() {
        $this->model('triwulan');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $thn = isset($_POST["thn"]) ? $_POST["thn"] : "";
            $jenistw = isset($_POST["jenistw"]) ? $_POST["jenistw"] : "";
            $idi = isset($_POST["idi"]) ? $_POST["idi"] : "";
            $jumlah = isset($_POST["jumlah"]) ? $_POST["jumlah"] : "";


            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->triwulan->insert(
                      array(
                        'idindikatorkerja' => $idi,
                        'alokasi' => $jumlah,
                        'tahun' => $thn,
                        'jenistw' => $jenistw
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'idindikatorkerja' => $idr,
                'alokasi' => $indikator,
                'tahun' => $capaianawal,
                'jenistw' => $unit
              );
              if(count($error) == 0) {
                  $update = $this->triwulan->update($updateArrayData, array('idtriwulanan' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('triwulan');
          $delete = $this->triwulan->delete(array('idtriwulanan' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
