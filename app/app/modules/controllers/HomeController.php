<?php

use \modules\controllers\MainController;

class HomeController extends MainController {

    public function index() {

      if ($_SESSION["login"]->idunitkerja==20 || $_SESSION["type"]==1) {
        $this->template('home', array());
      } else {
        $idunitlogin = $_SESSION["login"]->idunitkerja;
        $tahun = date("Y");
        $tahunan = ["2019","2020","2021","2022","2023"];

        $this->model('pokja');
        $data = $this->pokja->getJoin(array('kodeprogram', 'unitkerja'),
            array(
              'pokja.idkode' => 'kodeprogram.idkode',
              'pokja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN',
            array('pokja.idunitkerja' => $idunitlogin)
        );

        $datas = array();
        foreach ($data as $singledata) {
          // echo $singledata->idprogram;
          $this->model('raker');
          $data1 = $this->raker->getJoin(array('kodeprogram'),
              array(
                'raker.idkode' => 'kodeprogram.idkode'
              ),
              'JOIN',
              array(
                'raker.idpokja' => $singledata->idprogram
              )
          );

          $temparray = array();
          foreach ($data1 as $datanya) {
            $this->model('indikator');
            $data2 = $this->indikator->getJoin(array('unitkerja'),
                array(
                  'indikatorkerja.idunitkerja' => 'unitkerja.idunitkerja'
                ),
                'JOIN',
                array(
                  'indikatorkerja.idraker' => $datanya->idraker
                )
            );

            $datarakertw = array();
            foreach($data2 as $item) {
              $this->model('dpa');
              $datapertahun = array();
              foreach ($tahunan as $thn) {
                if ($thn <= $tahun ) {
                  $coutnya = "SELECT SUM( CASE WHEN tw = 'TW1' THEN jumlah END ) AS tw1,
                  SUM( CASE WHEN tw = 'TW2' THEN jumlah END ) AS tw2,
                  SUM( CASE WHEN tw = 'TW3' THEN jumlah END ) AS tw3,
                  SUM( CASE WHEN tw = 'TW4' THEN jumlah END ) AS tw4 FROM dpa WHERE idindikator = ".$item->idindikatorkerja." and tahun = ".$thn;
                  $datacount = $this->dpa->customSql($coutnya);
                  $datapertahun[$thn] = $datacount[0];
                }
                // array_push($datapertahun, ar$datacount[0]);
              }
              array_push($datarakertw, array("indikator" => $item, "realisasi" => $datapertahun));

            }

            $bor = array('raker' => $datanya, 'indikator' => $datarakertw);
            array_push($temparray, $bor);
          }

          array_push($datas, array("program" => $singledata, "raker" => $temparray));
        }
        $datax = array();
        $datax["tahunberjalan"] = $tahun;
        $datax["data"] = $datas;


        // indikator
        $this->model('indikator');
        $data2 = $this->indikator->getJoin(array('unitkerja'),
            array(
              'indikatorkerja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN',
            array(
              'indikatorkerja.idunitkerja' => $idunitlogin
            )
        );

        $datarakertw = array();
        foreach($data2 as $item) {
          $this->model('dpa');
          $datapertahun = array();
          foreach ($tahunan as $thn) {
            if ($thn <= $tahun ) {
              $coutnya = "SELECT SUM( CASE WHEN tw = 'TW1' THEN jumlah END ) AS tw1,
              SUM( CASE WHEN tw = 'TW2' THEN jumlah END ) AS tw2,
              SUM( CASE WHEN tw = 'TW3' THEN jumlah END ) AS tw3,
              SUM( CASE WHEN tw = 'TW4' THEN jumlah END ) AS tw4 FROM dpa WHERE idindikator = ".$item->idindikatorkerja." and tahun = ".$thn;
              $datacount = $this->dpa->customSql($coutnya);
              $datapertahun[$thn] = $datacount[0];
            }
            // array_push($datapertahun, ar$datacount[0]);
          }
          array_push($datarakertw, array("indikator" => $item, "realisasi" => $datapertahun));
        }

        $this->template('home', array("kode"=>$datax, "indi"=>$datarakertw));
      }


    }
}
?>
