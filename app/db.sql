-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Waktu pembuatan: 26 Sep 2020 pada 01.54
-- Versi server: 5.7.26
-- Versi PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `emonev`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dataibubayi`
--

CREATE TABLE `dataibubayi` (
  `idibu` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `jumlahpersalinan` varchar(11) NOT NULL,
  `jumlahkematianibu` varchar(11) NOT NULL,
  `jumlahkematianbayi` varchar(11) NOT NULL,
  `penyebabmatiibu` varchar(256) NOT NULL,
  `penyebabmatibayi` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `dataibubayi`
--

INSERT INTO `dataibubayi` (`idibu`, `nomorbulan`, `bulan`, `tahun`, `jumlahpersalinan`, `jumlahkematianibu`, `jumlahkematianbayi`, `penyebabmatiibu`, `penyebabmatibayi`) VALUES
(1, 1, 'Januari', '2019', '178', '1', '16', 'IUFD', 'BBLR,Asfiksia,Tetanus,Neonatorum,RDS');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datajiwa`
--

CREATE TABLE `datajiwa` (
  `idjiwa` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `laki` varchar(64) NOT NULL,
  `perempuan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `datajiwa`
--

INSERT INTO `datajiwa` (`idjiwa`, `nomorbulan`, `bulan`, `tahun`, `laki`, `perempuan`) VALUES
(1, 1, 'Januari', '2019', '105', '86'),
(2, 2, 'Februari', '2019', '103', '74');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datars`
--

CREATE TABLE `datars` (
  `idrs` int(11) NOT NULL,
  `namars` varchar(64) NOT NULL,
  `tahunberdiri` varchar(64) NOT NULL,
  `direktur` varchar(64) NOT NULL,
  `jenis` varchar(64) NOT NULL,
  `kelas` varchar(64) NOT NULL,
  `status` varchar(64) NOT NULL,
  `kepemilikan` varchar(64) NOT NULL,
  `namapenyelenggara` varchar(64) NOT NULL,
  `bpjs` varchar(64) NOT NULL,
  `alamat` varchar(64) NOT NULL,
  `kab` varchar(64) NOT NULL,
  `luastanah` varchar(64) NOT NULL,
  `luasbangunan` varchar(64) NOT NULL,
  `telpon` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `website` varchar(64) NOT NULL,
  `noizin` varchar(64) NOT NULL,
  `suratizin` varchar(64) NOT NULL,
  `berlaku` varchar(64) NOT NULL,
  `akreditasi` varchar(64) NOT NULL,
  `penetapan` varchar(64) NOT NULL,
  `aktifakreditasi` varchar(64) NOT NULL,
  `layananunggulan` varchar(64) NOT NULL,
  `simrs` varchar(64) NOT NULL,
  `bankdarah` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `datars`
--

INSERT INTO `datars` (`idrs`, `namars`, `tahunberdiri`, `direktur`, `jenis`, `kelas`, `status`, `kepemilikan`, `namapenyelenggara`, `bpjs`, `alamat`, `kab`, `luastanah`, `luasbangunan`, `telpon`, `email`, `website`, `noizin`, `suratizin`, `berlaku`, `akreditasi`, `penetapan`, `aktifakreditasi`, `layananunggulan`, `simrs`, `bankdarah`) VALUES
(1, 'RS Umum Daerah Syarifah Ambami Rato Ebu', '1959', 'Dr. Nunuk Kristiani', 'Rumah Sakit Umum', 'B', 'BLUD', 'Pemkab', 'Pemkab Bangkalan', 'Ya', 'Jalan Pemuda Kaffa No. 09 Bangkalan', 'Bangkalan', '25.247', '18.439', '3091111', 'eres_bangkalan@yahoo.co.id', 'www.rsabangkalan.com', 'P2T/6/03.22/01/VI/2016', '06-06-2016', '06-06-2021', 'Tingkat Utama', '23-09-2019', '01-01-2019', 'CT Scan, Atroschpy, HD', 'Berfungsi - Front Office, Back Office', 'Ada');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dpa`
--

CREATE TABLE `dpa` (
  `iddpa` int(11) NOT NULL,
  `idindikator` int(11) NOT NULL,
  `idrekening` int(11) NOT NULL,
  `sumberdana` varchar(256) NOT NULL,
  `jumlah` int(16) NOT NULL,
  `tw` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `tglinput` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `dpa`
--

INSERT INTO `dpa` (`iddpa`, `idindikator`, `idrekening`, `sumberdana`, `jumlah`, `tw`, `tahun`, `tglinput`) VALUES
(6, 1, 6, 'DAUM', 1200000, 'TW1', '2020', '2020-09-16'),
(7, 1, 7, 'DAUM', 850000, 'TW1', '2020', '2020-09-15'),
(8, 1, 9, 'DAUM', 350000, 'TW1', '2020', '2020-09-07'),
(9, 1, 10, 'DAUM', 1800000, 'TW1', '2020', '2020-09-03'),
(10, 1, 11, 'DAUM', 1800000, 'TW1', '2020', '2020-09-09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fasilitastt`
--

CREATE TABLE `fasilitastt` (
  `idfasilitas` int(11) NOT NULL,
  `jenispelayanan` varchar(64) NOT NULL,
  `vvip` int(3) NOT NULL,
  `vip` int(3) NOT NULL,
  `kelas1` int(3) NOT NULL,
  `kelas2` int(3) NOT NULL,
  `kelas3` int(3) NOT NULL,
  `khusus` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `fasilitastt`
--

INSERT INTO `fasilitastt` (`idfasilitas`, `jenispelayanan`, `vvip`, `vip`, `kelas1`, `kelas2`, `kelas3`, `khusus`) VALUES
(1, 'Penyakit Dalam', 0, 0, 16, 20, 40, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `indikatorkerja`
--

CREATE TABLE `indikatorkerja` (
  `idindikatorkerja` int(11) NOT NULL,
  `idraker` int(11) NOT NULL,
  `indikator` varchar(255) NOT NULL,
  `capaianawal` varchar(64) NOT NULL,
  `t1t` varchar(64) NOT NULL,
  `t1r` bigint(12) NOT NULL,
  `t2t` varchar(64) NOT NULL,
  `t2r` bigint(12) NOT NULL,
  `t3t` varchar(64) NOT NULL,
  `t3r` bigint(12) NOT NULL,
  `t4t` varchar(64) NOT NULL,
  `t4r` bigint(12) NOT NULL,
  `t5t` varchar(64) NOT NULL,
  `t5r` bigint(12) NOT NULL,
  `targetakhir` varchar(64) NOT NULL,
  `realisasiakhir` bigint(12) NOT NULL,
  `idunitkerja` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `indikatorkerja`
--

INSERT INTO `indikatorkerja` (`idindikatorkerja`, `idraker`, `indikator`, `capaianawal`, `t1t`, `t1r`, `t2t`, `t2r`, `t3t`, `t3r`, `t4t`, `t4r`, `t5t`, `t5r`, `targetakhir`, `realisasiakhir`, `idunitkerja`) VALUES
(1, 1, 'Jumlah petugas yang memperoleh vaksin hepatitis B sebagai tindakan pencegahan', '100 Orang', '100 Orang', 127500000, '100 Orang', 127500000, '100 Orang', 127500000, '100 Orang', 127500000, '100 Orang', 127500000, '500 Orang', 637500000, 2),
(2, 2, 'Jumlah alat dan media promosi kesehatan', '1 Paket', '1 Paket', 222500000, '1 Paket', 222500000, '1 Paket', 222500000, '1 Paket', 222500000, '1 Paket', 222500000, '1 Paket', 1125000000, 3),
(3, 3, 'Jumlah petugas dan dokter yang terpapar', '27 Orang', '27 Orang', 150000000, '27 Orang', 150000000, '27 Orang', 150000000, '27 Orang', 150000000, '27 Orang', 150000000, '27 Orang', 750000000, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `indikatorlayanan`
--

CREATE TABLE `indikatorlayanan` (
  `idindikatorlayanan` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `bor` varchar(64) NOT NULL,
  `los` varchar(64) NOT NULL,
  `bto` varchar(64) NOT NULL,
  `toi` varchar(64) NOT NULL,
  `ndr` varchar(64) NOT NULL,
  `gdr` varchar(64) NOT NULL,
  `ratarata` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `indikatorlayanan`
--

INSERT INTO `indikatorlayanan` (`idindikatorlayanan`, `tahun`, `bor`, `los`, `bto`, `toi`, `ndr`, `gdr`, `ratarata`) VALUES
(1, 2019, '63,17', '2,96', '79,16', '1,70', '21,11', '54,00', '445');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kodeprogram`
--

CREATE TABLE `kodeprogram` (
  `idkode` int(11) NOT NULL,
  `kodeprogram` varchar(64) NOT NULL,
  `namaprogram` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `kodeprogram`
--

INSERT INTO `kodeprogram` (`idkode`, `kodeprogram`, `namaprogram`) VALUES
(1, '1.02.39.', 'Program Pelayanan Kesehatan Masyarakat'),
(2, '1.02.39.02', 'Penurunan Faktor Resiko Penyakit Menular Termasuk Imunisasi'),
(3, '1.02.39.03', 'Peningkatan Promosi Kesehatan'),
(4, '1.02.39.04', 'Peningkatan Kesehatan Keluarga');

-- --------------------------------------------------------

--
-- Struktur dari tabel `koderekening`
--

CREATE TABLE `koderekening` (
  `idkoderek` int(11) NOT NULL,
  `koderekening` varchar(64) NOT NULL,
  `uraian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `koderekening`
--

INSERT INTO `koderekening` (`idkoderek`, `koderekening`, `uraian`) VALUES
(5, '5.2.1', 'Belanja Pegawai'),
(6, '5.2.1.01', 'Honorium PNS'),
(7, '5.2.1.01.02', 'Honorarium Tim Pengadaan Barang Dan Jasa'),
(9, '5.2.1.01.10', 'Honor Pejabat Pelaksan Teknis Kegiatan'),
(10, '5.2.1.02', 'Honorarium Non Pns'),
(11, '5.2.1.02.02', 'Honor / Uang Transport Kegiatan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelayanan`
--

CREATE TABLE `pelayanan` (
  `idpelayanan` int(11) NOT NULL,
  `namalayanan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pelayanan`
--

INSERT INTO `pelayanan` (`idpelayanan`, `namalayanan`) VALUES
(1, 'Pelayanan Medik Dasar'),
(2, 'Pelayanan Medik Gigi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengunjungirja`
--

CREATE TABLE `pengunjungirja` (
  `idpengunjung` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `namalayanan` varchar(64) NOT NULL,
  `laki` int(11) NOT NULL,
  `perempuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pengunjungirja`
--

INSERT INTO `pengunjungirja` (`idpengunjung`, `nomorbulan`, `bulan`, `tahun`, `namalayanan`, `laki`, `perempuan`) VALUES
(1, 1, 'Januari', '2019', 'Penyakit Dalam', 549, 729);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengunjungirna`
--

CREATE TABLE `pengunjungirna` (
  `idpengunjung` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `namalayanan` varchar(64) NOT NULL,
  `laki` int(11) NOT NULL,
  `perempuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pengunjungirna`
--

INSERT INTO `pengunjungirna` (`idpengunjung`, `nomorbulan`, `bulan`, `tahun`, `namalayanan`, `laki`, `perempuan`) VALUES
(1, 1, 'Januari', '2019', 'Penyakit Dalam', 549, 729);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengunjungrs`
--

CREATE TABLE `pengunjungrs` (
  `idpengunjung` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `barul` int(11) NOT NULL,
  `barup` int(11) NOT NULL,
  `lamal` int(11) NOT NULL,
  `lamap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pengunjungrs`
--

INSERT INTO `pengunjungrs` (`idpengunjung`, `nomorbulan`, `bulan`, `tahun`, `barul`, `barup`, `lamal`, `lamap`) VALUES
(1, 1, 'Januari', '2019', 2642, 3197, 3131, 3869);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pokja`
--

CREATE TABLE `pokja` (
  `idprogram` int(11) NOT NULL,
  `idkode` int(11) NOT NULL,
  `indikator` varchar(255) NOT NULL,
  `capaianawal` varchar(64) NOT NULL,
  `t1t` varchar(64) NOT NULL,
  `t1r` bigint(12) NOT NULL,
  `t2t` varchar(64) NOT NULL,
  `t2r` bigint(12) NOT NULL,
  `t3t` varchar(64) NOT NULL,
  `t3r` bigint(12) NOT NULL,
  `t4t` varchar(64) NOT NULL,
  `t4r` bigint(12) NOT NULL,
  `t5t` varchar(64) NOT NULL,
  `t5r` bigint(12) NOT NULL,
  `targetakhir` varchar(64) NOT NULL,
  `realisasiakhir` bigint(12) NOT NULL,
  `idunitkerja` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pokja`
--

INSERT INTO `pokja` (`idprogram`, `idkode`, `indikator`, `capaianawal`, `t1t`, `t1r`, `t2t`, `t2r`, `t3t`, `t3r`, `t4t`, `t4r`, `t5t`, `t5r`, `targetakhir`, `realisasiakhir`, `idunitkerja`) VALUES
(1, 1, 'Presentase Capaian Layanan Kesehatan Kepada Masyarakat', '100%', '100%', 500000, '100%', 500000, '100%', 500000, '100%', 500000, '100%', 500000, '100%', 2500000000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `raker`
--

CREATE TABLE `raker` (
  `idraker` int(11) NOT NULL,
  `idkode` int(11) NOT NULL,
  `t1` bigint(12) NOT NULL,
  `t2` bigint(12) NOT NULL,
  `t3` bigint(12) NOT NULL,
  `t4` bigint(12) NOT NULL,
  `t5` bigint(12) NOT NULL,
  `akhir` bigint(12) NOT NULL,
  `idpokja` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `raker`
--

INSERT INTO `raker` (`idraker`, `idkode`, `t1`, `t2`, `t3`, `t4`, `t5`, `akhir`, `idpokja`) VALUES
(1, 2, 127500000, 127500000, 127500000, 127500000, 127500000, 637500000, 1),
(2, 3, 222500000, 222500000, 222500000, 222500000, 222500000, 1112500000, 1),
(3, 4, 150000000, 150000000, 150000000, 150000000, 150000000, 750000000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tenirja`
--

CREATE TABLE `tenirja` (
  `idten` int(11) NOT NULL,
  `kodeicd` varchar(64) NOT NULL,
  `deskripsi` varchar(64) NOT NULL,
  `hidupl` int(11) NOT NULL,
  `hidupp` int(11) NOT NULL,
  `matil` int(11) NOT NULL,
  `matip` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tenirja`
--

INSERT INTO `tenirja` (`idten`, `kodeicd`, `deskripsi`, `hidupl`, `hidupp`, `matil`, `matip`, `nomorbulan`, `bulan`, `tahun`) VALUES
(1, 'K04.1', 'Peny. Pulpa', 68, 75, 0, 1, 1, 'Januari', '2019');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tenirna`
--

CREATE TABLE `tenirna` (
  `idten` int(11) NOT NULL,
  `kodeicd` varchar(64) NOT NULL,
  `deskripsi` varchar(64) NOT NULL,
  `hidupl` int(11) NOT NULL,
  `hidupp` int(11) NOT NULL,
  `matil` int(11) NOT NULL,
  `matip` int(11) NOT NULL,
  `nomorbulan` int(11) NOT NULL,
  `bulan` varchar(64) NOT NULL,
  `tahun` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tenirna`
--

INSERT INTO `tenirna` (`idten`, `kodeicd`, `deskripsi`, `hidupl`, `hidupp`, `matil`, `matip`, `nomorbulan`, `bulan`, `tahun`) VALUES
(1, 'R50.9', 'Febris', 68, 75, 0, 1, 1, 'Januari', '2019');

-- --------------------------------------------------------

--
-- Struktur dari tabel `triwulanan`
--

CREATE TABLE `triwulanan` (
  `idtriwulanan` int(11) NOT NULL,
  `idindikatorkerja` int(11) NOT NULL,
  `alokasi` int(11) NOT NULL,
  `tahun` varchar(64) NOT NULL,
  `jenistw` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `triwulanan`
--

INSERT INTO `triwulanan` (`idtriwulanan`, `idindikatorkerja`, `alokasi`, `tahun`, `jenistw`) VALUES
(1, 1, 83000000, '2019', 'TW1'),
(2, 1, 0, '2019', 'TW2'),
(3, 1, 0, '2019', 'TW3'),
(4, 1, 54000000, '2019', 'TW4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttrs`
--

CREATE TABLE `ttrs` (
  `idtt` int(11) NOT NULL,
  `kelas` varchar(64) NOT NULL,
  `jumlah` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ttrs`
--

INSERT INTO `ttrs` (`idtt`, `kelas`, `jumlah`) VALUES
(1, 'VVIP/Suoer VIP', '4'),
(2, 'VIP', '18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `unitkerja`
--

CREATE TABLE `unitkerja` (
  `idunitkerja` int(11) NOT NULL,
  `namaunit` varchar(256) NOT NULL,
  `keterangan` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `unitkerja`
--

INSERT INTO `unitkerja` (`idunitkerja`, `namaunit`, `keterangan`) VALUES
(1, 'Unit Kerja 1', 'Keterangna Unit Kerja 1'),
(2, 'Unit Kerja 2', 'Keterangan Unit Kerja 2'),
(3, 'Unit Kerja 3', 'Keterangan Unit Kerja 3'),
(5, 'Unit Kerja 4', 'Keterangan Unit Kerja 4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `nama` varchar(256) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `idunitkerja` int(11) NOT NULL,
  `jabatan` varchar(64) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'o ruangan 1 admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`iduser`, `nama`, `username`, `password`, `idunitkerja`, `jabatan`, `status`) VALUES
(1, 'Administrator ', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'Administrator', 1),
(2, 'Sofian Eka Sandra', 'fianeka', 'c21f969b5f03d33d43e04f8f136e7682', 1, 'Coba Jabatan', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dataibubayi`
--
ALTER TABLE `dataibubayi`
  ADD PRIMARY KEY (`idibu`);

--
-- Indeks untuk tabel `datajiwa`
--
ALTER TABLE `datajiwa`
  ADD PRIMARY KEY (`idjiwa`);

--
-- Indeks untuk tabel `datars`
--
ALTER TABLE `datars`
  ADD PRIMARY KEY (`idrs`);

--
-- Indeks untuk tabel `dpa`
--
ALTER TABLE `dpa`
  ADD PRIMARY KEY (`iddpa`);

--
-- Indeks untuk tabel `fasilitastt`
--
ALTER TABLE `fasilitastt`
  ADD PRIMARY KEY (`idfasilitas`);

--
-- Indeks untuk tabel `indikatorkerja`
--
ALTER TABLE `indikatorkerja`
  ADD PRIMARY KEY (`idindikatorkerja`);

--
-- Indeks untuk tabel `indikatorlayanan`
--
ALTER TABLE `indikatorlayanan`
  ADD PRIMARY KEY (`idindikatorlayanan`);

--
-- Indeks untuk tabel `kodeprogram`
--
ALTER TABLE `kodeprogram`
  ADD PRIMARY KEY (`idkode`);

--
-- Indeks untuk tabel `koderekening`
--
ALTER TABLE `koderekening`
  ADD PRIMARY KEY (`idkoderek`);

--
-- Indeks untuk tabel `pelayanan`
--
ALTER TABLE `pelayanan`
  ADD PRIMARY KEY (`idpelayanan`);

--
-- Indeks untuk tabel `pengunjungirja`
--
ALTER TABLE `pengunjungirja`
  ADD PRIMARY KEY (`idpengunjung`);

--
-- Indeks untuk tabel `pengunjungirna`
--
ALTER TABLE `pengunjungirna`
  ADD PRIMARY KEY (`idpengunjung`);

--
-- Indeks untuk tabel `pengunjungrs`
--
ALTER TABLE `pengunjungrs`
  ADD PRIMARY KEY (`idpengunjung`);

--
-- Indeks untuk tabel `pokja`
--
ALTER TABLE `pokja`
  ADD PRIMARY KEY (`idprogram`);

--
-- Indeks untuk tabel `raker`
--
ALTER TABLE `raker`
  ADD PRIMARY KEY (`idraker`);

--
-- Indeks untuk tabel `tenirja`
--
ALTER TABLE `tenirja`
  ADD PRIMARY KEY (`idten`);

--
-- Indeks untuk tabel `tenirna`
--
ALTER TABLE `tenirna`
  ADD PRIMARY KEY (`idten`);

--
-- Indeks untuk tabel `triwulanan`
--
ALTER TABLE `triwulanan`
  ADD PRIMARY KEY (`idtriwulanan`);

--
-- Indeks untuk tabel `ttrs`
--
ALTER TABLE `ttrs`
  ADD PRIMARY KEY (`idtt`);

--
-- Indeks untuk tabel `unitkerja`
--
ALTER TABLE `unitkerja`
  ADD PRIMARY KEY (`idunitkerja`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dataibubayi`
--
ALTER TABLE `dataibubayi`
  MODIFY `idibu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `datajiwa`
--
ALTER TABLE `datajiwa`
  MODIFY `idjiwa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `datars`
--
ALTER TABLE `datars`
  MODIFY `idrs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `dpa`
--
ALTER TABLE `dpa`
  MODIFY `iddpa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `fasilitastt`
--
ALTER TABLE `fasilitastt`
  MODIFY `idfasilitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `indikatorkerja`
--
ALTER TABLE `indikatorkerja`
  MODIFY `idindikatorkerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `indikatorlayanan`
--
ALTER TABLE `indikatorlayanan`
  MODIFY `idindikatorlayanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `kodeprogram`
--
ALTER TABLE `kodeprogram`
  MODIFY `idkode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `koderekening`
--
ALTER TABLE `koderekening`
  MODIFY `idkoderek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `pelayanan`
--
ALTER TABLE `pelayanan`
  MODIFY `idpelayanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pengunjungirja`
--
ALTER TABLE `pengunjungirja`
  MODIFY `idpengunjung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pengunjungirna`
--
ALTER TABLE `pengunjungirna`
  MODIFY `idpengunjung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pengunjungrs`
--
ALTER TABLE `pengunjungrs`
  MODIFY `idpengunjung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pokja`
--
ALTER TABLE `pokja`
  MODIFY `idprogram` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `raker`
--
ALTER TABLE `raker`
  MODIFY `idraker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tenirja`
--
ALTER TABLE `tenirja`
  MODIFY `idten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tenirna`
--
ALTER TABLE `tenirna`
  MODIFY `idten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `triwulanan`
--
ALTER TABLE `triwulanan`
  MODIFY `idtriwulanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `ttrs`
--
ALTER TABLE `ttrs`
  MODIFY `idtt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `unitkerja`
--
ALTER TABLE `unitkerja`
  MODIFY `idunitkerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
