<?php
// Turn off all error reporting
error_reporting(0);
session_start();
include("../config.php");
include("../library/mylib.php");
require __DIR__ . '/Mike42/autoloader.php';
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("connection failed".mysqli_errno());
$idt = $_GET['idt'];

$sql = "SELECT * FROM `transaksi`, user WHERE transaksi.idpetugas = user.id and  idtransaksi =".$idt;
$resulttrans =mysqli_query($con,$sql);
$rowtrans = mysqli_fetch_assoc($resulttrans);

$date = date('d-m-Y',strtotime($rowtrans['tglorder']));
$time = date('H:i:s',strtotime($rowtrans['tglorder']));

$sql ="SELECT * FROM transaksidetail, produk WHERE transaksidetail.idproduk = produk.idproduk and transaksidetail.idtransaksi =".$idt;
$result =mysqli_query($con,$sql);

// membuat connector printer ke shared printer bernama "printer_a" (yang telah disetting sebelumnya)
$connector = new Escpos\PrintConnectors\WindowsPrintConnector("Printer2");
// membuat objek $printer agar dapat di lakukan fungsinya
$printer = new Escpos\Printer($connector);


/* Information for the receipt */
$items = array();
$jml = 0;
// output data of each row
while($row = mysqli_fetch_assoc($result)) {
  array_push($items,new item($row["qty"]." x ".str_pad($row["namaproduk"],24)."".$row["hargajual"], $row["hargajual"] * $row["qty"]));
}

$invoice = new item('Invoice', $rowtrans['invoice']);
$kodekasir = new item('Kode Kasir', $rowtrans['kodekasir']);
$subtotal = new item('Subtotal', $rowtrans['totalbelanja']);
$discount = new item('Diskon'."(".$rowtrans['discountpercent']."%)", $rowtrans['discount']);
$tax = new item('Ppn (10%)', $rowtrans['tax']);
$jmlbayar = new item('Jumlah Bayar', $rowtrans['jmlbayar']);
$kembalian = new item('Kembalian', $rowtrans['jmlbayar'] - $rowtrans['total'] );
$total = new item('Total', $rowtrans['total'], true);
/* Date is kept the same for testing */
// $date = date('l jS \of F Y h:i:s A');
// $date1 = $date ."-" $time;

/* Start the printer */
// $logo = EscposImage::load("resources/escpos-php.png", false);
// $printer = new Printer($connector);

/* Print top logo */
// $printer -> setJustification(Printer::JUSTIFY_CENTER);
// $printer -> graphics($logo);

/* Name of shop */
$printer -> selectPrintMode(Escpos\Printer::MODE_DOUBLE_WIDTH);
$printer -> setJustification(Escpos\Printer::JUSTIFY_CENTER);
$printer -> text("SURAMADU RESTO.\n");
$printer -> selectPrintMode();
$printer -> text("Fasilitas Ruang VIP & Permainan Anak.\n");
$printer -> selectPrintMode();
$printer -> text("Jln Raya Tangkel, Burneh, Bangkalan - Madura\n");
$printer -> text("Telpon :0812-3141-7651\n\n");
$printer -> selectPrintMode(Escpos\Printer::MODE_DOUBLE_WIDTH);
// $printer -> selectPrintMode(Escpos\Printer::MODE_DOUBLE_HEIGHT);
$printer -> setJustification(Escpos\Printer::JUSTIFY_CENTER);
$printer -> setTextSize(3, 3);
$printer -> text($rowtrans['nomeja']."\n");
$printer -> feed();

/* Title of receipt */
$printer -> setEmphasis(true);
$printer -> selectPrintMode();
$printer -> setJustification(Escpos\Printer::JUSTIFY_CENTER);
$printer -> text("NO INVOICES : ".$rowtrans['invoice']." | Kasir : ".$rowtrans['nama']."\n");
$printer -> text("Tanggal : ".$date." | Jam : ".$time."\n");
// $printer -> text("Nama Pembeli : ".$rowtrans['atasnama']."\n");
$printer -> setEmphasis(false);

/* Items */
$printer -> setJustification(Escpos\Printer::JUSTIFY_LEFT);
$printer -> setEmphasis(true);
$printer -> text(new item('', 'Rp.'));
$printer -> setEmphasis(false);
foreach ($items as $item) {
    $printer -> text($item);
}
$printer -> setEmphasis(true);
$printer -> text($subtotal);
$printer -> setEmphasis(false);
$printer -> feed();

/* Tax and total */
$printer -> text($discount);
$printer -> text($tax);
$printer -> selectPrintMode(Escpos\Printer::MODE_DOUBLE_WIDTH);
$printer -> text($total);
$printer -> selectPrintMode();
$printer -> text($jmlbayar);
$printer -> text($kembalian);

/* Footer */
$printer -> feed(2);
$printer -> setJustification(Escpos\Printer::JUSTIFY_CENTER);
$printer -> text("SURAMADU RESTO\n");
$printer -> text("Terimakasih Telah Berkujung Ke Resto Kami\n");
$printer -> feed(2);
// $printer -> text($date . "\n");

/* Cut the receipt and open the cash drawer */
$printer -> cut();
$printer -> pulse();

$printer -> close();
header('Location: '."http://localhost/surapos/app/");


 /* A wrapper to do organise item names & prices into columns */
class item
{
    private $name;
    private $price;
    private $dollarSign;

    public function __construct($name = '', $price = '', $dollarSign = false)
    {
        $this -> name = $name;
        $this -> price = $price;
        $this -> dollarSign = $dollarSign;
    }

    public function __toString()
    {
        $rightCols = 10;
        $leftCols = 38;
        if ($this -> dollarSign) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($this -> name, $leftCols) ;

        $sign = ($this -> dollarSign ? 'Rp. ' : '');
        $right = str_pad($sign . $this -> price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }
}
?>
