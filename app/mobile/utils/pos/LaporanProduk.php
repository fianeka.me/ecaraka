<?php
session_start();
include("../../config.php");
include("../../library/mylib.php");
$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("connection failed".mysqli_errno());
$tgl = $_GET['tgl'];
$tgl = date('Y-m-d', strtotime($tgl));
$tglnext = date('Y-m-d', strtotime($tgl . ' +1 day'));
$sql ="SELECT transaksidetail.idproduk, SUM(transaksidetail.qty) AS jml, produk.hargajual, produk.namaproduk FROM transaksidetail, produk WHERE produk.idproduk = transaksidetail.idproduk AND tgltrans like '".$tgl."%'GROUP BY idproduk";
$result=mysqli_query($con,$sql);
?>


<?php if (mysqli_num_rows($result) > 0): ?>
  <a href="http://localhost/surapos/app/report/CetakClosingan.php?&&idt=<?php echo $tgl;?>" class="btn btn-primary btn-icon-split btn-sm" type="button">
    <span class="icon text-white-50">
      <i class="fas fa-file-pdf"></i>
    </span>
    <span class="text">Cetak Laporan</span>
  </a>
  <hr>
  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
      <tr>
        <th>No</oh>
        <th>Nama Menu</th>
        <th>Jumlah Terjual</th>
        <th>Harga Satuan</th>
        <th>Total</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th>No</oh>
        <th>Nama Menu</th>
        <th>Jumlah Terjual</th>
        <th>Harga Satuan</th>
        <th>Total</th>
      </tr>
    </tfoot>
    <tbody>
      <tbody>
        <?php
        $jml = 0;
        $no = 1;
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
        ?>
        <tr style="border-bottom: 1px solid grey">
          <td><?php echo $no; ?></td>
          <td><?php echo $row["namaproduk"]; ?></td>
          <td><?php echo $row["jml"]." Porsi"; ?></td>
          <td><?php echo rupiah((int)$row["hargajual"]); ?></td>
          <td><?php echo rupiah((int)$row["hargajual"] * $row["jml"]); ?></td>
        </tr>
        <?php
        $no++;
        }
        ?>
      </tbody>
    </tbody>
  </table>
<?php else: ?>
  <!-- Belum Ada Pesanan Di Transaksi Ini -->
<?php endif; ?>
