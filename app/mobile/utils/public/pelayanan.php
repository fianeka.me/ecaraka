<?php
session_start();
// if (isset($_SESSION['toprint'])) {
  $toprint = $_SESSION['toprint'];
  include '../../library/mylib.php';



// }
?>
<pre>
  <!-- <?php print_r($toprint["pengunjungirja"]); ?> -->
</pre>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Laporan Program</title>
  <!-- <link rel="stylesheet" href="table.css"> -->
  <style>

    body {
        background: #fff;
        font-size: 18px;
    }

    table {
     border-collapse: collapse;
    }

    table, th, td {
     border: 1px solid black;
     padding: 8px;
     text-align: left;
    }

    .content {
/* 72 dpi (web) = 595 X 842 pixels */
        height: 500px;
        width: 1080px;
        margin: auto;
        background: white;
        padding: 10px;
    }
    hr.style15 {
      border-top: 6px double #8c8b8b;
    }
    #prety {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        /* width: 100%; */
    }

    #prety td, #prety th {
        /* border: 1px solid #ddd; */
        padding: 7px;
    }
  </style>
</head>
<body>
  <?php if (isset($_SESSION['toprint'])): ?>
    <div class="content">
      <hr class="style15">
      <br><br>
      <?php
        $date = date('Y-m-d');
      ?>
      <center><u><h2>DATA LAPORAN PELAYANAN</h2></u></center>

      <h4>FORMULIR RL 1.1 DATA DASAR RUMAH SAKIT</h4>
        <table border="1">
        <tbody>
          <tr>
            <th>Nama Fasyankes</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->namars; ?></td>
          </tr>
          <tr>
            <th>Tahun Berdiri</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->tahunberdiri; ?></td>
          </tr>
          <tr>
            <th>Nama Direktur</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->direktur; ?></td>
          </tr>
          <tr>
            <th>Jenis</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->jenis; ?></td>
          </tr>
          <tr>
            <th>Kelas</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->kelas; ?></td>
          </tr>
          <tr>
            <th>Status BLU</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->status; ?></td>
          </tr>
          <tr>
            <th>Kepemilikan</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->kepemilikan; ?></td>
          </tr>
          <tr>
            <th>Nama Penyelenggara</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->namapenyelenggara; ?></td>
          </tr>
          <tr>
            <th>Kerjasama BPJS</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->bpjs; ?></td>
          </tr>
          <tr>
            <th>Alamat RS</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->alamat; ?></td>
          </tr>
          <tr>
            <th>Kota</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->kab; ?></td>
          </tr>
          <tr>
            <th>Luas Tanah</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->luastanah; ?></td>
          </tr>
          <tr>
            <th>Luas Bangunan</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->luasbangunan; ?></td>
          </tr>
          <tr>
            <th>Nomor Telpon</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->telpon; ?></td>
          </tr>
          <tr>
            <th>Email</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->email; ?></td>
          </tr>
          <tr>
            <th>Website</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->website; ?></td>
          </tr>
          <tr>
            <th>No Surat Izin Operasional</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->noizin; ?></td>
          </tr>
          <tr>
            <th>Tanggal Surat Izin</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->suratizin; ?></td>
          </tr>
          <tr>
            <th>Tanggal Berlaku Surat Izin</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->berlaku; ?></td>
          </tr>
          <tr>
            <th>Pentahapan Akreditasi</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->akreditasi; ?></td>
          </tr>
          <tr>
            <th>Penetapan Akreditasi</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->penetapan; ?></td>
          </tr>
          <tr>
            <th>Tanggal Berlaku Akreditasi</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->aktifakreditasi; ?></td>
          </tr>
          <tr>
            <th>Layanan Unggulan</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->layananunggulan; ?></td>
          </tr>
          <tr>
            <th>SIMRS</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->simrs; ?></td>
          </tr>
          <tr>
            <th>Bank Darah</th>
            <td>:</td>
            <td><?php echo $toprint["datautama"]->bankdarah; ?></td>
          </tr>
        </tbody>
        </table>
        <br><br>

      <h4>FORMULIR RL 1.1 DATA TEMPAT TIDUR</h4>
      <table class="table table-bordered" id="" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Nomor</th>
            <th>Kelas</th>
            <th>Jumlah Tempat Tidur</th>
          </tr>
        </thead>
        <tbody>
          <?php $nomor = 1; ?>
          <?php foreach ($toprint["ttrs"] as $usr): ?>
            <tr>
              <td>
                <?php echo $nomor; ?>
              </td>
              <td>
                <?php echo $usr->kelas; ?>
              </td>
              <td>
                <?php echo $usr->jumlah; ?>
              </td>
            </tr>
            <?php $nomor = $nomor + 1; ?>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br><br>

      <h4>FORMULIR RL 1.1 DATA PELAYANAN</h4>
      <table class="table table-bordered" id="" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Layanan</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($toprint["pelayanan"] as $usr): ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $usr->namalayanan; ?>
              </td>
              <?php $no = $no + 1; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br><br>

      <h4>FORMULIR RL 1.2 INDIKATOR PELAYANAN RUMAH SAKIT</h4>
      <table class="table table-bordered" id="" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Tahun</th>
            <th>BOR</th>
            <th>LOS</th>
            <th>BTO</th>
            <th>TOI</th>
            <th>NDR</th>
            <th>GDR</th>
            <th>RATA RATA KUNJUNGAN HARIAN</th>
          </tr>
        </thead>
        <tbody>
            <tr>
              <td>
                <?php echo $toprint["idk"]->tahun; ?>
              </td>
              <td>
                <?php echo $toprint["idk"]->bor; ?>
              </td>
              <td>
                <?php echo $toprint["idk"]->los; ?>
              </td>
              <td>
                <?php echo $toprint["idk"]->bto; ?>
              </td>
              <td>
                <?php echo $toprint["idk"]->toi; ?>
              </td>
              <td>
                <?php echo $toprint["idk"]->ndr; ?>
              </td>
              <td>
                <?php echo $toprint["idk"]->gdr; ?>
              </td>
              <td>
                <?php echo $toprint["idk"]->ratarata; ?>
              </td>
            </tr>
        </tbody>
      </table>
      <br><br>

      <h4>FORMULIR RL 1.3 FASILITAS TEMPAT TIDUR RAWAT INAP</h4>
      <table class="table table-bordered" id="" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Jenis Pelayanan</th>
            <th>VVIP</th>
            <th>VIP</th>
            <th>I</th>
            <th>II</th>
            <th>III</th>
            <th>Kelas Khusus</th>
            <th>Jumlah Total</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($toprint["fasilitastt"] as $usr): ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $usr->jenispelayanan; ?>
              </td>
              <td>
                <?php echo $usr->vvip; ?>
              </td>
              <td>
                <?php echo $usr->vip; ?>
              </td>
              <td>
                <?php echo $usr->kelas1; ?>
              </td>
              <td>
                <?php echo $usr->kelas2; ?>
              </td>
              <td>
                <?php echo $usr->kelas3; ?>
              </td>
              <td>
                <?php echo $usr->khusus; ?>
              </td>
              <td>
                <?php echo $usr->vvip+$usr->vip+$usr->kelas1+$usr->kelas2+$usr->kelas3+$usr->khusus; ?>
              </td>
              <?php $no = $no + 1; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br><br>

      <h4>FORMULIR RL 5.1 DATA BULANAN PENGUNJUNG RUMAH SAKIT</h4>
      <?php $no = 1; ?>
      <?php foreach ($toprint["pengunjungrs"] as $usr): ?>
        <?php if (!empty($usr[0]->bulan)): ?>
          Bulan : <?php echo $usr[0]->bulan." ".$toprint["tahunberjalan"]; ?>
          <table class="table table-bordered" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>No</th>
                <th>Jenis Kegiatan</th>
                <th>Laki Laki</th>
                <th>Perempuan</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td> 1 </td>
                <td> Pengunjung Baru</td>
                <td> <?php echo $usr[0]->barul; ?></td>
                <td> <?php echo $usr[0]->barup; ?></td>
                <td> <?php echo $usr[0]->barul+$usr[0]->barup; ?></td>
              </tr>
              <tr>
                <td> 2</td>
                <td> Pengunjung Lama</td>
                <td> <?php echo $usr[0]->lamal; ?></td>
                <td> <?php echo $usr[0]->lamap; ?></td>
                <td> <?php echo $usr[0]->lamal+$usr[0]->lamap; ?></td>
              </tr>
            </tbody>
          </table>
          <br><br>
        <?php endif; ?>
      <?php endforeach; ?>

      <h4>FORMULIR RL 5.2 DATA BULANAN KUNJUNGAN RAWAT JALAN </h4>
      <?php $no = 1; ?>
      <?php foreach ($toprint["pengunjungirja"] as $key => $value): ?>
        <?php if (!empty($value)): ?>
          Bulan : <?php echo $key." ".$toprint["tahunberjalan"]; ?>
          <table class="table table-bordered" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>No</th>
                <th>Jenis Kegiatan</th>
                <th>Laki Laki</th>
                <th>Perempuan</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; ?>
              <?php foreach ($value as $usr): ?>
                <tr>
                  <td>
                    <?php echo $no; ?>
                  </td>
                  <td>
                    <?php echo $usr->namalayanan; ?>
                  </td>
                  <td>
                    <?php echo $usr->laki; ?>
                  </td>
                  <td>
                    <?php echo $usr->perempuan; ?>
                  </td>
                  <td>
                    <?php echo $usr->laki + $usr->perempuan; ?>
                  </td>
                  <?php $no = $no + 1; ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <br><br>
        <?php endif; ?>
      <?php endforeach; ?>
      <br><br>

      <h4>FORMULIR RL 5.2 DATA BULANAN KUNJUNGAN RAWAT INAP </h4>
      <?php $no = 1; ?>
      <?php foreach ($toprint["pengunjungirna"] as $key => $value): ?>
        <?php if (!empty($value)): ?>
          Bulan : <?php echo $key." ".$toprint["tahunberjalan"]; ?>
          <table class="table table-bordered" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>No</th>
                <th>Jenis Kegiatan</th>
                <th>Laki Laki</th>
                <th>Perempuan</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; ?>
              <?php foreach ($value as $usr): ?>
                <tr>
                  <td>
                    <?php echo $no; ?>
                  </td>
                  <td>
                    <?php echo $usr->namalayanan; ?>
                  </td>
                  <td>
                    <?php echo $usr->laki; ?>
                  </td>
                  <td>
                    <?php echo $usr->perempuan; ?>
                  </td>
                  <td>
                    <?php echo $usr->laki + $usr->perempuan; ?>
                  </td>
                  <?php $no = $no + 1; ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <br><br>
        <?php endif; ?>
      <?php endforeach; ?>
      <br><br>

      <h4>FORMULIR RL 5.2.2 DATA BULANAN KUNJUNGAN GANGGUAN JIWA RAWAT JALAN </h4>
      <table class="table table-bordered" id="" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Bulan</th>
            <th>Laki - Laki</th>
            <th>Perempuan</th>
            <th>Jumlah</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($toprint["datajiwa"] as $usr): ?>
            <?php if (!empty($usr)): ?>
              <tr>
                <td>
                  <?php echo $usr->bulan; ?>
                </td>
                <td>
                  <?php echo $usr->laki; ?>
                </td>
                <td>
                  <?php echo $usr->perempuan; ?>
                </td>
                <td>
                  <?php echo $usr->laki+$usr->perempuan; ?>
                </td>
                <?php $no = $no + 1; ?>
              </tr>
            <?php endif; ?>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br><br>

      <h4>FORMULIR RL 5.2.3 DATA BULANAN KEMATIAN IBU DAN BAYI </h4>
      <table class="table table-bordered" id="" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Bulan</th>
            <th>Jumlah Persalinan</th>
            <th>Kematian Ibu</th>
            <th>Kematian Bayi</th>
            <th>Penyebab Kematian Ibu</th>
            <th>Penyebab Kematian Bayi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($toprint["dataibu"] as $usr): ?>
            <?php if (!empty($usr)): ?>
              <tr>
                <td>
                  <?php echo $usr->bulan; ?>
                </td>
                <td>
                  <?php echo $usr->jumlahpersalinan; ?>
                </td>
                <td>
                  <?php echo $usr->jumlahkematianibu; ?>
                </td>
                <td>
                  <?php echo $usr->jumlahkematianbayi; ?>
                </td>
                <td>
                  <?php echo $usr->penyebabmatiibu; ?>
                </td>
                <td>
                  <?php echo $usr->penyebabmatibayi; ?>
                </td>
                <?php $no = $no + 1; ?>
              </tr>
            <?php endif; ?>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br><br>

      <h4>FORMULIR RL 5.3 DATA BULANAN 10 BESAR PENYAKIT RAWAT INAP </h4>
      <?php $no = 1; ?>
      <?php foreach ($toprint["tenirna"] as $key => $value): ?>
        <?php if (!empty($value)): ?>
          Bulan : <?php echo $key." ".$toprint["tahunberjalan"]; ?>
          <table class="table table-bordered" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Kode ICD 10</th>
                <th rowspan="2">Deskripsi</th>
                <th colspan="2">Pasien Keluar Hidup</th>
                <th colspan="2">Pasien Keluar Mati</th>
                <th rowspan="2">Total Hidup Dan Mati</th>
              </tr>
              <tr>
                <th>Laki Laki</th>
                <th>Perempuan</th>
                <th>Laki Laki</th>
                <th>Perempuan</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; ?>
              <?php foreach ($value as $usr): ?>
                <tr>
                  <td>
                    <?php echo $no; ?>
                  </td>
                  <td>
                    <?php echo $usr->kodeicd; ?>
                  </td>
                  <td>
                    <?php echo $usr->deskripsi; ?>
                  </td>
                  <td>
                    <?php echo $usr->hidupl; ?>
                  </td>
                  <td>
                    <?php echo $usr->hidupp; ?>
                  </td>
                  <td>
                    <?php echo $usr->matil; ?>
                  </td>
                  <td>
                    <?php echo $usr->matip; ?>
                  </td>
                  <td>
                    <?php echo $usr->hidupl+$usr->hidupp+$usr->matil+$usr->matip; ?>
                  </td>

                  <?php $no = $no + 1; ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <br><br>
        <?php endif; ?>
      <?php endforeach; ?>


      <h4>FORMULIR RL 5.3 DATA BULANAN 10 BESAR PENYAKIT RAWAT JALAN </h4>
      <?php $no = 1; ?>
      <?php foreach ($toprint["tenirja"] as $key => $value): ?>
        <?php if (!empty($value)): ?>
          Bulan : <?php echo $key." ".$toprint["tahunberjalan"]; ?>
          <table class="table table-bordered" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Kode ICD 10</th>
                <th rowspan="2">Deskripsi</th>
                <th colspan="2">Pasien Keluar Hidup</th>
                <th colspan="2">Pasien Keluar Mati</th>
                <th rowspan="2">Total Hidup Dan Mati</th>
              </tr>
              <tr>
                <th>Laki Laki</th>
                <th>Perempuan</th>
                <th>Laki Laki</th>
                <th>Perempuan</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; ?>
              <?php foreach ($value as $usr): ?>
                <tr>
                  <td>
                    <?php echo $no; ?>
                  </td>
                  <td>
                    <?php echo $usr->kodeicd; ?>
                  </td>
                  <td>
                    <?php echo $usr->deskripsi; ?>
                  </td>
                  <td>
                    <?php echo $usr->hidupl; ?>
                  </td>
                  <td>
                    <?php echo $usr->hidupp; ?>
                  </td>
                  <td>
                    <?php echo $usr->matil; ?>
                  </td>
                  <td>
                    <?php echo $usr->matip; ?>
                  </td>
                  <td>
                    <?php echo $usr->hidupl+$usr->hidupp+$usr->matil+$usr->matip; ?>
                  </td>

                  <?php $no = $no + 1; ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <br><br>
        <?php endif; ?>
      <?php endforeach; ?>
      <br><br>



    </div>
  <?php endif; ?>

  <script type="text/javascript">
    window.onload = function() { window.print(); }
  </script>
</body>
</html>
