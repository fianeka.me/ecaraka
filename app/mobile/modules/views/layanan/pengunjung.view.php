<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Pengunjung Rumah Sakit</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Data Pengunjung</span>
      </button>
      <hr>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Bulan</th>
            <th>Tahun</th>
            <th>Baru (L)</th>
            <th>Baru (P)</th>
            <th>Total Baru</th>
            <th>Lama (L)</th>
            <th>Lama (P)</th>
            <th>Total Lama</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Bulan</th>
            <th>Tahun</th>
            <th>Baru (L)</th>
            <th>Baru (P)</th>
            <th>Total Baru</th>
            <th>Lama (L)</th>
            <th>Lama (P)</th>
            <th>Total Lama</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($data["kode"] as $usr): ?>
            <?php $datas = $usr->idpengunjung."/".$usr->bulan."/".$usr->tahun
            ."/".$usr->barul."/".$usr->barup."/".$usr->lamal
            ."/".$usr->lamap; ?>
            <tr>
              <td>
                <?php echo $usr->bulan; ?>
              </td>
              <td>
                <?php echo $usr->tahun; ?>
              </td>
              <td>
                <?php echo $usr->barul; ?>
              </td>
              <td>
                <?php echo $usr->barup; ?>
              </td>
              <td>
                <?php echo $usr->barul+$usr->barup; ?>
              </td>
              <td>
                <?php echo $usr->lamal; ?>
              </td>
              <td>
                <?php echo $usr->lamap; ?>
              </td>
              <td>
                <?php echo $usr->lamal+$usr->lamap; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=layanan-pengunjungrs&&action=delete&&id=<?php echo $usr->idpengunjung; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
              <?php $no = $no + 1; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Kode Program</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=layanan-pengunjungrs&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-6">
              <input type="hidden" name="id" id="id" class="form-control">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Bulan</label>
                <select class="form-control" id='bulan' name="bulan">
                 <option value='Januari'>Januari</option>
                 <option value='Februari'>Februari</option>
                 <option value='Maret'>Maret</option>
                 <option value='April'>April</option>
                 <option value='Mei'>Mei</option>
                 <option value='Juni'>Juni</option>
                 <option value='Juli'>Juli</option>
                 <option value='Agustus'>Agustus</option>
                 <option value='September'>September</option>
                 <option value='Oktober'>Oktober</option>
                 <option value='November'>November</option>
                 <option value='Desember'>Desember</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Tahun</label>
                <select class="form-control" name="tahun" id="tahun">
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> Pengunjung Baru (L) </label>
                <input type="text" name="barul" id="barul" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> Pengunjung Baru (P) </label>
                <input type="text" name="barup" id="barup" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> Pengunjung Lama (L)</label>
                <input type="text" name="lamal" id="lamal" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01"> Pengunjung Lama (P)</label>
                <input type="text" name="lamap" id="lamap" class="form-control" required>
              </div>
            </div>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('bulan').value= data[1];
              document.getElementById('tahun').value= data[2];
              document.getElementById('barul').value= data[3];
              document.getElementById('barup').value= data[4];
              document.getElementById('lamal').value= data[5];
              document.getElementById('lamap').value= data[6];
            }

         });
    });
  </script>
