<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Utama Rumah Sakit</h6>
  </div>
  <div class="card-body">
    <form id="myForm" role="form" action="<?php echo PATH; ?>?page=layanan-datautama&&action=addchange" method="post">
      <div class="form-row">
        <div class="col-md-12">
          <input type="hidden" name="id" id="id" v class="form-control">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Nama Fasyankes</label>
            <input type="text" name="namars" id="namaunit" class="form-control" value="<?php echo $data["kode"]->namars; ?>" readonly>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Tahun Berdiri</label>
            <input type="text" name="tahunberdiri" id="namaunit" class="form-control" value="<?php echo $data["kode"]->tahunberdiri; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Nama Direktur</label>
            <input type="text" name="direktur" id="namaunit" class="form-control" value="<?php echo $data["kode"]->direktur; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Jenis</label>
            <input type="text" name="jenis" id="namaunit" class="form-control" value="<?php echo $data["kode"]->jenis; ?>" readonly>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Kelas</label>
            <input type="text" name="kelas" id="namaunit" class="form-control" value="<?php echo $data["kode"]->kelas; ?>" readonly>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Status BLU</label>
            <input type="text" name="status" id="namaunit" class="form-control" value="<?php echo $data["kode"]->status; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Kepemilikan</label>
            <input type="text" name="kepemilikan" id="namaunit" class="form-control" value="<?php echo $data["kode"]->kepemilikan; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Nama Penyelenggara</label>
            <input type="text" name="namapenyelenggara" id="namaunit" class="form-control" value="<?php echo $data["kode"]->namapenyelenggara; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Kerjasama BPJS</label>
            <input type="text" name="bpjs" id="namaunit" class="form-control" value="<?php echo $data["kode"]->bpjs; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Alamat RS</label>
            <input type="text" name="alamat" id="namaunit" class="form-control" value="<?php echo $data["kode"]->alamat; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Kota</label>
            <input type="text" name="kab" id="namaunit" class="form-control" value="<?php echo $data["kode"]->kab; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Luas Tanah</label>
            <input type="text" name="luastanah" id="namaunit" class="form-control" value="<?php echo $data["kode"]->luastanah; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Luas Bangunan</label>
            <input type="text" name="luasbangunan" id="namaunit" class="form-control" value="<?php echo $data["kode"]->luasbangunan; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Nomor Telpon</label>
            <input type="text" name="telpon" id="namaunit" class="form-control" value="<?php echo $data["kode"]->telpon; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Email</label>
            <input type="text" name="email" id="namaunit" class="form-control" value="<?php echo $data["kode"]->email; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Website</label>
            <input type="text" name="website" id="namaunit" class="form-control" value="<?php echo $data["kode"]->website; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">No Surat Izin Operasional</label>
            <input type="text" name="noizin" id="namaunit" class="form-control" value="<?php echo $data["kode"]->noizin; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Tanggal Surat Izin</label>
            <input type="text" name="suratizin" id="namaunit" class="form-control" value="<?php echo $data["kode"]->suratizin; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Tanggal Berlaku Surat Izin</label>
            <input type="text" name="berlaku" id="namaunit" class="form-control" value="<?php echo $data["kode"]->berlaku; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Pentahapan Akreditasi</label>
            <input type="text" name="akreditasi" id="namaunit" class="form-control" value="<?php echo $data["kode"]->akreditasi; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Penetapan Akreditasi</label>
            <input type="text" name="penetapan" id="namaunit" class="form-control" value="<?php echo $data["kode"]->penetapan; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <input type="hidden" name="id" id="id" class="form-control">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Tanggal Berlaku Akreditasi</label>
            <input type="text" name="aktifakreditasi" id="namaunit" class="form-control" value="<?php echo $data["kode"]->aktifakreditasi; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Layanan Unggulan</label>
            <input type="text" name="layananunggulan" id="namaunit" class="form-control" value="<?php echo $data["kode"]->layananunggulan; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">SIMRS</label>
            <input type="text" name="simrs" id="namaunit" class="form-control" value="<?php echo $data["kode"]->simrs; ?>" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Bank Darah</label>
            <input type="text" name="bankdarah" id="namaunit" class="form-control" value="<?php echo $data["kode"]->bankdarah; ?>" required>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
  </form>
  </div>
</div>
