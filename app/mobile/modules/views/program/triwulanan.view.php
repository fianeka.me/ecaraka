<!-- DataTales Example -->

<div class="row">
  <div class="col-md-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Informasi Indikator Kerja</h6>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered">
                <tr>
                  <th>Nama Indikator</th>
                  <th>:</th>
                  <th><?php echo $data["indikator"]->indikator; ?></th>
                </tr>

                <tr>
                  <td>Tahun 2019<br></td>
                  <td>:</td>
                  <td><?php echo rupiah($data["indikator"]->t1r); ?></td>
                </tr>

                <tr>
                  <td>Tahun 2020<br></td>
                  <td>:</td>
                  <td><?php echo rupiah($data["indikator"]->t1r); ?></td>
                </tr>

                <tr>
                  <td>Tahun 2021<br></td>
                  <td>:</td>
                  <td><?php echo rupiah($data["indikator"]->t1r); ?></td>
                </tr>

                <tr>
                  <td>Tahun 2022<br></td>
                  <td>:</td>
                  <td><?php echo rupiah($data["indikator"]->t1r); ?></td>
                </tr>

                <tr>
                  <td>Tahun 2023<br></td>
                  <td>:</td>
                  <td><?php echo rupiah($data["indikator"]->t1r); ?></td>
                </tr>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Triwulanan</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
            <span class="icon text-white-50">
              <i class="fas fa-plus"></i>
            </span>
            <span class="text">Set Triwulanan Baru</span>
          </button>
          <hr>
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Tahun</th>
                <th>Jenis Tw</th>
                <th>Jumlah</th>
                <th>Tindakan</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Kode</th>
                <th>Uraian</th>
                <th>Jumlah</th>
                <th>Tindakan</th>
              </tr>
            </tfoot>
            <tbody>
              <?php foreach ($data["tw"] as $usr): ?>
                <tr>
                  <td>
                    <?php echo $usr->tahun; ?>
                  </td>
                  <td>
                    <?php echo $usr->jenistw; ?>
                  </td>
                  <td>
                    <?php echo rupiah($usr->alokasi); ?>
                  </td>
                  <td>
                    <a href="<?php echo SITE_URL; ?>?page=program-triwulanan&&action=delete&&id=<?php echo $usr->idtriwulanan; ?>" onclick="return confirm('Data Akan Di Hapus ?');"  class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                          <i class="fas fa-trash"></i>
                        </span>
                        <span class="text">Hapus</span>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade bd-example-modal-xl" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-m" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Setting Triwulanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=program-triwulanan&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Tahun</label>
                <select class="form-control" name="thn" id="thn">
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                </select>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Jenis Tw</label>
                <select class="form-control" name="jenistw" id="jenistw">
                  <option value="TW1">TW1</option>
                  <option value="TW2">TW2</option>
                  <option value="TW3">TW3</option>
                  <option value="TW4">TW4</option>
                </select>
              </div>
            </div>

            <div class="col-md-12">
              <input type="hidden" name="id" id="id" class="form-control">
              <input type="hidden" name="idi" id="idi" value="<?php echo $data["indikator"]->idindikatorkerja; ?>" class="form-control">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Jumlah Dana</label>
                <input type="number" name="jumlah" id="jumlah" class="form-control" placeholder="" required>
              </div>
            </div>


          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('idi').value= data[1];
              document.getElementById('jumlah').value= data[4];
              document.getElementById('dana').value= data[3];
              document.getElementById('koderek').value= data[2];
            }

         });
    });
  </script>
