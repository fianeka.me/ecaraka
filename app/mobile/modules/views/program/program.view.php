<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Program</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Program Baru</span>
      </button>
      <hr>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Kode</th>
            <th>Nama Program</th>
            <th>Indikator</th>
            <th>Unit Kerja</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Kode</th>
            <th>Nama Program</th>
            <th>Indikator</th>
            <th>Unit Kerja</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php foreach ($data["program"] as $usr): ?>
            <?php
            $datas = $usr->idprogram."/".$usr->kodeprogram.
            "/".$usr->indikator."/".$usr->capaianawal.
            "/".$usr->t1t."/".$usr->t1r.
            "/".$usr->t2t."/".$usr->t2r.
            "/".$usr->t3t."/".$usr->t3r.
            "/".$usr->t4t."/".$usr->t4r.
            "/".$usr->t5t."/".$usr->t5r.
            "/".$usr->targetakhir."/".$usr->realisasiakhir."/".$usr->idunitkerja;
            ?>
            <tr>
              <td>
                <?php echo $usr->kodeprogram; ?>
              </td>
              <td>
                <?php echo $usr->namaprogram; ?>
              </td>
              <td>
                <?php echo $usr->indikator; ?>
              </td>

              <td>
                <?php echo $usr->namaunit; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>"  class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-flag"></i>
                    </span>
                    <span class="text"></span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=program-program&&action=delete&&id=<?php echo $usr->idprogram; ?>" onclick="return confirm('Data Akan Di Hapus ?');"  class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text"></span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=program-program&&action=detail&&id=<?php echo $usr->idprogram; ?>" class="btn btn-info btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-info"></i>
                    </span>
                    <span class="text"> Detail</span>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- <pre>
  <?php print_r($data["unitkerja"]); ?>
</pre> -->

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Rencana Program Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=program-program&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-12">
              <input type="hidden" name="id" id="id" class="form-control">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Nama Program : </label>
                <select name="kodeprogram" class="form-control js-example-basic-single" id="kodeprogram">
                  <?php
                      foreach ($data["kodeprogram"] as $bahan) {
                          ?>
                          <option value="<?php echo $bahan->idkode; ?>"><?php echo $bahan->kodeprogram." - ".$bahan->namaprogram;?></option>
                      <?php
                      }
                  ?>
                </select>
              </div>
              <hr>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Indikator</label>
                <input type="text" name="indikator" id="indikator" class="form-control" placeholder="ex : Capaian awal ..." required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Capaian Awal</label>
                <input type="text" name="capaianawal" id="capaianawal" class="form-control" placeholder="ex : 100%" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Penanggung Jawab</label>
                <select name="unit" class="form-control" id="unit">
                  <?php
                      foreach ($data["unitkerja"] as $bahan) {
                          ?>
                          <option value="<?php echo $bahan->idunitkerja; ?>"><?php echo $bahan->namaunit;?></option>
                      <?php
                      }
                  ?>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <hr>
            </div>
            <div class="col-md-1">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">T2019</label>
                <input type="text" name="t1" id="t1" class="form-control" placeholder="" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label"s for="validationDefault01">RP2019</label>
                <input type="text" name="r1" id="r1" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-1">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">T2020</label>
                <input type="text" name="t2" id="t2" class="form-control" placeholder="" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">RP2020</label>
                <input type="text" name="r2" id="r2" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-1">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">T2021</label>
                <input type="text" name="t3" id="t3" class="form-control" placeholder="" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">RP2021</label>
                <input type="text" name="r3" id="r3" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-1">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">T2022</label>
                <input type="text" name="t4" id="t4" class="form-control" placeholder="" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">RP2022</label>
                <input type="text" name="r4" id="r4" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-1">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">T2023</label>
                <input type="text" name="t5" id="t5" class="form-control" placeholder="" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">RP2023</label>
                <input type="text" name="r5" id="r5" class="form-control" placeholder="" required>
              </div>
            </div>

            <div class="col-md-1">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Target</label>
                <input type="text" name="takhir" id="takhir" class="form-control" placeholder="" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Nominal</label>
                <input type="text" name="rakhir" id="rakhir" class="form-control" placeholder="" required>
              </div>
            </div>

          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            console.log(rowdata);
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              $("#kodeprogram").val(data[16]).trigger('change');
              document.getElementById('indikator').value= data[2];
              document.getElementById('capaianawal').value= data[3];
              document.getElementById('unit').value= data[16];
              $('#unit').val(data[16]);
              document.getElementById('t1').value= data[4];
              document.getElementById('r1').value= data[5];
              document.getElementById('t2').value= data[6]
              document.getElementById('r2').value= data[7];
              document.getElementById('t3').value= data[8];
              document.getElementById('r3').value= data[9]
              document.getElementById('t4').value= data[10];
              document.getElementById('r4').value= data[11];
              document.getElementById('t5').value= data[12]
              document.getElementById('r5').value= data[13];
              document.getElementById('takhir').value= data[14];
              document.getElementById('rakhir').value= data[15]
            }

         });
    });
  </script>
  <script type="text/javascript">
  (function($) {
    'use strict';
    if ($(".js-example-basic-single").length) {
      $(".js-example-basic-single").select2();
    }
  })(jQuery);
  </script>
