<!-- DataTales Example -->
  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Proses Laporan</h6>
        </div>
        <div class="card-body">
          <form id="myForm" role="form" action="<?php echo PATH; ?>?page=laporan-program" method="post">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-group">
                  <select class="form-control" name="tahun" id="tahun">
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-block btn-sm btn-primary">Proses Laporan Program</button>
          </div>
        </form>
        </div>
    </div>
    <?php if (isset($data["kode"])): ?>
      <div class="col-lg-12">
        <div class="list-group">
          <a href="#" class="list-group-item list-group-item-action active">
            <?php echo "Laporan Tahun ". $data["thn"]; ?>
          </a>
          <ul class="list-group">
            <?php foreach ($data["kode"]["data"] as $usr): ?>
              <li class="list-group-item d-flex justify-content-between align-items-center">
                <a style="color: inherit;text-decoration: none;" href="<?php echo PATH; ?>?page=laporan-program&&action=detail&&id=<?php echo $usr["program"]->idprogram; ?>&&tahun=<?php echo $data["thn"];?>"><?php echo $usr["program"]->kodeprogram." ".$usr["program"]->namaprogram;?></a>
                <span class="badge badge-warning badge-pill"><?php echo count($usr["raker"]); ?></span>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    <?php endif; ?>
  </div>
  <br>
