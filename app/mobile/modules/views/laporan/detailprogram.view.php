<!-- DataTales Example -->
  <div class="row">
    <?php if (isset($data["kode"])): ?>
      <div class="col-lg-12">
        <div class="list-group">

            <?php foreach ($data["kode"]["data"] as $usr): ?>
              <div class="accordion" id="accordionExample">
                <?php foreach ($usr["raker"] as $usr1): ?>
                  <div class="card">
                    <div class="card-header" id="headingOne">
                      <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#<?php echo "string".$usr1["raker"]->idraker; ?>" aria-expanded="true" aria-controls="collapseOne">
                          <?php echo $usr1["raker"]->kodeprogram." ".$usr1["raker"]->namaprogram;?>
                        </button>
                      </h2>
                    </div>
                    <div id="<?php echo "string".$usr1["raker"]->idraker;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body">
                        <dl class="row">
                          <dt class="col-sm-3">Kode Program</dt>
                          <dd class="col-sm-9"><?php echo $usr1["raker"]->kodeprogram; ?></dd>

                          <dt class="col-sm-3">Nama Program</dt>
                          <dd class="col-sm-9"><?php echo $usr1["raker"]->namaprogram; ?></dd>

                          <?php
                            for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                          ?>
                          <?php if ($x==2019): ?>
                            <dt class="col-sm-3"><?php echo "Pagu ".$x;?></dt>
                            <dd class="col-sm-9"><?php echo rupiah($usr1["raker"]->t1); ?></dd>
                          <?php endif; ?>
                          <?php if ($x==2020): ?>
                            <dt class="col-sm-3"><?php echo "Pagu ".$x;?></dt>
                            <dd class="col-sm-9"><?php echo rupiah($usr1["raker"]->t2); ?></dd>
                          <?php endif; ?>
                          <?php if ($x==2021): ?>
                            <dt class="col-sm-3"><?php echo "Pagu ".$x;?></dt>
                            <dd class="col-sm-9"><?php echo rupiah($usr1["raker"]->t3); ?></dd>
                          <?php endif; ?>
                          <?php if ($x==2022): ?>
                            <dt class="col-sm-3"><?php echo "Pagu ".$x;?></dt>
                            <dd class="col-sm-9"><?php echo rupiah($usr1["raker"]->t4); ?></dd>
                          <?php endif; ?>
                          <?php if ($x==2023): ?>
                            <dt class="col-sm-3"><?php echo "Pagu ".$x;?></dt>
                            <dd class="col-sm-9"><?php echo rupiah($usr1["raker"]->t5); ?></dd>
                          <?php endif; ?>
                          <?php
                            }
                          ?>
                        </dl>

                        <?php $datatahunan = array('2019' => $usr1["raker"]->t1, '2020' => $usr1["raker"]->t2, '2021' => $usr1["raker"]->t3, '2022' => $usr1["raker"]->t4, '2023' => $usr1["raker"]->t5 ); ?>
                        <?php foreach ($usr1["indikator"] as $usr2): ?>
                          <div class="alert alert-info" role="alert">
                            <b>
                              <?php echo $usr2["indikator"]->indikator; ?>
                            </b>
                          </div>
                          <div class="alert alert-warning" role="alert">
                            <b>
                              <?php echo $usr2["indikator"]->namaunit; ?>
                            </b>
                          </div>

                          <div class="row">
                            <div class="col-12">
                              <div class="list-group list-group-horizontal-lg" id="list-tab" role="tablist">
                                <?php
                                  for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                                ?>
                                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-<?php echo $usr2["indikator"]->idindikatorkerja.$x; ?>" role="tab" aria-controls="profile"><?php echo $x; ?></a>
                                <?php
                                  }
                                ?>
                              </div>
                            </div>
                            <hr>
                            <div class="col-12">
                              <hr>
                              <div class="tab-content" id="nav-tabContent">
                                <?php
                                  for ($x = 2019; $x <= $data["kode"]["tahunberjalan"]; $x++) {
                                ?>
                                <div class="tab-pane fade" id="list-<?php echo $usr2["indikator"]->idindikatorkerja.$x; ?>" role="tabpanel" aria-labelledby="list-profile-list">

                                  <div class="table-responsive">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Data <?php echo $x ?></th>
                                          <th>Total Belanja</th>
                                          <th>Realisasi (%)</th>
                                        </tr>
                                      </thead>
                                      <?php $totalbelanja = $usr2["realisasi"][$x]->tw1+$usr2["realisasi"][$x]->tw2+$usr2["realisasi"][$x]->tw3+$usr2["realisasi"][$x]->tw4; ?>
                                      <tbody>
                                        <tr>
                                          <td>TW 1</td>
                                          <td><?php echo rupiah($usr2["realisasi"][$x]->tw1) ?></td>
                                          <td><?php echo round(($usr2["realisasi"][$x]->tw1/$datatahunan[$x])*100,2)."%"; ?></td>
                                        </tr>
                                        <tr>
                                          <td>TW 2</td>
                                          <td><?php echo rupiah($usr2["realisasi"][$x]->tw2) ?></td>
                                          <td><?php echo round(($usr2["realisasi"][$x]->tw2/$datatahunan[$x])*100,2)."%"; ?></td>
                                        </tr>
                                        <tr>
                                          <td>TW 3</td>
                                          <td><?php echo rupiah($usr2["realisasi"][$x]->tw3) ?></td>
                                          <td><?php echo round(($usr2["realisasi"][$x]->tw3/$datatahunan[$x])*100,2)."%"; ?></td>
                                        </tr>
                                        <tr>
                                          <td>TW 4</td>
                                          <td><?php echo rupiah($usr2["realisasi"][$x]->tw4) ?></td>
                                          <td><?php echo round(($usr2["realisasi"][$x]->tw4/$datatahunan[$x])*100,2)."%"; ?></td>
                                        </tr>
                                        <tr>
                                          <th>Total Pagu</th>
                                          <th>Total Belanja</th>
                                          <th>Total Realisasi</th>
                                        </tr>
                                        <tr>
                                          <td><?php echo rupiah($datatahunan[$x]) ?></td>
                                          <td><?php echo rupiah($totalbelanja) ?></td>
                                          <td><?php echo round(($totalbelanja/$datatahunan[$x])*100,2)."%" ?></td>
                                        </tr>
                                        <tr>
                                          <th>Sisa Uang</th>
                                          <th colspan="2"><?php echo rupiah($datatahunan[$x]-$totalbelanja) ?></th>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>

                                  <div class="chart-pie pt-4 pb-2">
                                    <canvas id="myPieChart-<?php echo $usr2["indikator"]->idindikatorkerja.$x; ?>"></canvas>
                                    <?php $w1 = round(($totalbelanja/$datatahunan[$x])*100,2); ?>
                                    <?php $w2 = 100 - round(($totalbelanja/$datatahunan[$x])*100,2); ?>
                                    <script type="text/javascript">
                                    var ctx = document.getElementById("myPieChart-<?php echo $usr2["indikator"]->idindikatorkerja.$x; ?>");
                                    var w1 = <?php echo(json_encode($w1)); ?>;
                                    var w2 = <?php echo(json_encode($w2)); ?>;
                                    var myPieChart = new Chart(ctx, {
                                      type: 'pie',
                                      data: {
                                        labels: ["Terealisasi", "Belum Terealisasi"],
                                        datasets: [{
                                          data: [w1, w2],
                                          backgroundColor: ['#4e73df', '#1cc88a'],
                                          hoverBorderColor: "rgba(234, 236, 244, 1)",
                                        }],
                                      },
                                      options: {
                                        maintainAspectRatio: false,
                                        tooltips: {
                                          backgroundColor: "rgb(255,255,255)",
                                          bodyFontColor: "#858796",
                                          borderColor: '#dddfeb',
                                          borderWidth: 1,
                                          xPadding: 15,
                                          yPadding: 15,
                                          displayColors: false,
                                          caretPadding: 10,
                                        },
                                        legend: {
                                          display: false
                                        },
                                        cutoutPercentage: 80,
                                      },
                                    });
                                    </script>
                                  </div>
                                  <div class="mt-4 text-center small">
                                    <span class="mr-2">
                                      <i class="fas fa-circle text-primary"></i> Terealisasi  <?php echo $w1."%"; ?>
                                    </span>
                                    <span class="mr-2">
                                      <i class="fas fa-circle text-success"></i> Belum Terealisasi <?php echo $w2."%"; ?>
                                    </span>
                                  </div>

                                </div>
                                <?php
                                  }
                                ?>
                              </div>
                            </div>
                          </div>

                        <?php endforeach; ?>
                      </div>

                    </div>
                  </div>
                  <br>
                <?php endforeach; ?>
              </div>
            <?php endforeach; ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
  <br>
