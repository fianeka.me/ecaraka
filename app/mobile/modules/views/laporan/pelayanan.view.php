<!-- DataTales Example -->
  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Proses Pelayanan</h6>
        </div>
        <div class="card-body">
          <form id="myForm" role="form" action="<?php echo PATH; ?>?page=laporan-pelayanan" method="post">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault01">Pilih Tahun</label>
                  <select class="form-control" name="tahun" id="tahun">
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-block btn-primary">Proses Laporan Pelayanan</button>
          </div>
        </form>
        </div>
      </div>
      <?php if (isset($data["datanya"])): ?>
<!--
        <div class="col-lg-6">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">FORMULIR RL 1.1 DATA DASAR RUMAH SAKIT</h6>
            </div>
            <div class="card-body tect-center">
                <a href="#" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Lihat Laporan</span>
                  </a>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">
              FORMULIR RL 1.1 DATA TEMPAT TIDUR
              </h6>
            </div>
            <div class="card-body tect-center">
                <a href="#" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Lihat Laporan</span>
                  </a>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">
              FORMULIR RL 1.1 DATA PELAYANAN
              </h6>
            </div>
            <div class="card-body tect-center">
                <a href="#" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Lihat Laporan</span>
                  </a>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">
              FORMULIR RL 1.2 INDIKATOR PELAYANAN RUMAH SAKIT
              </h6>
            </div>
            <div class="card-body tect-center">
                <a href="#" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Lihat Laporan</span>
                  </a>
            </div>
          </div>
        </div> -->
      <?php endif; ?>
      <div class="col-lg-12">
          <?php if (isset($data["datanya"])): ?>
            <div class="accordion" id="accordionExample">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      FORMULIR RL 1.1 DATA DASAR RUMAH SAKIT
                    </button>
                  </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="form-row">
                      <div class="col-md-12">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Nama Fasyankes</label>
                          <input type="text" name="namars" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->namars; ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Tahun Berdiri</label>
                          <input type="text" name="tahunberdiri" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->tahunberdiri; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Nama Direktur</label>
                          <input type="text" name="direktur" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->direktur; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Jenis</label>
                          <input type="text" name="jenis" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->jenis; ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Kelas</label>
                          <input type="text" name="kelas" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->kelas; ?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Status BLU</label>
                          <input type="text" name="status" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->status; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Kepemilikan</label>
                          <input type="text" name="kepemilikan" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->kepemilikan; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Nama Penyelenggara</label>
                          <input type="text" name="namapenyelenggara" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->namapenyelenggara; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Kerjasama BPJS</label>
                          <input type="text" name="bpjs" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->bpjs; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Alamat RS</label>
                          <input type="text" name="alamat" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->alamat; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Kota</label>
                          <input type="text" name="kab" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->kab; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Luas Tanah</label>
                          <input type="text" name="luastanah" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->luastanah; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Luas Bangunan</label>
                          <input type="text" name="luasbangunan" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->luasbangunan; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Nomor Telpon</label>
                          <input type="text" name="telpon" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->telpon; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Email</label>
                          <input type="text" name="email" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->email; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Website</label>
                          <input type="text" name="website" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->website; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">No Surat Izin Operasional</label>
                          <input type="text" name="noizin" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->noizin; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Tanggal Surat Izin</label>
                          <input type="text" name="suratizin" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->suratizin; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Tanggal Berlaku Surat Izin</label>
                          <input type="text" name="berlaku" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->berlaku; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Pentahapan Akreditasi</label>
                          <input type="text" name="akreditasi" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->akreditasi; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Penetapan Akreditasi</label>
                          <input type="text" name="penetapan" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->penetapan; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <input type="hidden" name="id" id="id" class="form-control">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Tanggal Berlaku Akreditasi</label>
                          <input type="text" name="aktifakreditasi" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->aktifakreditasi; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Layanan Unggulan</label>
                          <input type="text" name="layananunggulan" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->layananunggulan; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">SIMRS</label>
                          <input type="text" name="simrs" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->simrs; ?>" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="form-control-label" for="validationDefault01">Bank Darah</label>
                          <input type="text" name="bankdarah" id="namaunit" class="form-control" value="<?php echo $data["datanya"]["datautama"]->bankdarah; ?>" required>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwoi" aria-expanded="false" aria-controls="collapseTwo">
                      FORMULIR RL 1.1 DATA TEMPAT TIDUR
                    </button>
                  </h5>
                </div>
                <div id="collapseTwoi" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div class="card-body">
                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Nomor</th>
                          <th>Kelas</th>
                          <th>Jumlah</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $nomor = 1; ?>
                        <?php foreach ($data["datanya"]["ttrs"] as $usr): ?>
                          <tr>
                            <td>
                              <?php echo $nomor; ?>
                            </td>
                            <td>
                              <?php echo $usr->kelas; ?>
                            </td>
                            <td>
                              <?php echo $usr->jumlah; ?>
                            </td>
                          </tr>
                          <?php $nomor = $nomor + 1; ?>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 1.1 DATA PELAYANAN
                    </button>
                  </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Layanan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data["datanya"]["pelayanan"] as $usr): ?>
                          <?php $datas = $usr->idpelayanan."/".$usr->namalayanan; ?>
                          <tr>
                            <td>
                              <?php echo $no; ?>
                            </td>
                            <td>
                              <?php echo $usr->namalayanan; ?>
                            </td>
                            <?php $no = $no + 1; ?>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapseindikatorlayanan" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 1.2 INDIKATOR PELAYANAN RUMAH SAKIT
                    </button>
                  </h5>
                </div>
                <div id="colapseindikatorlayanan" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                      <tbody>
                          <tr>
                            <th>
                              Tahun
                            </th>
                            <th>
                              <?php echo $data["datanya"]["idk"]->tahun; ?>
                            </th>
                          </tr>
                          <tr>
                            <td>
                              BOR
                            </td>
                            <td>
                              <?php echo $data["datanya"]["idk"]->bor; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              LOS
                            </td>
                            <td>
                              <?php echo $data["datanya"]["idk"]->los; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              BTO
                            </td>
                            <td>
                              <?php echo $data["datanya"]["idk"]->bto; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              TOI
                            </td>
                            <td>
                              <?php echo $data["datanya"]["idk"]->toi; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              NDR
                            </td>
                            <td>
                              <?php echo $data["datanya"]["idk"]->ndr; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              GDR
                            </td>
                            <td>
                              <?php echo $data["datanya"]["idk"]->gdr; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              RATA
                            </td>
                            <td>
                              <?php echo $data["datanya"]["idk"]->ratarata; ?>
                            </td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapsettirna" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 1.3 FASILITAS TEMPAT TIDUR RAWAT INAP
                    </button>
                  </h5>
                </div>
                <div id="colapsettirna" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <ul class="list-group list-group-flush">
                      <?php foreach ($data["datanya"]["fasilitastt"] as $usr): ?>
                        <li class="list-group-item">
                          <dl class="row">
                            <dt class="col-sm-3">Jenis Pelayanan </dt>
                            <dd class="col-sm-9"><?php echo $usr->jenispelayanan; ?></dd>
                            <dt class="col-sm-3"> VVIP </dt>
                            <dd class="col-sm-9"><?php echo $usr->vvip; ?></dd>
                            <dt class="col-sm-3"> VIP </dt>
                            <dd class="col-sm-9"><?php echo $usr->vip; ?></dd>
                            <dt class="col-sm-3"> KELAS 1 </dt>
                            <dd class="col-sm-9"><?php echo $usr->kelas1; ?></dd>
                            <dt class="col-sm-3"> KELAS 2 </dt>
                            <dd class="col-sm-9"><?php echo $usr->kelas2; ?></dd>
                            <dt class="col-sm-3"> KELAS 3 </dt>
                            <dd class="col-sm-9"><?php echo $usr->kelas3; ?></dd>
                            <dt class="col-sm-3"> KHUSUS </dt>
                            <dd class="col-sm-9"><?php echo $usr->khusus; ?></dd>
                            <dt class="col-sm-3"> TOTAL </dt>
                            <dd class="col-sm-9"><?php echo $usr->vvip+$usr->vip+$usr->kelas1+$usr->kelas2+$usr->kelas3+$usr->khusus; ?></dd>
                          </dl>
                        </li>

                      <?php endforeach; ?>
                    </ul>

                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapsespenguungrs" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 5.1 DATA BULANAN PENGUNJUNG RUMAH SAKIT
                    </button>
                  </h5>
                </div>
                <div id="colapsespenguungrs" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <?php foreach ($data["datanya"]["pengunjungrs"] as $usr): ?>
                        <?php if (!empty($usr[0]->bulan)): ?>
                          <div class="col-lg-6">
                            <b>
                              Bulan : <?php echo $usr[0]->bulan." ".$data["datanya"]["tahunberjalan"]; ?>
                            </b>
                            <br>
                            <br>
                            <table class="table table-bordered" id="" width="100%" cellspacing="0">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Jenis Kegiatan</th>
                                  <th>Laki Laki</th>
                                  <th>Perempuan</th>
                                  <th>Jumlah</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td> 1 </td>
                                  <td> Pengunjung Baru</td>
                                  <td> <?php echo $usr[0]->barul; ?></td>
                                  <td> <?php echo $usr[0]->barup; ?></td>
                                  <td> <?php echo $usr[0]->barul+$usr[0]->barup; ?></td>
                                </tr>
                                <tr>
                                  <td> 2</td>
                                  <td> Pengunjung Lama</td>
                                  <td> <?php echo $usr[0]->lamal; ?></td>
                                  <td> <?php echo $usr[0]->lamap; ?></td>
                                  <td> <?php echo $usr[0]->lamal+$usr[0]->lamap; ?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapseirnakunjung" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 5.2 DATA BULANAN KUNJUNGAN RAWAT INAP
                    </button>
                  </h5>
                </div>
                <div id="colapseirnakunjung" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <?php foreach ($data["datanya"]["pengunjungirna"] as $key => $value): ?>
                        <?php if (!empty($value)): ?>
                          <div class="col-lg-6">
                            <b>
                              Bulan : <?php echo $key." ".$data["datanya"]["tahunberjalan"]; ?>
                            </b>
                            <br>
                            <br>
                            <table class="table table-bordered" id="" width="100%" cellspacing="0">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Jenis Kegiatan</th>
                                  <th>Laki Laki</th>
                                  <th>Perempuan</th>
                                  <th>Jumlah</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($value as $usr): ?>
                                  <tr>
                                    <td>
                                      <?php echo $no; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->namalayanan; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->laki; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->perempuan; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->laki + $usr->perempuan; ?>
                                    </td>
                                    <?php $no = $no + 1; ?>
                                  </tr>
                                <?php endforeach; ?>
                              </tbody>
                            </table>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapseirjakunjung" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 5.2 DATA BULANAN KUNJUNGAN RAWAT JALAN
                    </button>
                  </h5>
                </div>
                <div id="colapseirjakunjung" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <?php foreach ($data["datanya"]["pengunjungirja"] as $key => $value): ?>
                        <?php if (!empty($value)): ?>
                          <div class="col-lg-6">
                            <b>
                              Bulan : <?php echo $key." ".$data["datanya"]["tahunberjalan"]; ?>
                            </b>
                            <br>
                            <br>
                            <table class="table table-bordered" id="" width="100%" cellspacing="0">
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Jenis Kegiatan</th>
                                  <th>Laki Laki</th>
                                  <th>Perempuan</th>
                                  <th>Jumlah</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($value as $usr): ?>
                                  <tr>
                                    <td>
                                      <?php echo $no; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->namalayanan; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->laki; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->perempuan; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->laki + $usr->perempuan; ?>
                                    </td>
                                    <?php $no = $no + 1; ?>
                                  </tr>
                                <?php endforeach; ?>
                              </tbody>
                            </table>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapsegangguanjiwa" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 5.2.2 DATA BULANAN KUNJUNGAN GANGGUAN JIWA RAWAT JALAN
                    </button>
                  </h5>
                </div>
                <div id="colapsegangguanjiwa" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Bulan</th>
                          <th>Laki - Laki</th>
                          <th>Perempuan</th>
                          <th>Jumlah</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data["datanya"]["datajiwa"] as $usr): ?>
                          <?php if (!empty($usr)): ?>
                            <tr>
                              <td>
                                <?php echo $usr->bulan; ?>
                              </td>
                              <td>
                                <?php echo $usr->laki; ?>
                              </td>
                              <td>
                                <?php echo $usr->perempuan; ?>
                              </td>
                              <td>
                                <?php echo $usr->laki+$usr->perempuan; ?>
                              </td>
                              <?php $no = $no + 1; ?>
                            </tr>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapseibuanak" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 5.2.3 DATA BULANAN KEMATIAN IBU DAN BAYI
                    </button>
                  </h5>
                </div>
                <div id="colapseibuanak" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <table class="table table-bordered" id="" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Bulan</th>
                          <th>Jumlah Persalinan</th>
                          <th>Kematian Ibu</th>
                          <th>Kematian Bayi</th>
                          <th>Penyebab Kematian Ibu</th>
                          <th>Penyebab Kematian Bayi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data["datanya"]["dataibu"] as $usr): ?>
                          <?php if (!empty($usr)): ?>
                            <tr>
                              <td>
                                <?php echo $usr->bulan; ?>
                              </td>
                              <td>
                                <?php echo $usr->jumlahpersalinan; ?>
                              </td>
                              <td>
                                <?php echo $usr->jumlahkematianibu; ?>
                              </td>
                              <td>
                                <?php echo $usr->jumlahkematianbayi; ?>
                              </td>
                              <td>
                                <?php echo $usr->penyebabmatiibu; ?>
                              </td>
                              <td>
                                <?php echo $usr->penyebabmatibayi; ?>
                              </td>
                              <?php $no = $no + 1; ?>
                            </tr>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapsetenirna" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 5.3 DATA BULANAN 10 BESAR PENYAKIT RAWAT INAP
                    </button>
                  </h5>
                </div>
                <div id="colapsetenirna" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <?php $no = 1; ?>
                      <?php foreach ($data["datanya"]["tenirna"] as $key => $value): ?>
                        <?php if (!empty($value)): ?>
                          <div class="col-lg-6">
                            <b>
                              Bulan : <?php echo $key." ".$data["datanya"]["tahunberjalan"]; ?>
                            </b>
                            <br>
                            <br>
                            <table class="table table-bordered" id="" width="100%" cellspacing="0">
                              <thead>
                                <tr>
                                  <th rowspan="2">No</th>
                                  <th rowspan="2">Kode ICD 10</th>
                                  <th rowspan="2">Deskripsi</th>
                                  <th colspan="2">Pasien Keluar Hidup</th>
                                  <th colspan="2">Pasien Keluar Mati</th>
                                  <th rowspan="2">Total Hidup Dan Mati</th>
                                </tr>
                                <tr>
                                  <th>L</th>
                                  <th>P</th>
                                  <th>L</th>
                                  <th>P</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($value as $usr): ?>
                                  <tr>
                                    <td>
                                      <?php echo $no; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->kodeicd; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->deskripsi; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->hidupl; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->hidupp; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->matil; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->matip; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->hidupl+$usr->hidupp+$usr->matil+$usr->matip; ?>
                                    </td>

                                    <?php $no = $no + 1; ?>
                                  </tr>
                                <?php endforeach; ?>
                              </tbody>
                            </table>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#colapsetenirja" aria-expanded="false" aria-controls="collapseThree">
                      FORMULIR RL 5.3 DATA BULANAN 10 BESAR PENYAKIT RAWAT JALAN
                    </button>
                  </h5>
                </div>
                <div id="colapsetenirja" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <?php $no = 1; ?>
                      <?php foreach ($data["datanya"]["tenirja"] as $key => $value): ?>
                        <?php if (!empty($value)): ?>
                          <div class="col-lg-6">
                            <b>
                              Bulan : <?php echo $key." ".$data["datanya"]["tahunberjalan"]; ?>
                            </b>
                            <br>
                            <br>
                            <table class="table table-bordered" id="" width="100%" cellspacing="0">
                              <thead>
                                <tr>
                                  <th rowspan="2">No</th>
                                  <th rowspan="2">Kode ICD 10</th>
                                  <th rowspan="2">Deskripsi</th>
                                  <th colspan="2">Pasien Keluar Hidup</th>
                                  <th colspan="2">Pasien Keluar Mati</th>
                                  <th rowspan="2">Total Hidup Dan Mati</th>
                                </tr>
                                <tr>
                                  <th>L</th>
                                  <th>P</th>
                                  <th>L</th>
                                  <th>P</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($value as $usr): ?>
                                  <tr>
                                    <td>
                                      <?php echo $no; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->kodeicd; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->deskripsi; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->hidupl; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->hidupp; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->matil; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->matip; ?>
                                    </td>
                                    <td>
                                      <?php echo $usr->hidupl+$usr->hidupp+$usr->matil+$usr->matip; ?>
                                    </td>

                                    <?php $no = $no + 1; ?>
                                  </tr>
                                <?php endforeach; ?>
                              </tbody>
                            </table>
                          </div>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>
        </div>
  </div>
