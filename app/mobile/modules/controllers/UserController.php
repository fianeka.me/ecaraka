<?php
use \modules\controllers\MainController;

class UserController extends MainController {

    public function index() {
        $this->model('user');
        $data = $this->user->getJoin(array('unitkerja'),
            array(
              'user.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN'
        );

        $this->model('unitkerja');
        $data1 = $this->unitkerja->get();

        $this->template('user', array('user' => $data, 'unitkerja' => $data1));
    }

    public function addchange() {
        $this->model('user');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $nama  = isset($_POST["nama"]) ? $_POST["nama"] : "";
            $unitkerja = isset($_POST["unitkerja"]) ? $_POST["unitkerja"] : "";
            $username = isset($_POST["username"]) ? $_POST["username"] : "";
            $jabatan = isset($_POST["jabatan"]) ? $_POST["jabatan"] : "";

            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->user->insert(
                      array(
                        'nama' => $nama,
                        'username' => $username,
                        'password' => md5("default"),
                        'jabatan' => $jabatan,
                        'idunitkerja' => $unitkerja,
                        'status' => 0
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'nama' => $nama,
                'username' => $username,
                'jabatan' => $jabatan,
                'idunitkerja' => $unitkerja
              );
              if(count($error) == 0) {
                  $update = $this->user->update($updateArrayData, array('iduser' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
        // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
    }

    public function setting(){
      $this->model('user');
      $error      = array();
      $success    = null;
      $id =  $_SESSION["login"]->uniqid;
      $data = $this->user->getWhere(array(
          'uniqid' => $id
      ));
      if(count($data) == 0) $this->redirect(PATH);
      if($_SERVER["REQUEST_METHOD"] == "POST"){
          $psd       = isset($_POST["psdlama"])  ? $_POST["psdlama"] : "";
          $psdbaru1  = isset($_POST["psdbaru1"]) ? $_POST["psdbaru1"] : "";
          $psdbaru2  = isset($_POST["psdbaru2"]) ? $_POST["psdbaru2"] : "";
          if(empty($psd) || $psd == "") {
              array_push($error, "Password Lama wajib di isi.");
          }
          if(empty($psdbaru1) || $psdbaru1 == "") {
              array_push($error, "Password Baru wajib di isi.");
          }
          if(empty($psdbaru2) || $psdbaru2 == "") {
              array_push($error, "Pasword Baru Ulang wajib di isi.");
          }
          if (md5($psd) != $data[0]->password) {
            array_push($error, "Pasword lama tidak sesuai dengan sebelumnya");
          }else{
            if($psdbaru1 != $psdbaru2) {
                array_push($error, "Pasword Baru Tidak Cocok.");
            }
          }
          $updateArrayData = array(
              'password' => md5($psdbaru1)
          );

          if(count($error) == 0) {
              $update = $this->user->update($updateArrayData, array('uniqid' => $id));
              if($update) {
                  $success = "Password berhasil di rubah.";
              }
          }
      }
      $this->template('frmsetting', array('user' => $data[0],'error' => $error, 'success' => $success, 'title' => 'Kelola Password'));
    }

    public function resetPassword() {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('user');
        $update = $this->user->update(array('password' => "827ccb0eea8a706c4c34a16891f84e7b"), array('id' => $id));
        if($update) {
          // $this->back();
        }
    }
    public function delete()
    {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('user');
        $delete = $this->user->delete(array('iduser' => $id));
        if ($delete) {
            $this->back();
        }
    }


    public function change() {
        $this->model('user');
        $error      = array();
        $success    = null;
        $id = isset($_GET["id"]) ? $_GET["id"] : "";
        $todo = isset($_GET["todo"]) ? $_GET["todo"] : "";
        $data = $this->user->getWhere(array(
            'iduser' => $id
        ));
        if(count($data) == 0) $this->redirect(PATH . '?page=store');
        if ($todo==0) {
          $updateArrayData = array(
              'aktif' => '0'
          );
        }else{
          $updateArrayData = array(
              'aktif' => '1'
          );
        }
        if(count($error) == 0) {
            $update = $this->user->update($updateArrayData, array('iduser' => $id));
            if($update) {
              if ($todo==0) {
                $success = "User Telah Di-Nonaktifkan";
              }else{
                $success = "User Telah Di-Aktifkan";
              }
            }
        }
        $this->back();
        // $this->template('user', array('user' => $alldata,'success' => $success));
    }
}
?>
