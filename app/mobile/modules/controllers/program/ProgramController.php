<?php

use \modules\controllers\MainController;

class ProgramController extends MainController {

      public function index() {
        $this->model('pokja');
        $data = $this->pokja->getJoin(array('kodeprogram', 'unitkerja'),
            array(
              'pokja.idkode' => 'kodeprogram.idkode',
              'pokja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN'
        );

        $this->model('kodeprogram');
        $data1 = $this->kodeprogram->get();
        $this->model('unitkerja');
        $data2 = $this->unitkerja->get();

        $this->template('program/program', array("program"=>$data,"kodeprogram"=>$data1,"unitkerja"=>$data2));
      }

      public function detail() {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;

        $this->model('pokja');
        $data = $this->pokja->getJoin(array('kodeprogram', 'unitkerja'),
            array(
              'pokja.idkode' => 'kodeprogram.idkode',
              'pokja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN',
            array(
              'pokja.idprogram' => $id
            )
        );


        $this->model('raker');
        $data1 = $this->raker->getJoin(array('kodeprogram'),
            array(
              'raker.idkode' => 'kodeprogram.idkode'
            ),
            'JOIN',
            array(
              'raker.idpokja' => $id
            )
        );

        $this->model('kodeprogram');
        $data2 = $this->kodeprogram->get();

        $this->template('program/detailprogram', array("program"=>$data[0], "kodeprogram"=>$data2, "raker"=>$data1));
      }

      public function addchange() {
          $this->model('pokja');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $kodeprogram = isset($_POST["kodeprogram"]) ? $_POST["kodeprogram"] : "";
              $indikator = isset($_POST["indikator"]) ? $_POST["indikator"] : "";
              $capaianawal = isset($_POST["capaianawal"]) ? $_POST["capaianawal"] : "";
              $unit = isset($_POST["unit"]) ? $_POST["unit"] : "";
              $t1 = isset($_POST["t1"]) ? $_POST["t1"] : "";
              $r1 = isset($_POST["r1"]) ? $_POST["r1"] : "";
              $t2 = isset($_POST["t2"]) ? $_POST["t2"] : "";
              $r2 = isset($_POST["r2"]) ? $_POST["r2"] : "";
              $t3 = isset($_POST["t3"]) ? $_POST["t3"] : "";
              $r3 = isset($_POST["r3"]) ? $_POST["r3"] : "";
              $t4 = isset($_POST["t4"]) ? $_POST["t4"] : "";
              $r4 = isset($_POST["r4"]) ? $_POST["r4"] : "";
              $t5 = isset($_POST["t5"]) ? $_POST["t5"] : "";
              $r5 = isset($_POST["r5"]) ? $_POST["r5"] : "";
              $takhir = isset($_POST["takhir"]) ? $_POST["takhir"] : "";
              $rakhir = isset($_POST["rakhir"]) ? $_POST["rakhir"] : "";

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->pokja->insert(
                        array(
                          'idkode' => $kodeprogram,
                          'indikator' => $indikator,
                          'capaianawal' => $capaianawal,
                          'idunitkerja' => $unit,
                          't1t' => $t1,
                          't1r' => $r1,
                          't2t' => $t2,
                          't2r' => $r2,
                          't3t' => $t3,
                          't3r' => $r3,
                          't4t' => $t4,
                          't4r' => $r4,
                          't5t' => $t5,
                          't5r' => $r5,
                          'targetakhir' => $takhir,
                          'realisasiakhir' => $rakhir
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'idkode' => $kodeprogram,
                  'indikator' => $indikator,
                  'capaianawal' => $capaianawal,
                  'idunitkerja' => $unit,
                  't1t' => $t1,
                  't1r' => $r1,
                  't2t' => $t2,
                  't2r' => $r2,
                  't3t' => $t3,
                  't3r' => $r3,
                  't4t' => $t4,
                  't4r' => $r4,
                  't5t' => $t5,
                  't5r' => $r5,
                  'targetakhir' => $takhir,
                  'realisasiakhir' => $rakhir
                );
                if(count($error) == 0) {
                    $update = $this->pokja->update($updateArrayData, array('idprogram' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('pokja');
          $delete = $this->pokja->delete(array('idprogram' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
