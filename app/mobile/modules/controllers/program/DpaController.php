<?php

use \modules\controllers\MainController;

class DpaController extends MainController {

    public function index() {
      $this->model('unitkerja');
      $data1 = $this->unitkerja->get();
      $this->template('master/unitkerja', array("unitkerja"=>$data1));
    }

    public function addchange() {
        $this->model('dpa');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $idrek = isset($_POST["koderek"]) ? $_POST["koderek"] : "";
            $idi = isset($_POST["idi"]) ? $_POST["idi"] : "";
            $jumlah  = isset($_POST["jumlah"]) ? $_POST["jumlah"] : "";
            $dana  = isset($_POST["dana"]) ? $_POST["dana"] : "";
            $thn = isset($_POST["thn"]) ? $_POST["thn"] : "";
            $jenistw = isset($_POST["jenistw"]) ? $_POST["jenistw"] : "";

            $tgl = date("Y-m-d");

            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->dpa->insert(
                      array(
                        'idrekening' => $idrek,
                        'idindikator' => $idi,
                        'sumberdana' => $dana,
                        'tw' => $jenistw,
                        'tahun' => $thn,
                        'tglinput' => $tgl,
                        'jumlah' => $jumlah
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'idrekening' => $idrek,
                'idindikator' => $idi,
                'sumberdana' => $dana,
                'tw' => $jenistw,
                'tahun' => $thn,
                'jumlah' => $jumlah
              );
              if(count($error) == 0) {
                  $update = $this->dpa->update($updateArrayData, array('iddpa' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
        // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
    }

    public function delete()
    {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('dpa');
        $delete = $this->dpa->delete(array('iddpa' => $id));
        if ($delete) {
            $this->back();
        }
    }
}
?>
