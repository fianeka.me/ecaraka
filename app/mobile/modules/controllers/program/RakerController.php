<?php

use \modules\controllers\MainController;

class RakerController extends MainController {

      public function index() {
        $idr = isset($_GET["idr"]) ? $_GET["idr"] : 0;
        $idp = isset($_GET["idp"]) ? $_GET["idp"] : 0;

        $this->model('pokja');
        $data = $this->pokja->getJoin(array('kodeprogram', 'unitkerja'),
            array(
              'pokja.idkode' => 'kodeprogram.idkode',
              'pokja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN',
            array(
              'pokja.idprogram' => $idp
            )
        );


        $this->model('raker');
        $data1 = $this->raker->getJoin(array('kodeprogram'),
            array(
              'raker.idkode' => 'kodeprogram.idkode'
            ),
            'JOIN',
            array(
              'raker.idraker' => $idr
            )
        );

        $this->model('indikator');
        $data2 = $this->indikator->getJoin(array('unitkerja'),
            array(
              'indikatorkerja.idunitkerja' => 'unitkerja.idunitkerja'
            ),
            'JOIN',
            array(
              'indikatorkerja.idraker' => $idr
            )
        );

        $this->model('unitkerja');
        $data3 = $this->unitkerja->get();

        $this->template('program/raker', array("program"=>$data[0], "raker"=>$data1[0], "indikator"=>$data2, "unitkerja"=>$data3));
      }


      public function addchange() {
          $this->model('raker');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $idpokja = isset($_POST["idpokja"]) ? $_POST["idpokja"] : "";
              $kodeprogram = isset($_POST["kodeprogram"]) ? $_POST["kodeprogram"] : "";
              $r1 = isset($_POST["r1"]) ? $_POST["r1"] : "";
              $r2 = isset($_POST["r2"]) ? $_POST["r2"] : "";
              $r3 = isset($_POST["r3"]) ? $_POST["r3"] : "";
              $r4 = isset($_POST["r4"]) ? $_POST["r4"] : "";
              $r5 = isset($_POST["r5"]) ? $_POST["r5"] : "";
              $rakhir = isset($_POST["rakhir"]) ? $_POST["rakhir"] : "";

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->raker->insert(
                        array(
                          'idkode' => $kodeprogram,
                          'idpokja' => $idpokja,
                          't1' => $r1,
                          't2' => $r2,
                          't3' => $r3,
                          't4' => $r4,
                          't5' => $r5,
                          'akhir' => $rakhir
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'idkode' => $kodeprogram,
                  'idpokja' => $idpokja,
                  't1' => $r1,
                  't2' => $r2,
                  't3' => $r3,
                  't4' => $r4,
                  't5' => $r5,
                  'akhir' => $rakhir
                );
                if(count($error) == 0) {
                    $update = $this->raker->update($updateArrayData, array('idraker' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('raker');
          $delete = $this->raker->delete(array('idraker' => $id));
          if ($delete) {
              $this->back();
          }
      }

}
?>
