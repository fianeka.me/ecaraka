<?php

use \modules\controllers\MainController;

class PengunjungirjaController extends MainController {

      public function index() {
        $this->model('pengunjungirja');
        $data1 = $this->pengunjungirja->get();
        $this->template('layanan/pengunjungirja', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('pengunjungirja');
          $error      = array();
          $success    = null;
          $databulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $bulan  = isset($_POST["bulan"]) ? $_POST["bulan"] : "";
              $tahun  = isset($_POST["tahun"]) ? $_POST["tahun"] : "";
              $namalayanan  = isset($_POST["namalayanan"]) ? $_POST["namalayanan"] : "";
              $laki  = isset($_POST["laki"]) ? $_POST["laki"] : "";
              $perempuan  = isset($_POST["perempuan"]) ? $_POST["perempuan"] : "";
              $nomorbulan = array_search($bulan, $databulan)+1;

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->pengunjungirja->insert(
                        array(
                          'nomorbulan' => $nomorbulan,
                          'bulan' => $bulan,
                          'tahun' => $tahun,
                          'namalayanan' => $namalayanan,
                          'laki' => $laki,
                          'perempuan' => $perempuan
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'nomorbulan' => $nomorbulan,
                  'bulan' => $bulan,
                  'tahun' => $tahun,
                  'namalayanan' => $namalayanan,
                  'laki' => $laki,
                  'perempuan' => $perempuan
                );
                if(count($error) == 0) {
                    $update = $this->pengunjungirja->update($updateArrayData, array('idpengunjung' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('pengunjungirja');
          $delete = $this->pengunjungirja->delete(array('idpengunjung' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
