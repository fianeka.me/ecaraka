<?php

use \modules\controllers\MainController;

class IndikatorlayananController extends MainController {

      public function index() {
        $this->model('indikatorlayanan');
        $data1 = $this->indikatorlayanan->get();
        $this->template('layanan/indikator', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('indikatorlayanan');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $tahun  = isset($_POST["tahun"]) ? $_POST["tahun"] : "";
              $bor  = isset($_POST["bor"]) ? $_POST["bor"] : "";
              $los  = isset($_POST["los"]) ? $_POST["los"] : "";
              $bto  = isset($_POST["bto"]) ? $_POST["bto"] : "";
              $toi  = isset($_POST["toi"]) ? $_POST["toi"] : "";
              $ndr  = isset($_POST["ndr"]) ? $_POST["ndr"] : "";
              $gdr  = isset($_POST["gdr"]) ? $_POST["gdr"] : "";
              $ratarata  = isset($_POST["ratarata"]) ? $_POST["ratarata"] : "";

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->indikatorlayanan->insert(
                        array(
                          'tahun' => $tahun,
                          'bor' => $bor,
                          'los' => $los,
                          'bto' => $bto,
                          'toi' => $toi,
                          'ndr' => $ndr,
                          'gdr' => $gdr,
                          'ratarata' => $ratarata
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'tahun' => $tahun,
                  'bor' => $bor,
                  'los' => $los,
                  'bto' => $bto,
                  'toi' => $toi,
                  'ndr' => $ndr,
                  'gdr' => $gdr,
                  'ratarata' => $ratarata
                );
                if(count($error) == 0) {
                    $update = $this->indikatorlayanan->update($updateArrayData, array('idindikatorlayanan' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('indikatorlayanan');
          $delete = $this->indikatorlayanan->delete(array('idindikatorlayanan' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
