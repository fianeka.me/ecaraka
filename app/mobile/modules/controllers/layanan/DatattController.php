<?php

use \modules\controllers\MainController;

class DatattController extends MainController {

      public function index() {
        $this->model('ttrs');
        $data1 = $this->ttrs->get();
        $this->template('layanan/ttrs', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('ttrs');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $kelas  = isset($_POST["kelas"]) ? $_POST["kelas"] : "";
              $jumlahtt = isset($_POST["jumlahtt"]) ? $_POST["jumlahtt"] : "";

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->ttrs->insert(
                        array(
                          'kelas' => $kelas,
                          'jumlah' => $jumlahtt
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'kelas' => $kelas,
                  'jumlah' => $jumlahtt
                );
                if(count($error) == 0) {
                    $update = $this->ttrs->update($updateArrayData, array('idtt' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('ttrs');
          $delete = $this->ttrs->delete(array('idtt' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
