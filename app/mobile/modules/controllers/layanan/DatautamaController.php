<?php

use \modules\controllers\MainController;

class DatautamaController extends MainController {

      public function index() {
        $this->model('datautama');
        $data1 = $this->datautama->get();
        $this->template('layanan/datautama', array("kode"=>$data1[0]));
      }

      public function addchange() {
          $this->model('datautama');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $namars  = isset($_POST["namars"]) ? $_POST["namars"] : "";
              $tahunberdiri = isset($_POST["tahunberdiri"]) ? $_POST["tahunberdiri"] : "";
              $direktur  = isset($_POST["direktur"]) ? $_POST["direktur"] : "";
              $jenis = isset($_POST["jenis"]) ? $_POST["jenis"] : "";
              $kelas  = isset($_POST["kelas"]) ? $_POST["kelas"] : "";
              $status = isset($_POST["status"]) ? $_POST["status"] : "";
              $kepemilikan  = isset($_POST["kepemilikan"]) ? $_POST["kepemilikan"] : "";
              $namapenyelenggara = isset($_POST["namapenyelenggara"]) ? $_POST["namapenyelenggara"] : "";
              $bpjs  = isset($_POST["bpjs"]) ? $_POST["bpjs"] : "";
              $alamat = isset($_POST["alamat"]) ? $_POST["alamat"] : "";
              $kab  = isset($_POST["kab"]) ? $_POST["kab"] : "";
              $luastanah = isset($_POST["luastanah"]) ? $_POST["luastanah"] : "";
              $luasbangunan  = isset($_POST["luasbangunan"]) ? $_POST["luasbangunan"] : "";
              $telpon = isset($_POST["telpon"]) ? $_POST["telpon"] : "";
              $email  = isset($_POST["email"]) ? $_POST["email"] : "";
              $website = isset($_POST["website"]) ? $_POST["website"] : "";
              $noizin  = isset($_POST["noizin"]) ? $_POST["noizin"] : "";
              $suratizin = isset($_POST["suratizin"]) ? $_POST["suratizin"] : "";
              $berlaku  = isset($_POST["berlaku"]) ? $_POST["berlaku"] : "";
              $akreditasi = isset($_POST["akreditasi"]) ? $_POST["akreditasi"] : "";
              $penetapan  = isset($_POST["penetapan"]) ? $_POST["penetapan"] : "";
              $aktifakreditasi = isset($_POST["aktifakreditasi"]) ? $_POST["aktifakreditasi"] : "";
              $layananunggulan  = isset($_POST["layananunggulan"]) ? $_POST["layananunggulan"] : "";
              $simrs = isset($_POST["simrs"]) ? $_POST["simrs"] : "";
              $bankdarah = isset($_POST["bankdarah"]) ? $_POST["bankdarah"] : "";
              //
              $updateArrayData = array(
                'namars' => $namars,
                'tahunberdiri' => $tahunberdiri,
                'direktur' => $direktur,
                'jenis' => $jenis,
                'kelas' => $kelas,
                'status' => $status,
                'kepemilikan' => $kepemilikan,
                'namapenyelenggara' => $namapenyelenggara,
                'bpjs' => $bpjs,
                'alamat' => $alamat,
                'kab' => $kab,
                'luastanah' => $luastanah,
                'luasbangunan' => $luasbangunan,
                'telpon' => $telpon,
                'email' => $email,
                'website' => $website,
                'noizin' => $noizin,
                'suratizin' => $suratizin,
                'berlaku' => $berlaku,
                'akreditasi' => $akreditasi,
                'penetapan' => $penetapan,
                'aktifakreditasi' => $aktifakreditasi,
                'layananunggulan' => $layananunggulan,
                'simrs' => $simrs,
                'bankdarah' => $bankdarah
              );
              if(count($error) == 0) {
                  $update = $this->datautama->update($updateArrayData, array('idrs' => "1"));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('kodeprogram');
          $delete = $this->kodeprogram->delete(array('idkode' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
