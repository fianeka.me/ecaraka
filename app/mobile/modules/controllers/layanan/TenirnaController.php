<?php

use \modules\controllers\MainController;

class TenirnaController extends MainController {

      public function index() {
        $this->model('tenirna');
        $data1 = $this->tenirna->get();
        $this->template('layanan/tenirna', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('tenirna');

          $databulan = [
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
          ];

          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $bulan  = isset($_POST["bulan"]) ? $_POST["bulan"] : "";
              $tahun  = isset($_POST["tahun"]) ? $_POST["tahun"] : "";
              $kodeicd  = isset($_POST["kodeicd"]) ? $_POST["kodeicd"] : "";
              $deskripsi  = isset($_POST["deskripsi"]) ? $_POST["deskripsi"] : "";
              $hidupl  = isset($_POST["hidupl"]) ? $_POST["hidupl"] : "";
              $hidupp  = isset($_POST["hidupp"]) ? $_POST["hidupp"] : "";
              $matil  = isset($_POST["matil"]) ? $_POST["matil"] : "";
              $matip  = isset($_POST["matip"]) ? $_POST["matip"] : "";

              $nomorbulan = array_search($bulan, $databulan)+1;


              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->tenirna->insert(
                        array(
                          'nomorbulan' => $nomorbulan,
                          'bulan' => $bulan,
                          'tahun' => $tahun,
                          'kodeicd' => $kodeicd,
                          'deskripsi' => $deskripsi,
                          'hidupl' => $hidupl,
                          'hidupp' => $hidupp,
                          'matil' => $matil,
                          'matip' => $matip
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'nomorbulan' => $nomorbulan,
                  'bulan' => $bulan,
                  'tahun' => $tahun,
                  'kodeicd' => $kodeicd,
                  'deskripsi' => $deskripsi,
                  'hidupl' => $hidupl,
                  'hidupp' => $hidupp,
                  'matil' => $matil,
                  'matip' => $matip
                );
                if(count($error) == 0) {
                    $update = $this->tenirna->update($updateArrayData, array('idten' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('tenirna');
          $delete = $this->tenirna->delete(array('idten' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
