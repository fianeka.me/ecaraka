<?php

use \modules\controllers\MainController;

class FasilitasttController extends MainController {

      public function index() {
        $this->model('fasilitastt');
        $data1 = $this->fasilitastt->get();
        $this->template('layanan/fasilitastt', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('fasilitastt');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $jenispelayanan  = isset($_POST["jenispelayanan"]) ? $_POST["jenispelayanan"] : "";
              $vvip  = isset($_POST["vvip"]) ? $_POST["vvip"] : "";
              $vip  = isset($_POST["vip"]) ? $_POST["vip"] : "";
              $kelas1  = isset($_POST["kelas1"]) ? $_POST["kelas1"] : "";
              $kelas2  = isset($_POST["kelas2"]) ? $_POST["kelas2"] : "";
              $kelas3  = isset($_POST["kelas3"]) ? $_POST["kelas3"] : "";
              $khusus  = isset($_POST["khusus"]) ? $_POST["khusus"] : "";

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->fasilitastt->insert(
                        array(
                          'jenispelayanan' => $jenispelayanan,
                          'vvip' => $vvip,
                          'vip' => $vip,
                          'kelas1' => $kelas1,
                          'kelas2' => $kelas2,
                          'kelas3' => $kelas3,
                          'khusus' => $khusus
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'jenispelayanan' => $jenispelayanan,
                  'vvip' => $vvip,
                  'vip' => $vip,
                  'kelas1' => $kelas1,
                  'kelas2' => $kelas2,
                  'kelas3' => $kelas3,
                  'khusus' => $khusus
                );
                if(count($error) == 0) {
                    $update = $this->fasilitastt->update($updateArrayData, array('idfasilitas' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('fasilitastt');
          $delete = $this->fasilitastt->delete(array('idfasilitas' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
