<?php

use \modules\controllers\MainController;

class PelayananController extends MainController {

      public function index() {
        $this->model('dataibu');
        $data1 = $this->dataibu->get();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          $tahun  = isset($_POST["tahun"]) ? $_POST["tahun"] : "";
          $databulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

          $this->model('datautama');
          $datautama = $this->datautama->get();

          $this->model('ttrs');
          $ttrs = $this->ttrs->get();

          $this->model('pelayanan');
          $pelayanan = $this->pelayanan->get();

          $this->model('indikatorlayanan');
          $idk1 = $this->indikatorlayanan->getWhere(array(
              'tahun' => $tahun
          ));

          $this->model('fasilitastt');
          $fasilitastt = $this->fasilitastt->get();


          $pengunjungrs = array();
          $pengunjungirna = array();
          $pengunjungirja = array();
          $datajiwa = array();
          $dataibu = array();
          $tenirna = array();
          $tenirja = array();

          foreach ($databulan as $bulan) {

            $this->model('pengunjungrs');
            $idk = $this->pengunjungrs->getWhere(array(
              'tahun' => $tahun,
              'bulan' => $bulan,
            ));
            $pengunjungrs[$bulan] = $idk;

            $this->model('pengunjungirja');
            $idk = $this->pengunjungirja->getWhere(array(
              'tahun' => $tahun,
              'bulan' => $bulan,
            ));
            $pengunjungirja[$bulan] = $idk;

            $this->model('pengunjungirna');
            $idk = $this->pengunjungirna->getWhere(array(
              'tahun' => $tahun,
              'bulan' => $bulan,
            ));
            $pengunjungirna[$bulan] = $idk;

            $this->model('datajiwa');
            $idk = $this->datajiwa->getWhere(array(
              'tahun' => $tahun,
              'bulan' => $bulan,
            ));
            $datajiwa[$bulan] = $idk[0];

            $this->model('dataibu');
            $idk = $this->dataibu->getWhere(array(
              'tahun' => $tahun,
              'bulan' => $bulan,
            ));
            $dataibu[$bulan] = $idk[0];

            $this->model('tenirja');
            $idk = $this->tenirja->getWhere(array(
              'tahun' => $tahun,
              'bulan' => $bulan,
            ));
            $tenirja[$bulan] = $idk;

            $this->model('tenirna');
            $idk = $this->tenirna->getWhere(array(
              'tahun' => $tahun,
              'bulan' => $bulan,
            ));
            $tenirna[$bulan] = $idk;
          }

          $datas= array(
            "datautama"=>$datautama[0],
            "ttrs"=>$ttrs,
            "fasilitastt"=>$fasilitastt,
            "idk"=>$idk1[0],
            "pengunjungrs"=>$pengunjungrs,
            "pengunjungirna"=>$pengunjungirna,
            "pengunjungirja"=>$pengunjungirja,
            "datajiwa"=>$datajiwa,
            "dataibu"=>$dataibu,
            "tenirna"=>$tenirna,
            "tenirja"=>$tenirja,
            "pelayanan"=>$pelayanan);

          $datas["tahunberjalan"] = $tahun;
          $_SESSION['toprint'] = $datas;



          $this->template('laporan/pelayanan', array("datanya" => $datas));


        }else{
          $this->template('laporan/pelayanan', array());

        }
      }

      public function reportprint() {
          ?>
            <meta http-equiv="refresh" content="1;url=<?php SITE_URL; ?>utils/public/pelayanan.php">
          <?php
      }


}
?>
