<?php

use \modules\controllers\MainController;

class KoderekeningController extends MainController {

      public function index() {
        $this->model('koderekening');
        $data1 = $this->koderekening->get();
        $this->template('master/koderekening', array("kode"=>$data1));
      }

      public function addchange() {
          $this->model('koderekening');
          $error      = array();
          $success    = null;
          if($_SERVER["REQUEST_METHOD"] == "POST") {
              $id = isset($_POST["id"]) ? $_POST["id"] : "";
              $kodeprogram  = isset($_POST["kodeprogram"]) ? $_POST["kodeprogram"] : "";
              $namaprogram = isset($_POST["namaprogram"]) ? $_POST["namaprogram"] : "";

              if (empty($id)) {
                if(count($error) == 0) {
                    $insert = $this->koderekening->insert(
                        array(
                          'koderekening' => $kodeprogram,
                          'uraian' => $namaprogram
                        )
                    );
                    if($insert) {
                        $success = "Data Berhasil di ditambahkan.";
                    }
                }
              } else {
                $updateArrayData = array(
                  'koderekening' => $kodeprogram,
                  'uraian' => $namaprogram
                );
                if(count($error) == 0) {
                    $update = $this->koderekening->update($updateArrayData, array('idkoderek' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
              }
          }
          $this->back();
          // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
      }

      public function delete()
      {
          $id = isset($_GET["id"]) ? $_GET["id"] : 0;
          $this->model('koderekening');
          $delete = $this->koderekening->delete(array('idkoderek' => $id));
          if ($delete) {
              $this->back();
          }
      }
}
?>
